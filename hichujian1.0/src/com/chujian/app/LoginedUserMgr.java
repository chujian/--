package com.chujian.app;

import com.hichujian.client.model.UserDO;

public class LoginedUserMgr {
	private static UserDO loginedUser;
	
	public static UserDO getLoginedUser(){
		return loginedUser;
	}

	public static void setLoginedUser(UserDO loginedUser) {
		LoginedUserMgr.loginedUser = loginedUser;
	}
	
}
