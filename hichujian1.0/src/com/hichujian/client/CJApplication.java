/*package com.hichujian.client;

import android.util.Log;
import android.widget.Toast;

import com.baidu.frontia.FrontiaApplication;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKGeneralListener;
import com.baidu.mapapi.map.MKEvent;
import com.baidu.mapapi.map.MyLocationOverlay.LocationMode;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.hichujian.client.utils.DataKeeper;

public class CJApplication extends FrontiaApplication {
	public static final boolean ON_NO_SERVER = true;
	
	public BMapManager mapManager;//在活动地图界面赋值，之后作为全局变量不再初始化
	public MKGeneralListener generalListener;//百度地图获取监听器
	public LocationClient locationClient;//定位
	public BDLocationListener locationListener;//定位监听
	public BDLocation curLocation;//当前位置
	
	@Override
	public void onCreate() {
		super.onCreate();
		DataKeeper.init(this);
		//地图注册监听器
		generalListener = new MKGeneralListener() {
			@Override
			public void onGetPermissionState(int iError) {
				//非零值表示key验证未通过
	            if (iError != 0) {
	                //授权Key错误：
	                Toast.makeText(getApplicationContext(), "AndroidManifest.xml 文件输入正确的授权Key,并检查您的网络连接是否正常！error: "+iError, Toast.LENGTH_LONG).show();
	            }
	            else{
	            	Toast.makeText(getApplicationContext(), "key认证成功", Toast.LENGTH_LONG).show();
	            }
				Log.i("", "onGetPermissionState = " + iError);
			}
			@Override
			public void onGetNetworkState(int iError) {
				if (iError == MKEvent.ERROR_NETWORK_CONNECT) {
	                Toast.makeText(getApplicationContext(), "您的网络出错啦！", Toast.LENGTH_LONG).show();
	            }
	            else if (iError == MKEvent.ERROR_NETWORK_DATA) {
	                Toast.makeText(getApplicationContext(), "输入正确的检索条件！",
	                        Toast.LENGTH_LONG).show();
	            }
				Log.i("", "onGetNetworkState = " + iError);
			}
		};
		//定位监听器
		locationListener = new BDLocationListener() {
			@Override
			public void onReceivePoi(BDLocation arg0) {}
			@Override
			public void onReceiveLocation(BDLocation location) {
				if(location == null) {
					return;
				}
				curLocation = location;
				locationClient.stop();//停止位置检测
			}
		};
        //定位初始化
        locationClient = new LocationClient(this);
        locationClient.registerLocationListener(locationListener);
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true);//打开gps
        option.setCoorType("bd09ll");     //设置坐标类型
        option.setScanSpan(1000);
        locationClient.setLocOption(option);
        
        locationClient.start();
	}

	@Override
	public void onTerminate() {
		DataKeeper.destroy();
		super.onTerminate();
	}
}
*/