package com.hichujian.client.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.hichujian.client.Constants;
import com.hichujian.client.utils.AESUtils;
import com.hichujian.client.utils.AsyncRunner;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
/**
 * 活动评论相关API
 * @author 307427687
 *
 */
public class CommentAPI {
	//请求URL
	public static final String URL_COMMENT_RECORDS = Constants.BASE_URL + "comment/records";
	public static final String URL_COMMENT_CREATE = Constants.BASE_URL + "comment/create";
	public static final String URL_COMMENT_DELETE = Constants.BASE_URL + "comment/delete";
	
	//URL参数
	public static final String KEY_USER_ID = AESUtils.encrypt("user_id"); 
	public static final String KEY_COMMENT_ID = AESUtils.encrypt("comment_id"); 
	public static final String KEY_ACTIVITY_ID = AESUtils.encrypt("activity_id");
	public static final String KEY_OFFSET = AESUtils.encrypt("offset");
	public static final String KEY_SIZE = AESUtils.encrypt("size");
	public static final String KEY_BEFORE_FLOOR = AESUtils.encrypt("before_floor");
	public static final String KEY_CTEATE_TIME = AESUtils.encrypt("create_time");
	public static final String KEY_CONTENT = AESUtils.encrypt("content");
	public static final String KEY_FLOOR = AESUtils.encrypt("floor");
	
	private static CommentAPI commentAPI;//单例
	
	private CommentAPI() {}
	
	public synchronized static CommentAPI getInstance() {
		if(null == commentAPI) {
			commentAPI = new CommentAPI();
		}
		return commentAPI;
	}
	/**
	 * 获取活动的评论列表
	 * @param userID 
	 * 		用户ID
	 * @param activityID 
	 * 		活动ID
	 * @param offset	
	 * 		偏移量
	 * @param size	
	 * 		获取数量
	 * @param beforFloor	
	 * 		获取在该楼层以下的评论
	 * 		为0 表示获取最新楼层以下的
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void getCommnentList(long userID, long activityID, int offset, int size, int beforFloor, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_ID, AESUtils.encrypt(String.valueOf(activityID))));
		params.add(new BasicNameValuePair(KEY_OFFSET, AESUtils.encrypt(String.valueOf(offset))));
		params.add(new BasicNameValuePair(KEY_SIZE, AESUtils.encrypt(String.valueOf(size))));
		params.add(new BasicNameValuePair(KEY_BEFORE_FLOOR, AESUtils.encrypt(String.valueOf(beforFloor))));
		AsyncRunner.sendRequest(URL_COMMENT_RECORDS, params, AsyncRunner.HTTP_GET, listener);
	}
	/**
	 * 发表一条评论
	 * @param userID	
	 * 		用户id
	 * @param activityID	
	 * 		评论的活动ID
	 * @param content	
	 * 		评论内容
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void createComment(long userID, long activityID, String content, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_ID, AESUtils.encrypt(String.valueOf(activityID))));
		params.add(new BasicNameValuePair(KEY_CONTENT, AESUtils.encrypt(content)));
		AsyncRunner.sendRequest(URL_COMMENT_CREATE, params, AsyncRunner.HTTP_POST, listener);
	}
	/**
	 * 删除一条评论
	 * @param userID	
	 * 		用户id
	 * @param commentID	
	 * 		评论ID
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void deleteComment(long userID, long commentID, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_COMMENT_ID, AESUtils.encrypt(String.valueOf(commentID))));
		AsyncRunner.sendRequest(URL_COMMENT_DELETE, params, AsyncRunner.HTTP_POST, listener);
	}
}