package com.hichujian.client.api;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

import com.hichujian.client.Constants;
import com.hichujian.client.utils.AESUtils;
import com.hichujian.client.utils.AsyncRunner;
import com.hichujian.client.utils.Utils;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
/**
 * 用户相关API
 * 		对于所有请求的参数名和值都需要通过AES加密后传输
 * @author alen
 *
 */
public class UserAPI {
	//请求URL
	public static final String URL_USER_INFO = Constants.BASE_URL + "user/info";
	public static final String URL_USER_FRIEND_INFO = Constants.BASE_URL + "user/friendinfo";
	public static final String URL_USER_GET_AUTH_CODE = Constants.BASE_URL + "user/getauthcode";
	public static final String URL_USER_LOGIN = Constants.BASE_URL + "user/login";
	public static final String URL_USER_TOKEN_LOGIN = Constants.BASE_URL + "user/tokenlogin";
	public static final String URL_USER_LOGOUT = Constants.BASE_URL + "user/logout";
	public static final String URL_USER_CHECK_AUTH_CODE = Constants.BASE_URL + "user/checkauthcode";
	public static final String URL_USER_REGISTER = Constants.BASE_URL + "user/register";
	public static final String URL_USER_CHANGE_PASSWORD = Constants.BASE_URL + "user/changepwd";
	public static final String URL_USER_RESET_PASSWORD = Constants.BASE_URL + "user/resetpwd";
	public static final String URL_USER_SET_INFO = Constants.BASE_URL + "user/setinfo";
	public static final String URL_USER_UPDATE_LOCATION = Constants.BASE_URL + "user/updateloc";
	public static final String URL_USER_CHECK_REGISTER = Constants.BASE_URL + "user/checkregister";
	//URL参数
	public static final String KEY_PHONE_NUMBER = AESUtils.encrypt("phone_number");
	public static final String KEY_USER_ID = AESUtils.encrypt("user_id");
	public static final String KEY_TOKEN = AESUtils.encrypt("token");
	public static final String KEY_FRIEND_USER_ID = AESUtils.encrypt("friend_user_id");
	public static final String KEY_PASSWORD = AESUtils.encrypt("password");
	public static final String KEY_AUTH_CODE = AESUtils.encrypt("auth_code");
	public static final String KEY_BIRTHDAY = AESUtils.encrypt("birthday");
	public static final String KEY_SEX = AESUtils.encrypt("sex");
	public static final String KEY_REGISTER_TIME = AESUtils.encrypt("register_time");
	public static final String KEY_NICKNAME = AESUtils.encrypt("nickname");
	public static final String KEY_HEAD_PORTRAIT = AESUtils.encrypt("head_portrait");
	
	public static final String KEY_OLD_PASSWORD = AESUtils.encrypt("old_password");
	public static final String KEY_NEW_PASSWORD = AESUtils.encrypt("new_password");
	public static final String KEY_USER_INFO_JSON = AESUtils.encrypt("user_info");
	public static final String KEY_LONGITUDE = AESUtils.encrypt("longitude");
	public static final String KEY_LATITUDE = AESUtils.encrypt("latitude");
	//登陆类型
	public static final String LOGIN_TYPE_PHONE_NUMBER = AESUtils.encrypt("phone");
	public static final String LOGIN_TYPE_QQ_NUMBER = AESUtils.encrypt("qq");
	public static final String LOGIN_TYPE_WEIBO_NUMBER = AESUtils.encrypt("weibo");
	
	private static UserAPI userAPI;//单例
	
	private UserAPI() {}
	
	public synchronized static UserAPI getInstance() {
		if(null == userAPI) {
			userAPI = new UserAPI();
		}
		return userAPI;
	}
	
	/**
	 * 获取用户信息
	 * @param userID	
	 * 		用户ID
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void getUserInfo(long userID, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		AsyncRunner.sendRequest(URL_USER_INFO, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 获取初友用户信息
	 * @param userID	
	 * 		用户ID
	 * @param friendUserID
	 * 		初友用户ID
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void getFriendUserInfo(long userID, long friendUserID, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_FRIEND_USER_ID, AESUtils.encrypt(String.valueOf(friendUserID))));
		AsyncRunner.sendRequest(URL_USER_FRIEND_INFO, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 获取验证码请求
	 * @param phone
	 * 		手机号
	 * @param listener
	 * 		请求返回回调
	 */
	public void getAuthCode(String phone, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_PHONE_NUMBER, AESUtils.encrypt(phone)));
		AsyncRunner.sendRequest(URL_USER_GET_AUTH_CODE, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 验证验证码
	 * @param phone
	 * 		手机号
	 * @param authCode
	 * 		验证码
	 * @param listener
	 * 		请求返回回调
	 */
	public void checkAuthCode(String phone, String authCode, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_PHONE_NUMBER, AESUtils.encrypt(phone)));
		params.add(new BasicNameValuePair(KEY_AUTH_CODE, AESUtils.encrypt(authCode)));
		AsyncRunner.sendRequest(URL_USER_CHECK_AUTH_CODE, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 登陆
	 * @param phoneNumber	
	 * 		手机号
	 * @param password	
	 * 		密码
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void login(String phoneNumber, String password, OnResponseListener listener) {
		password = Utils.md5(password);
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_PHONE_NUMBER, AESUtils.encrypt(phoneNumber)));
		params.add(new BasicNameValuePair(KEY_PASSWORD, AESUtils.encrypt(password)));
		AsyncRunner.sendRequest(URL_USER_LOGIN, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * Token登入
	 * @param userID
	 * 		用户id
	 * @param token
	 * 		用户登入Token令牌
	 * @param listener
	 * 		请求返回回调
	 */
	public void tokenLogin(long userID, String token, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_TOKEN, AESUtils.encrypt(token)));
		AsyncRunner.sendRequest(URL_USER_TOKEN_LOGIN, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 注销登陆
	 * @param userID 
	 * 		用户名
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void logout(long userID, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		AsyncRunner.sendRequest(URL_USER_LOGOUT, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 注册
	 * @param phoneNumber	
	 * 		手机号
	 * @param password	
	 * 		密码
	 * @param nickname	
	 * 		昵称
	 * @param headPortrait	
	 * 		头像
	 * @param sex	
	 * 		性别 java.sql.Date
	 * @param birthday	
	 * 		生日
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void register(String phoneNumber, String password, String nickname, String headPortrait, String sex, Date birthday, OnResponseListener listener) {
		password = Utils.md5(password);
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_PHONE_NUMBER, AESUtils.encrypt(phoneNumber)));
		params.add(new BasicNameValuePair(KEY_PASSWORD, AESUtils.encrypt(password)));
		params.add(new BasicNameValuePair(KEY_SEX, AESUtils.encrypt(sex)));
		params.add(new BasicNameValuePair(KEY_HEAD_PORTRAIT, AESUtils.encrypt(headPortrait)));
		params.add(new BasicNameValuePair(KEY_NICKNAME, AESUtils.encrypt(nickname)));
		params.add(new BasicNameValuePair(KEY_BIRTHDAY, AESUtils.encrypt(birthday.toString())));
		AsyncRunner.sendRequest(URL_USER_REGISTER, params, AsyncRunner.HTTP_POST, listener);
	}
	/**
	 * 修改密码
	 * @param userID	
	 * 		用户id
	 * @param oldPassword	
	 * 		旧密码
	 * @param newPassword	
	 * 		新密码
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void changePassword(long userID, String oldPassword, String newPassword, OnResponseListener listener) {
		Log.i("tag", oldPassword + "旧密码");
		Log.i("tag", newPassword + "新密码");
		oldPassword = Utils.md5(oldPassword);
		newPassword = Utils.md5(newPassword);
		Log.i("tag", oldPassword + "加密后旧密码");
		Log.i("tag", newPassword + "加密后新密码");
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_OLD_PASSWORD, AESUtils.encrypt(oldPassword)));
		params.add(new BasicNameValuePair(KEY_NEW_PASSWORD, AESUtils.encrypt(newPassword)));
		AsyncRunner.sendRequest(URL_USER_CHANGE_PASSWORD, params, AsyncRunner.HTTP_POST, listener);
	}
	/**
	 * 重置密码(用于忘记密码)
	 * @param phoneNumber	
	 * 		手机号
	 * @param newPassword	
	 * 		新密码
	 * @param listener	
	 * 		请求返回后回调	
	 */
	public void resetPassword(String phoneNumber, String newPassword, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_PHONE_NUMBER, AESUtils.encrypt(phoneNumber)));
		params.add(new BasicNameValuePair(KEY_NEW_PASSWORD, AESUtils.encrypt(newPassword)));
		AsyncRunner.sendRequest(URL_USER_RESET_PASSWORD, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 用户个人信息设置请求
	 * @param userID
	 * 		用户id
	 * @param userInfo
	 * 		用户DO的JSON字串
	 * 		注明:可以提供包括 微博、QQ、身高体重、生日、性别、工作、个性签名、语音、单位、图片、本地、
	 * 			昵称、头像、情感状态、兴趣爱好、常出没、星座等的修改
	 * 		其中引用类型字段为null、或数值类型字段为0不更新
	 * 	更新示例：
	 * 		更新用户个性签名
	 * 		UserDO user = new UserDO()
	 * 		user.setPersonalSignature("我的新个性签名");
	 * 		userInfo = JSON.toJSONString(user);即可
	 * 		因为其他字段为null 或 0 所以只会更新设置字段
	 * @param listener
	 */
	public void setUserInfo(long userID, String userInfo, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_USER_INFO_JSON, AESUtils.encrypt(userInfo)));
		AsyncRunner.sendRequest(URL_USER_SET_INFO, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 更新用户位置请求
	 * @param userID
	 * 		用户id
	 * @param longitude
	 * 		用户当前位置经度
	 * @param latitude
	 * 		用户当前位置纬度
	 * @param listener
	 * 		请求返回回调
	 */
	public void updateLoc(long userID, double longitude, double latitude, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_LONGITUDE, AESUtils.encrypt(String.valueOf(longitude))));
		params.add(new BasicNameValuePair(KEY_LATITUDE, AESUtils.encrypt(String.valueOf(latitude))));
		AsyncRunner.sendRequest(URL_USER_UPDATE_LOCATION, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 验证手机是否已注册
	 * @param phone
	 * 		手机号
	 * @param listener
	 * 		请求返回回调
	 */
	public void checkRegister(String phone, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_PHONE_NUMBER, AESUtils.encrypt(phone)));
		AsyncRunner.sendRequest(URL_USER_CHECK_REGISTER, params, AsyncRunner.HTTP_POST, listener);
	}
}