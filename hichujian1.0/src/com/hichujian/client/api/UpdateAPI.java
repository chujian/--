package com.hichujian.client.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.hichujian.client.Constants;
import com.hichujian.client.utils.AsyncRunner;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;

/**
 * 动态更新相关的API
 * 		该模块所有请求不需要加密、不需要登入认证
 * @author Alen
 *
 */
public class UpdateAPI {
	public static final String URL_CHECK_SUDOKU_UPDATE = Constants.BASE_URL + "update/checksudoku";
	public static final String URL_CHECK_CLIENT_UPDATE = Constants.BASE_URL + "update/checkclient";
	
	//请求字段
	public static final String KEY_VERSION = "version";
	//单例模式
	private static UpdateAPI updateAPI = null;
	private UpdateAPI() {}
	public synchronized static UpdateAPI getInstance() {
		if(updateAPI == null) {
			updateAPI = new UpdateAPI();
		}
		return updateAPI;
	}
	
	/**
	 * 检查九宫格图片更新
	 * @param currentVersion
	 * 		当前九宫格版本
	 * @param listener
	 * 		请求响应回调 
	 */
	public void checkSudokuUpdate(String currentVersion, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_VERSION, currentVersion));
		AsyncRunner.sendRequest(URL_CHECK_SUDOKU_UPDATE, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 检查客户端更新
	 * @param currentVersion
	 * 		当前客户端版本
	 * @param listener
	 *		请求响应回调 
	 */
	public void checkClientUpdate(String currentVersion, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_VERSION, currentVersion));
		AsyncRunner.sendRequest(URL_CHECK_CLIENT_UPDATE, params, AsyncRunner.HTTP_GET, listener);
	}
}
