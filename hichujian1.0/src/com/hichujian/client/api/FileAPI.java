package com.hichujian.client.api;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;
import android.util.Log;

import com.hichujian.client.Constants;
import com.hichujian.client.utils.AESUtils;
import com.hichujian.client.utils.AsyncRunner;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.upyun.api.Uploader;
import com.upyun.api.utils.UpYunException;
import com.upyun.api.utils.UpYunUtils;

/**
 * 文件相关API
 * @author Alen
 */
public class FileAPI {
	private static final String API_KEY = "c5ub9xrbDol1vMzIUCwz+KtZ8Zg="; //测试使用的表单api验证密钥
	private static final String BUCKET = "hichujian-files";						//存储空间
	private static final long EXPIRATION = System.currentTimeMillis()/1000 + 1000 * 5 * 10; //过期时间，必须大于当前时间
	
	//请求URL
	public static final String URL_FILE_DELETE = Constants.BASE_URL + "file/delete";
	public static final String URL_FILE_UPLOAD = Constants.BASE_URL + "file/upload";
	//请求参数名
	public static final String KEY_USER_ID = AESUtils.encrypt("user_id");
	public static final String KEY_FILE_PATH = AESUtils.encrypt("file_path");
	
	//单例模式
	private static FileAPI fileAPI;
	private FileAPI() {}
	public synchronized static FileAPI getInstance() {
		if(null == fileAPI) {
			fileAPI = new FileAPI();
		}
		return fileAPI;
	}
	
	/**
	 * 文件上传
	 * @param userID
	 * 		用户id
	 * @param filePath
	 * 		待上传的文件路径(本地的文件)
	 * @param listener
	 * 		请求返回回调
	 * @return
	 * 		上传文件路径
	 */
	public void upload(long userID, String filePath, OnResponseListener listener) {
		UploadTask task = new UploadTask(userID, filePath, listener);
		task.execute();
	}
	
	/**
	 * 删除文件
	 * @param userID
	 * 		用户id
	 * @param path
	 * 		文件完整url路径(http://hichujian-file.v0.upyun.com/1/40989124141.jpg)
	 * @param listener
	 * 		请求返回回调
	 */
	public void delete(long userID, String path, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_FILE_PATH, AESUtils.encrypt(String.valueOf(path))));
		AsyncRunner.sendRequest(URL_FILE_DELETE, params, AsyncRunner.HTTP_POST, listener);
	}
	
	private class UploadTask extends AsyncTask<Void, Void, String> {
		public long userID;
		public String filePath; 
		public OnResponseListener listener;
		public String saveKey;
		public UploadTask(long userID, String path, OnResponseListener listener) {
			// TODO Auto-generated constructor stub
			this.userID = userID;
			this.filePath = path;
			this.listener = listener;
			Log.i("tag", path);
		}

		@Override
		public String doInBackground(Void... params) {
			Log.i("tag", "doInBackground()");
			HashMap<String, Object> parameters = new HashMap<String, Object>();
			String string = null;
			parameters.put("notify-url", URL_FILE_UPLOAD);
			
			try {
				//设置服务器上保存文件的目录和文件名，如果服务器上同目录下已经有同名文件会被自动覆盖
				Calendar calendar = Calendar.getInstance();
				String date = calendar.get(Calendar.YEAR) + new DecimalFormat("00").format(calendar.get(Calendar.MONTH)+1) + calendar.get(Calendar.DAY_OF_MONTH);
				String fileName = userID + "_" + System.currentTimeMillis() + "." + filePath.substring(filePath.lastIndexOf(".") + 1);
				saveKey = File.separator + date + File.separator + fileName;
				Log.i("tag", saveKey);
				
				//取得base64编码后的policy
				String policy = UpYunUtils.makePolicy(saveKey, EXPIRATION, BUCKET, parameters);
				
				//根据表单api签名密钥对policy进行签名
				String signature = UpYunUtils.signature(policy + "&" + API_KEY);

				//上传文件到对应的bucket中
				string = Uploader.upload(policy, signature , BUCKET, filePath);

			} catch (UpYunException e) {
				e.printStackTrace();
				//Log.i("tag", e.getMessage());
			}

			return string;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result != null) {
				 Log.i("tag", "onPostExecute() 成功");
				listener.onResponse(saveKey + ";" + result);
			} else {
				Log.i("tag", "onPostExecute() 失败");
				listener.onError(result);
			}
		}

	}
	
}
