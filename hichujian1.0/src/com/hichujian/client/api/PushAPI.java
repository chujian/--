package com.hichujian.client.api;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.alibaba.fastjson.JSONObject;
import com.hichujian.client.Constants;
import com.hichujian.client.model.PushSettingDO;
import com.hichujian.client.utils.AESUtils;
import com.hichujian.client.utils.AsyncRunner;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
/**
 * 活动评论相关API
 * @author 307427687
 *
 */
public class PushAPI {
	//请求URL
	public static final String URL_PUSH_SETTINGS = Constants.BASE_URL + "push/settings";
	public static final String URL_PUSH_CHANGE_SETTINGS = Constants.BASE_URL + "push/changesettings";
	public static final String URL_PUSH_RECORDS = Constants.BASE_URL + "push/records";
	public static final String URL_PUSH_INFO = Constants.BASE_URL + "push/info";
	public static final String URL_PUSH_GET_ACTIVITY_PUSH = Constants.BASE_URL + "push/getactivitypush";
	
	//URL参数
	public static final String KEY_USER_ID = AESUtils.encrypt("user_id");
	public static final String KEY_PUSH_ID = AESUtils.encrypt("push_id");
	public static final String KEY_PUSH_TYPE = AESUtils.encrypt("push_type");
	public static final String KEY_OFFSET = AESUtils.encrypt("offset");
	public static final String KEY_SIZE = AESUtils.encrypt("size");
	public static final String KEY_BEFORE_TIME = AESUtils.encrypt("before_time");
	public static final String KEY_LONGITUDE = AESUtils.encrypt("longitude");
	public static final String KEY_LATITUDE =  AESUtils.encrypt("latitude");
	public static final String KEY_SEX = AESUtils.encrypt("sex");
	public static final String KEY_TYPES =  AESUtils.encrypt("types");
	public static final String KEY_DISTANCE =  AESUtils.encrypt("distance");
	public static final String KEY_PUSH_SETTINGS =  AESUtils.encrypt("push_settings");
		
	private static PushAPI pushAPI;//单例
	
	private PushAPI() {}
	
	public synchronized static PushAPI getInstance() {
		if(null == pushAPI) {
			pushAPI = new PushAPI();
		}
		return pushAPI;
	}
	
	/**
	 * 获取推送设置
	 * @param userID	
	 * 		用户ID
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void getPushSettings(long userID, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		AsyncRunner.sendRequest(URL_PUSH_SETTINGS, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 获取推送信息请求
	 * @param userID
	 * 		用户id
	 * @param pushID
	 * 		推送id
	 * @param listener
	 * 		请求返回回调
	 */
	public void getPushInfo(long userID, long pushID, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_PUSH_ID, AESUtils.encrypt(String.valueOf(pushID))));
		AsyncRunner.sendRequest(URL_PUSH_INFO, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 获取推送记录 
	 * @param userID	
	 * 		用户ID
	 * @param pushType	
	 * 		推送类型
	 * @param beforTime	
	 * 		获取在该时间之前的推送记录
	 * @param offset	
	 * 		偏移量
	 * @param size	
	 * 		获取的条数
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void getPushRecords(long userID, int pushType, Timestamp beforTime, int offset, int size, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_PUSH_TYPE, AESUtils.encrypt(String.valueOf(pushType))));
		params.add(new BasicNameValuePair(KEY_BEFORE_TIME, AESUtils.encrypt(String.valueOf(beforTime.getTime()))));
		params.add(new BasicNameValuePair(KEY_OFFSET, AESUtils.encrypt(String.valueOf(offset))));
		params.add(new BasicNameValuePair(KEY_SIZE, AESUtils.encrypt(String.valueOf(size))));
		AsyncRunner.sendRequest(URL_PUSH_RECORDS, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 修改推送设置
	 * @param userID	
	 * 		用户ID
	 * @param pushSettingDO	
	 * 		推送设置DO
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void chagePushSettings(long userID, PushSettingDO pushSettingDO, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_PUSH_SETTINGS, AESUtils.encrypt(JSONObject.toJSONString(pushSettingDO))));
		AsyncRunner.sendRequest(URL_PUSH_CHANGE_SETTINGS, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 获取活动推送请求
	 * @param userID
	 * 		用户id
	 * @param longitude
	 * 		用户当前位置经度
	 * @param latitude
	 * 		用户当前位置纬度
	 * @param sex
	 * 		性别 (男、女、null)
	 * @param type
	 * 		类型
	 * @param distance
	 * 		距离
	 * @param listener
	 * 		请求返回回调
	 */
	public void getActivityPush(long userID, double longitude, double latitude, String sex, List<String> type, double distance,  OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_LONGITUDE, AESUtils.encrypt(String.valueOf(longitude))));
		params.add(new BasicNameValuePair(KEY_LATITUDE, AESUtils.encrypt(String.valueOf(latitude))));
		if(sex != null && !"".equals(sex)) {
			params.add(new BasicNameValuePair(KEY_SEX, AESUtils.encrypt(sex)));
		}
		if(type != null && type.size() != 0) {
			String types = "";
			int index = 0;
			int length = type.size();
			while(index < length - 1) {
				types += type.get(index) + ",";
				index ++;
			}
			types +=  type.get(index);
			params.add(new BasicNameValuePair(KEY_TYPES, AESUtils.encrypt(types)));
		}
		params.add(new BasicNameValuePair(KEY_DISTANCE, AESUtils.encrypt(String.valueOf(distance))));
		AsyncRunner.sendRequest(URL_PUSH_GET_ACTIVITY_PUSH, params, AsyncRunner.HTTP_POST, listener);
	}
}