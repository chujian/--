package com.hichujian.client.api;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.hichujian.client.Constants;
import com.hichujian.client.utils.AESUtils;
import com.hichujian.client.utils.AsyncRunner;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;

/**
 * 活动相关API
 * @author ALEN
 */
public class ActivityAPI {
	//请求URL
	public static final String URL_ACTIVITY_INFO = Constants.BASE_URL + "activity/info";
	public static final String URL_ACTIVITY_AROUND_LIST = Constants.BASE_URL + "activity/around";
	public static final String URL_ACTIVITY_PUBLISHED_LIST = Constants.BASE_URL + "activity/publishlist";
	public static final String URL_ACTIVITY_SIGNUPED_LIST = Constants.BASE_URL + "activity/signuplist";	//我报名的活动列表
	public static final String URL_ACTIVITY_SIGNUPER = Constants.BASE_URL + "activity/signuper";		//活动报名者列表
	public static final String URL_ACTIVITY_CREATE = Constants.BASE_URL + "activity/create";
	public static final String URL_ACTIVITY_DELETE = Constants.BASE_URL + "activity/delete";
	public static final String URL_ACTIVITY_SIGN_UP = Constants.BASE_URL + "activity/signup";
	public static final String URL_ACTIVITY_SIGN_OUT = Constants.BASE_URL + "activity/signout";
	public static final String URL_ACTIVITY_REPORT = Constants.BASE_URL + "activity/report";
	public static final String URL_ACTIVITY_ACCEPT = Constants.BASE_URL + "activity/accept";
	//URL参数
	public static final String KEY_ACTIVITY_ID = AESUtils.encrypt("activity_id");
	public static final String KEY_USER_ID = AESUtils.encrypt("user_id");
	public static final String KEY_LONGITUDE = AESUtils.encrypt("longitude");
	public static final String KEY_LATITUDE = AESUtils.encrypt("latitude");
	public static final String KEY_START_AGE = AESUtils.encrypt("start_age");
	public static final String KEY_END_AGE = AESUtils.encrypt("end_age");
	public static final String KEY_SEX = AESUtils.encrypt("sex");
	public static final String KEY_ACTIVITY_TYPE = AESUtils.encrypt("activity_type");
	public static final String KEY_DISTANCE = AESUtils.encrypt("distance");
	public static final String KEY_CONSTELLATION = AESUtils.encrypt("constellation");
	public static final String KEY_OFFSET = AESUtils.encrypt("offset");
	public static final String KEY_SIZE = AESUtils.encrypt("size");
	public static final String KEY_BEFORE_TIME = AESUtils.encrypt("before_time");
	public static final String KEY_ACTIVITY_CONTENT = AESUtils.encrypt("activity_content");
	public static final String KEY_INVITED_OBJECT_SEX = AESUtils.encrypt("invited_object_sex");
	public static final String KEY_ACTIVITY_EXPLAIN = AESUtils.encrypt("activity_explain");
	public static final String KEY_EXPECTED_NUMBER = AESUtils.encrypt("expected_number");
	public static final String KEY_ACTIVITY_TIME = AESUtils.encrypt("activity_time");
	public static final String KEY_ACTIVITY_LONGITUDE = AESUtils.encrypt("activity_longitude");
	public static final String KEY_ACTIVITY_LATITUDE = AESUtils.encrypt("activity_latitude");
	public static final String KEY_ACTIVITY_PLACE = AESUtils.encrypt("activity_place");
	public static final String KEY_ACTIVITY_PAST_TIME = AESUtils.encrypt("activity_past_time");
	public static final String KEY_ACTIVITY_PICTURE = AESUtils.encrypt("activity_picture");
	public static final String KEY_SIGN_UP_INFO = AESUtils.encrypt("sign_up_info");
	public static final String KEY_REPORT_INFO = AESUtils.encrypt("report_info");
	public static final String KEY_SIGN_UPER_ID = AESUtils.encrypt("sign_uper_id");
	
	
	
	//获取自己或他人的活动详情
	private static final String KEY_TYPE = AESUtils.encrypt("type");
	public static final String OTHERS = "others";
	public static final String SELF = "self";
	
	private static ActivityAPI activityAPI;//单例
	
	private ActivityAPI() {}
	
	public synchronized static ActivityAPI getInstance() {
		if(null == activityAPI) {
			activityAPI = new ActivityAPI();
		}
		return activityAPI;
	}
	
	/**
	 * 获取活动详情
	 * @param userID
	 * 		用户id	
	 * @param activityID	
	 * 		活动ID
	 * @param type	
	 * 		获取何人的活动(取值有ActivityAPI.OTHERS、ActivityAPI.SELF)
	 * 		若获取他人的活动 多两个个字段：是否举报、是否报名
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void getActivityInfo(long userID, long activityID, String type, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_ID, AESUtils.encrypt(String.valueOf(activityID))));
		params.add(new BasicNameValuePair(KEY_TYPE, AESUtils.encrypt(type)));
		AsyncRunner.sendRequest(URL_ACTIVITY_INFO, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 获取附近的活动
	 * @param userID	
	 * 		用户id
	 * @param longitude	
	 * 		经度
	 * @param latitude	
	 * 		纬度
	 * @param distance	
	 * 		距离 
	 * @param sex	
	 * 		性别 
	 * 		为null为不作为筛选条件
	 * @param startAge	
	 * 		年龄开始范围 
	 * 		为0不作为筛选条件
	 * @param endAge	
	 * 		年龄结束范围
	 * @param activityType	
	 * 		活动类型
	 * 		为null不作为筛选条件
	 * @param constellation	
	 * 		星座 为null不作为晒条件
	 * @param offset	
	 * 		偏移量
	 * @param size	
	 * 		获取数量 为0是表示获取所有
	 * @param beforeTime	
	 * 		获取在该时间之前发布的活动 
	 * 		主要用于上拉
	 * 		为null 不作为筛选条件
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void getActivityAround(long userID, double longitude, double latitude, double distance, String sex, int startAge, int endAge,
			List<String> activityType, String constellation, int offset, int size, Timestamp beforeTime, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_LONGITUDE, AESUtils.encrypt(String.valueOf(longitude))));
		params.add(new BasicNameValuePair(KEY_LATITUDE, AESUtils.encrypt(String.valueOf(latitude))));
		params.add(new BasicNameValuePair(KEY_DISTANCE, AESUtils.encrypt(String.valueOf(distance))));
		if(sex != null && !"".equals(sex)) {
			params.add(new BasicNameValuePair(KEY_SEX, AESUtils.encrypt(sex)));
		}
		if(startAge != 0 && endAge != 0) {
			params.add(new BasicNameValuePair(KEY_START_AGE, AESUtils.encrypt(String.valueOf(startAge))));
			params.add(new BasicNameValuePair(KEY_END_AGE, AESUtils.encrypt(String.valueOf(endAge))));
		}
		if(null != activityType && activityType.size() != 0) {
			String types = "";
			int index = 0;
			int length = activityType.size();
			while(index < length - 1) {
				types += activityType.get(index) + ",";
				index ++;
			}
			types +=  activityType.get(index);
			params.add(new BasicNameValuePair(KEY_ACTIVITY_TYPE, AESUtils.encrypt(types)));
		}
		if(constellation != null && !"".equals(constellation)) {
			params.add(new BasicNameValuePair(KEY_CONSTELLATION, AESUtils.encrypt(constellation)));
		}
		if(size != 0) {
			params.add(new BasicNameValuePair(KEY_OFFSET, AESUtils.encrypt(String.valueOf(offset))));
			params.add(new BasicNameValuePair(KEY_SIZE, AESUtils.encrypt(String.valueOf(size))));
		}
		if(beforeTime != null) {
			params.add(new BasicNameValuePair(KEY_BEFORE_TIME, AESUtils.encrypt(String.valueOf(beforeTime.getTime()))));
			
		}
		AsyncRunner.sendRequest(URL_ACTIVITY_AROUND_LIST, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 获取我报名的活动列表
	 * @param userID
	 * 		用户id
	 * @param offset
	 * 		偏移量
	 * @param size
	 * 		获取的数量
	 * @param listener
	 * 		请求返回回调
	 */
	public void getMySignUpedActivityList(long userID,  int offset, int size, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		if(size != 0) {
			params.add(new BasicNameValuePair(KEY_OFFSET, AESUtils.encrypt(String.valueOf(offset))));
			params.add(new BasicNameValuePair(KEY_SIZE, AESUtils.encrypt(String.valueOf(size))));
		}
		AsyncRunner.sendRequest(URL_ACTIVITY_SIGNUPED_LIST, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 获取用户发布的活动列表
	 * @param userID
	 * 		用户id
	 * @param offset
	 * 		偏移量
	 * @param size
	 * 		获取数量 
	 * 		为0是获取所有
	 * @param listener
	 */
	public void getMyPublishedActivityList(long userID, int offset, int size, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		if(size != 0) {
			params.add(new BasicNameValuePair(KEY_OFFSET, AESUtils.encrypt(String.valueOf(offset))));
			params.add(new BasicNameValuePair(KEY_SIZE, AESUtils.encrypt(String.valueOf(size))));
		}
		AsyncRunner.sendRequest(URL_ACTIVITY_PUBLISHED_LIST, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 获取活动的报名者列表
	 * @param userID
	 * 		用户id
	 * @param activityID
	 * 		活动id
	 * @param offset
	 * 		偏移量
	 * @param size
	 * 		获取数量 为0时获取所有
	 * @param beforeTime
	 * 		获取该时间之前的报名的报名人
	 * 		为null时获取当前时间之前的活动
	 * @param listener
	 * 		请求返回回调
	 */
	public void getActivitySignUperList(long userID, long activityID, int offset, int size, Timestamp beforeTime, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_ID, AESUtils.encrypt(String.valueOf(activityID))));
		if(size != 0) {
			params.add(new BasicNameValuePair(KEY_OFFSET, AESUtils.encrypt(String.valueOf(offset))));
			params.add(new BasicNameValuePair(KEY_SIZE, AESUtils.encrypt(String.valueOf(size))));
		} 
		if(beforeTime != null) {
			params.add(new BasicNameValuePair(KEY_BEFORE_TIME, AESUtils.encrypt(String.valueOf(beforeTime.getTime()))));
		}
		AsyncRunner.sendRequest(URL_ACTIVITY_SIGNUPER, params, AsyncRunner.HTTP_GET, listener);
	}

	/**
	 * 创建发布活动
	 * @param userID
	 * 		用户id
	 * @param activityType
	 * 		活动类型
	 * @param activityContent
	 * 		活动内容主题
	 * @param expectSex
	 * 		期望性别
	 * @param activityExplain
	 * 		活动说明
	 * @param expectedNumber
	 * 		期望人数
	 * @param activityTime	
	 * 		活动时间
	 * @param activityLongitude
	 * 		活动地点经度
	 * @param activityLatitude
	 * 		活动地点纬度
	 * @param activityPlace
	 * 		活动位置(手动输入)
	 * @param pastTime
	 * 		过期时间 
	 * 		若为null 则默认过期时间为活动时间
	 * @param picture
	 * 		活动图片
	 * @param listener
	 * 		请求返回回调
	 */
	public void createActivity(long userID, String activityType, String activityContent, String expectSex, String activityExplain, int expectedNumber, Timestamp activityTime,
			double activityLongitude, double activityLatitude, String activityPlace, Timestamp pastTime, String picture, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_TYPE, AESUtils.encrypt(activityType)));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_CONTENT, AESUtils.encrypt(activityContent)));
		params.add(new BasicNameValuePair(KEY_INVITED_OBJECT_SEX, AESUtils.encrypt(expectSex)));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_EXPLAIN, AESUtils.encrypt(activityExplain)));
		params.add(new BasicNameValuePair(KEY_EXPECTED_NUMBER, AESUtils.encrypt(String.valueOf(expectedNumber))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_TIME, AESUtils.encrypt(String.valueOf(activityTime.getTime()))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_LONGITUDE, AESUtils.encrypt(String.valueOf(activityLongitude))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_LATITUDE, AESUtils.encrypt(String.valueOf(activityLatitude))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_PLACE, AESUtils.encrypt(String.valueOf(activityPlace))));
		if(pastTime != null) {
			params.add(new BasicNameValuePair(KEY_ACTIVITY_PAST_TIME, AESUtils.encrypt(String.valueOf(pastTime.getTime()))));
		}
		params.add(new BasicNameValuePair(KEY_ACTIVITY_PICTURE, AESUtils.encrypt(picture)));
		AsyncRunner.sendRequest(URL_ACTIVITY_CREATE, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 解散活动
	 * @param userID
	 * 		用户id
	 * @param activityID
	 * 		活动id
	 * @param listener
	 * 		请求返回回调
	 */
	public void deleteActivity(long userID, long activityID, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_ID, AESUtils.encrypt(String.valueOf(activityID))));
		AsyncRunner.sendRequest(URL_ACTIVITY_DELETE, params, AsyncRunner.HTTP_POST, listener);
		
	}
	
	/**
	 * 报名活动
	 * @param userID	
	 * 		用户ID
	 * @param activityID	
	 * 		活动ID
	 * @param info	
	 * 		报名信息
	 * @param listener	
	 * 		请求返回后回调	
	 */
	public void signUpActivity(long userID, long activityID, String info, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_ID, AESUtils.encrypt(String.valueOf(activityID))));
		params.add(new BasicNameValuePair(KEY_SIGN_UP_INFO, AESUtils.encrypt(info)));
		AsyncRunner.sendRequest(URL_ACTIVITY_SIGN_UP, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 取消报名活动
	 * @param userID	
	 * 		用户ID
	 * @param activityID	
	 * 		活动ID
	 * @param listener	
	 * 		请求返回后回调	
	 */
	public void signOutActivity(long userID, long activityID, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_ID, AESUtils.encrypt(String.valueOf(activityID))));
		AsyncRunner.sendRequest(URL_ACTIVITY_SIGN_OUT, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 举报活动
	 * @param userID	
	 * 		用户ID
	 * @param activityID	
	 * 		活动ID
	 * @param reportReason	
	 * 		举报理由
	 * @param listener	
	 * 		请求返回后回调	
	 */
	public void reportActivity(long userID, long activityID, String info, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_ID, AESUtils.encrypt(String.valueOf(activityID))));
		params.add(new BasicNameValuePair(KEY_REPORT_INFO, AESUtils.encrypt(info)));
		AsyncRunner.sendRequest(URL_ACTIVITY_REPORT, params, AsyncRunner.HTTP_POST, listener);
	}
	
	/**
	 * 接受活动报名
	 * @param userID
	 * 		用户id
	 * @param activityID
	 * 		活动id
	 * @param signUperID
	 * 		报名者id
	 * @param listener
	 * 		请求返回回调
	 */
	public void acceptActivitySignUp(long userID, long activityID, long signUperID, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ACTIVITY_ID, AESUtils.encrypt(String.valueOf(activityID))));
		params.add(new BasicNameValuePair(KEY_SIGN_UPER_ID, AESUtils.encrypt(String.valueOf(signUperID))));
		AsyncRunner.sendRequest(URL_ACTIVITY_ACCEPT, params, AsyncRunner.HTTP_POST, listener);
	}
}