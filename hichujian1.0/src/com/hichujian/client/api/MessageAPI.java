package com.hichujian.client.api;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.hichujian.client.Constants;
import com.hichujian.client.utils.AESUtils;
import com.hichujian.client.utils.AsyncRunner;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
/**
 * 活动评论相关API
 * @author 307427687
 *
 */
public class MessageAPI {
	//请求URL
	public static final String URL_TALK_RECORDS = Constants.BASE_URL + "message/records";
	public static final String URL_TALK_SEND = Constants.BASE_URL + "message/send";
	//public static final String URL_TALK_CREATE = "http://115.29.228.115/Test/send";
	public static final String URL_TALK_DELETE = Constants.BASE_URL + "message/delete";
	
	//URL参数
	public static final String KEY_MESSAGE_ID = AESUtils.encrypt("msg_id");
	public static final String KEY_USER_ID = AESUtils.encrypt("user_id");
	public static final String KEY_ANOTHER_USER_ID = AESUtils.encrypt("another_user_id");
	public static final String KEY_OFFSET = AESUtils.encrypt("offset");
	public static final String KEY_SIZE = AESUtils.encrypt("size");
	public static final String KEY_BEFORE_TIME = AESUtils.encrypt("before_time");
	public static final String KEY_TO_USER_ID = AESUtils.encrypt("to_user_id");
	public static final String KEY_CONTENT = AESUtils.encrypt("content");
	public static final String KEY_TYPE = AESUtils.encrypt("type");
	public static final String KEY_SEND_TIME = AESUtils.encrypt("send_time");
	
	private static MessageAPI talkAPI;//单例
	
	private MessageAPI() {}
	
	public synchronized static MessageAPI getInstance() {
		if(null == talkAPI) {
			talkAPI = new MessageAPI();
		}
		return talkAPI;
	}
	/**
	 * 获取聊天记录列表 
	 * @param userID	
	 * 		用户ID
	 * @param anotherUserID	
	 * 		另一个用户ID
	 * @param beforTime	
	 * 		获取在该时间之前的聊天记录
	 * @param offset
	 * 		偏移量	
	 * @param size	
	 * 		获取的条数
	 * @param listener	请求返回后回调
	 */
	public void getMessageList(long userID, long anotherUserID, Timestamp beforTime, int offset, int size, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_ANOTHER_USER_ID, AESUtils.encrypt(String.valueOf(anotherUserID))));
		params.add(new BasicNameValuePair(KEY_BEFORE_TIME, AESUtils.encrypt(String.valueOf(beforTime.getTime()))));
		params.add(new BasicNameValuePair(KEY_OFFSET, AESUtils.encrypt(String.valueOf(offset))));
		params.add(new BasicNameValuePair(KEY_SIZE, AESUtils.encrypt(String.valueOf(size))));
		AsyncRunner.sendRequest(URL_TALK_RECORDS, params, AsyncRunner.HTTP_GET, listener);
	}
	
	/**
	 * 发送一条聊天消息 
	 * @param userID 
	 * 		发送者ID
	 * @param toUserID	
	 * 		接受者ID
	 * @param content	
	 * 		内容
	 * @param type	
	 * 		类型（文本、图片、语音）
	 * @param sendTime	
	 * 		发送时间
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void sendMessage(long userID, long toUserID, String content, String type, Timestamp sendTime, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_TO_USER_ID, AESUtils.encrypt(String.valueOf(toUserID))));
		params.add(new BasicNameValuePair(KEY_SEND_TIME, AESUtils.encrypt(String.valueOf(sendTime.getTime()))));
		params.add(new BasicNameValuePair(KEY_CONTENT, AESUtils.encrypt(content)));
		params.add(new BasicNameValuePair(KEY_TYPE, AESUtils.encrypt(type)));
		AsyncRunner.sendRequest(URL_TALK_SEND, params, AsyncRunner.HTTP_POST, listener);
	}
	/**
	 * 删除一条聊天消息
	 * @param userID	
	 * 		用户id
	 * @param messageID	
	 * 		聊天消息id
	 * @param listener	
	 * 		请求返回后回调
	 */
	public void deleteMessage(long userID, long messageID, OnResponseListener listener) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(KEY_USER_ID, AESUtils.encrypt(String.valueOf(userID))));
		params.add(new BasicNameValuePair(KEY_MESSAGE_ID, AESUtils.encrypt(String.valueOf(messageID))));
		AsyncRunner.sendRequest(URL_TALK_DELETE, params, AsyncRunner.HTTP_POST, listener);
	}
}