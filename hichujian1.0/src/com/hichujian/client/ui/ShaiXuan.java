package com.hichujian.client.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hichujian.R;

public class ShaiXuan extends Activity{
	private static int value=1;
	boolean flag1=false;		//all
	boolean flag2=false;		//sporting
	boolean flag3=false;		//traveling
	boolean flag4=false;		//reading
	boolean flag5=false;		//eating
	boolean flag6=false;		//singing
	boolean flag7=false;		//tablegame
	boolean flag8=false;		//moving
	boolean flag9=false;		//computergame
	private LinearLayout all,sporting,traveling,reading,eating,singing,tablegame,moving,computergame;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.shuaixuan);
		//获取不限，男，女3个
		final TextView buxian2 = (TextView)findViewById(R.id.buxian2);
		final TextView man2 = (TextView)findViewById(R.id.man2);
		final TextView woman2 = (TextView)findViewById(R.id.woman2);
		//不限
        buxian2.setOnClickListener(new ImageButton.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				value=3;
				//不限的设置背景
				buxian2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_buxian_gray_shape));
				man2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
				woman2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_woman_white_shape));
			}
		});
        
        //男
        man2.setOnClickListener(new TextView.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				value=1;
				//男的设置背景
				buxian2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_buxian_white_shape));
				man2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
				woman2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_woman_white_shape));

			}
		});
        //女
        woman2.setOnClickListener(new TextView.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				value=2;
				//女的设置背景
				buxian2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_buxian_white_shape));
				man2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
				woman2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_woman_gray_shape));
			}
		});
        
        //筛选全部活动
        all=(LinearLayout)findViewById(R.id.all);
        sporting=(LinearLayout)findViewById(R.id.sporting);
        traveling=(LinearLayout)findViewById(R.id.traveling);
        reading=(LinearLayout)findViewById(R.id.reading);
        eating=(LinearLayout)findViewById(R.id.eating);
        singing=(LinearLayout)findViewById(R.id.singing);
        tablegame=(LinearLayout)findViewById(R.id.tablegame);
        moving=(LinearLayout)findViewById(R.id.moving);
        computergame=(LinearLayout)findViewById(R.id.computergame);
        
        all.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(flag1==true){	//已经选中
					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
					sporting.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					traveling.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_right_white_shape));
					reading.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					eating.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					singing.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					tablegame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_bottom_left_white_shape));
					moving.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					computergame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_bottom_right_white_shape));
					flag1=false;
					flag2=false;
					flag3=false;
					flag4=false;
					flag5=false;
					flag6=false;
					flag7=false;
					flag8=false;
					flag9=false;
				}else{	//未选中
					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_gray_shape));
					sporting.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					traveling.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_right_gray_shape));
					reading.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					eating.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					singing.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					tablegame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_bottom_left_gray_shape));
					moving.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					computergame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_bottom_right_gray_shape));
					flag1=true;
					flag2=true;
					flag3=true;
					flag4=true;
					flag5=true;
					flag6=true;
					flag7=true;
					flag8=true;
					flag9=true;
				}
			}
		});
        
        sporting.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(flag2==true){	//已经选中
					sporting.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
					flag1=false;
					flag2=false;
				}else{	//未选中
					sporting.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					flag2=true;
					
				}
			}
		});
        
        traveling.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(flag3==true){	//已经选中
					traveling.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
					flag1=false;
					flag3=false;
				}else{	//未选中
					traveling.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					flag3=true;
				}
			}
		});
        
        reading.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(flag4==true){	//已经选中
					reading.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
					flag1=false;
					flag4=false;
				}else{	//未选中
					reading.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					flag4=true;
				}
			}
		});
        
        eating.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(flag5==true){	//已经选中
					eating.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
					flag1=false;
					flag5=false;
				}else{	//未选中
					eating.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					flag5=true;
				}
			}
		});
        
        singing.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(flag6==true){	//已经选中
					singing.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
					flag1=false;
					flag6=false;
				}else{	//未选中
					singing.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					flag6=true;
				}
			}
		});
        
        tablegame.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(flag7==true){	//已经选中
					tablegame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
					flag1=false;
					flag7=false;
				}else{	//未选中
					tablegame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					flag7=true;
				}
			}
		});
        
        moving.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(flag8==true){	//已经选中
					moving.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
					flag1=false;
					flag8=false;
				}else{	//未选中
					moving.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					flag8=true;
				}
			}
		});
        
        computergame.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(flag9==true){	//已经选中
					computergame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
					flag1=false;
					flag9=false;
				}else{	//未选中
					computergame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
					flag9=true;
				}
			}
		});
        //取消
        TextView shuaixuan_cancel = (TextView)findViewById(R.id.shuaixuan_cancel);
        shuaixuan_cancel.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
        //确定
        TextView confirm = (TextView)findViewById(R.id.confirm);
        confirm.setOnClickListener(new LinearLayout.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = getIntent();
				Bundle bundle = new Bundle();
				bundle.putInt("value", value);
				
				bundle.putBoolean("flag1",flag1);
				bundle.putBoolean("flag2",flag2);
				bundle.putBoolean("flag3",flag3);
				bundle.putBoolean("flag4",flag4);
				bundle.putBoolean("flag5",flag5);
				bundle.putBoolean("flag6",flag6);
				bundle.putBoolean("flag7",flag7);
				bundle.putBoolean("flag8",flag8);
				bundle.putBoolean("flag9",flag9);
				
				intent.putExtras(bundle);
				setResult(0x11,intent);			
				finish();
			}
		});
        
	}//end of onCreate
	
}