package com.hichujian.client.ui;


import com.example.hichujian.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Tab extends FragmentActivity {

	protected static final String TAG = "MainActivity";
	// 未读消息textview
	private TextView unreadLabel;
	// 未读通讯录textview
	private TextView unreadAddressLable;

	private Button[] mTabs;
	private Fragment[] fragments;
	private int index;
	private RelativeLayout[] tab_containers;
	private TabMyMessage contactListFragment;
	private TabSquare chatHistoryFragment;
	private TabMySetting settingFragment;
	private TabMyActivity  btn_message;
	
	// 当前fragment的index
	private int currentTabIndex;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dating_new);
		initView();
		chatHistoryFragment = new TabSquare();
		btn_message=new TabMyActivity();
		contactListFragment = new TabMyMessage();
		settingFragment = new TabMySetting();
		fragments = new Fragment[] {chatHistoryFragment,btn_message, contactListFragment, settingFragment };
		// 添加显示第一个fragment
		getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, chatHistoryFragment)
				.add(R.id.fragment_container, btn_message).hide(btn_message).show(chatHistoryFragment).commit();

	}//end of onCreate

	/**
	 * 初始化组件   btn_message
	 */
	private void initView() {
		unreadLabel = (TextView) findViewById(R.id.unread_msg_number);
		unreadAddressLable = (TextView) findViewById(R.id.unread_address_number);
		mTabs = new Button[4];
		mTabs[0] = (Button) findViewById(R.id.btn_conversation);
		mTabs[1] = (Button) findViewById(R.id.btn_message);
		mTabs[2] = (Button) findViewById(R.id.btn_address_list);
		mTabs[3] = (Button) findViewById(R.id.btn_setting);
		// 把第一个tab设为选中状态
		mTabs[0].setSelected(true);

	}

	/**
	 * button点击事件
	 * 
	 * @param view
	 */
	public void onTabClicked(View view) {
		switch (view.getId()) {
		case R.id.btn_conversation:
			index = 0;
			break;
		case R.id.btn_message:
			index = 1;
			break;
		case R.id.btn_address_list:
			index = 2;
			break;
		case R.id.btn_setting:
			index = 3;
			break;
		}
		if (currentTabIndex != index) {
			FragmentTransaction trx = getSupportFragmentManager().beginTransaction();
			trx.hide(fragments[currentTabIndex]);
			if (!fragments[index].isAdded()) {
				trx.add(R.id.fragment_container, fragments[index]);
			}
			trx.show(fragments[index]).commit();
		}
		mTabs[currentTabIndex].setSelected(false);
		// 把当前tab设为选中状态
		mTabs[index].setSelected(true);
		currentTabIndex = index;
	}

	
	}

