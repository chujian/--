package com.hichujian.client.ui;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.UserAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.model.UserDO;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.ImageUtil;
import com.hichujian.client.utils.States;

public class PersonDetails extends Activity{
	private Long UserID;
	private static final int MAX_THREAD_COUNT = 5;//最大线程数量
	private static Handler handlerBitmap;
	private static ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREAD_COUNT);
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.person_details);
        
        SharedPreferences preferences = getSharedPreferences("User" , Context. MODE_PRIVATE);
        String UserId = preferences.getString( "UserId" , "" );
        UserID = Long. valueOf(UserId);
        Log. i( "tag",Long.valueOf(UserId)+ "  UserId Long型");
        Log. i( "tag", UserId+"hichujian_person_details 用户ID");
        
      //数据接收
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        Long UserIDActivity = bundle.getLong( "UserIDActivity");
        final String ActivityContent = bundle.getString( "ActivityContent");
        final String ActivityTime = bundle.getString( "ActivityTime");
        final String ActivityPlace = bundle.getString( "ActivityPlace");
        final int CommentNumber = bundle.getInt( "CommentNumber");
        final int SignUpNumber = bundle.getInt( "SignUpNumber");
        final double Distance = bundle.getDouble( "Distance");
        
        Log.i("tag",Long.valueOf(UserIDActivity)+ "   Long型UserIDActivity" );
        
        Log.i("tag",String.valueOf(UserIDActivity)+"     发起活动的用户ID101");
        //获取用户的一些信息
        final TextView birthday = (TextView)findViewById(R.id.birthday);
        final TextView nickname = (TextView)findViewById(R.id.person_nickname);
        final TextView person_sex = (TextView)findViewById(R.id.person_sex);
        final TextView emotionState = (TextView)findViewById(R.id.emotionState);
        final TextView interestHobbies = (TextView)findViewById(R.id.interestHobbies);
        final TextView usuallyAppearArea = (TextView)findViewById(R.id.usuallyAppearArea);
        final TextView location = (TextView)findViewById(R.id.location);
        
        //获取活动的信息
        final TextView activityContent = (TextView)findViewById(R.id.activityContent);
        final TextView activityTime = (TextView)findViewById(R.id.activityTime);
        final TextView activityPlace = (TextView)findViewById(R.id.activityPlace);
        final TextView commentNumber = (TextView)findViewById(R.id.commentNumber);
        final TextView signUpNumber = (TextView)findViewById(R.id.signUpNumber);
        final TextView distance = (TextView)findViewById(R.id.distance);
        
        
        final ImageView person_detail_head1 = (ImageView)findViewById(R.id.person_detail_head1);
        ImageView person_detail_head2 = (ImageView)findViewById(R.id.person_detail_head2);
        ImageView person_detail_head3 = (ImageView)findViewById(R.id.person_detail_head3);
        ImageView person_detail_head4 = (ImageView)findViewById(R.id.person_detail_head4);
        ImageView person_detail_head5 = (ImageView)findViewById(R.id.person_detail_head5);
        ImageView person_detail_head6 = (ImageView)findViewById(R.id.person_detail_head6);
        ImageView person_detail_head7 = (ImageView)findViewById(R.id.person_detail_head7);
        ImageView person_detail_head8 = (ImageView)findViewById(R.id.person_detail_head8);
        final ImageView mySettinghead[] = {person_detail_head1,person_detail_head2,person_detail_head3,person_detail_head4,person_detail_head5,person_detail_head6,person_detail_head7,person_detail_head8};
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
               UserDO user = (UserDO)msg.obj;
               String Nickname = user.getNickname();//昵称
               Date Birthday = user.getBirthday();//生日
               Calendar c = Calendar.getInstance();
               int ageInt = c.get(Calendar.YEAR)-user.getBirthday().getYear();//2014-1992获得年龄
               String EmotionState = user.getEmotionState();	//情感状态
               String InterestHobbies = user.getInterestHobbies();//兴趣
               String Units = user.getUnits();//单位
               String Sex = user.getSex();//性别
               String UsuallyAppearArea = user.getUsuallyAppearArea();
               String Location = user.getLocation();
               String HeadPortrait = user.getHeadPortrait();//头像URL
               String PhotoUri = user.getPhoto();	//获取照片Uri
				//把数据扔进去
                Drawable drawable;	//设置性别
				if(Sex.equals("男")){
					drawable= getResources().getDrawable(R.drawable.activity_man);  
				}else{
					drawable= getResources().getDrawable(R.drawable.activity_woman);  
				}
				/// 这一步必须要做,否则不会显示.  
				drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());  
				person_sex.setCompoundDrawables(drawable,null,null,null); 
                nickname.setText(Nickname);	//设置昵称
                person_sex.setText(ageInt+"");//设置年龄
                birthday.setText(Birthday+"");//设置生日
                								//语音
                location.setText(Location+"");		//地区
                emotionState.setText(EmotionState);//设置情感状态
                interestHobbies.setText(InterestHobbies);//设置兴趣爱好
                usuallyAppearArea.setText(UsuallyAppearArea);//常出没的地方
                //设置6个活动方面的信息
                activityContent.setText("  "+ActivityContent);//设置活动内容
                activityTime.setText("  "+ActivityTime);//设置活动时间
                activityPlace.setText("   "+ActivityPlace);//设置活动地点
                commentNumber.setText(CommentNumber+"");//设置评论数
                signUpNumber.setText(SignUpNumber+"");//设置报名数
                distance.setText(Distance+"km");//设置距离
                
                String Imageurl = user.getPhoto();//获得图片的Url
                Log.i("tag",Imageurl+"Imageurl");
                String[] url = Imageurl.split("\\s*;\\s*");
                Log.i("tag",String.valueOf(url.length));
                
                for(int i =0;i<url.length;i++){
             	   Log.i("tag", "图片"+i+"完成");
             	   loadImageAndShow(url[i],i);
             	   Log.i("tag",url[i]+"PersonDetails"+i);
                }
                
            }
        }; 

        
        TextView wechat=(TextView)findViewById(R.id.wechat);
        wechat.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent();
                intent.setClass( PersonDetails. this,  Chatting.class);
                PersonDetails.this.startActivity(intent);
				
			}
		});
        
        UserAPI userAPI = UserAPI.getInstance();
        Log.i("tag", "userID:" + UserID + ":" + "FriendUesrID:" + UserIDActivity);
        userAPI.getFriendUserInfo(UserID, UserIDActivity, new OnResponseListener() {
        	
			
			@Override
			public void onResponse(String data) {
				// TODO Auto-generated method stub
				Log.i("tag","请求成功");
				ResponseDO result = JSON.parseObject(data,ResponseDO.class);
				if(result.getResultCode()==States.GET_USER_INFO_SUCCESSFUL){	//获取用户信息成功
					UserDO user= JSON.parseObject(result.getData().toString(), UserDO.class);
					Log.i("tag", result.toString()+"result.getData().toString()");
					Log.i("tag", user.getNickname());
					
					//发送消息
					Message msg = new Message();
					msg.obj=user ;
					handler.sendMessage(msg); 
				}else if(result.getResultCode()==States.GET_USER_INFO_SUCCESSFUL){	//获取用户信息不存在
					Log.i("tag", "获取的用户不存在");
				}else if(result.getResultCode()==States.GET_USER_INFO_OTHER_ERROR){//获取用户信息其他错误
					Log.i("tag", "获取用户信息其他错误");
				}
			}
			
			@Override
			public void onError(String info) {
				// TODO Auto-generated method stub
				Log.i("tag","请求失败");
			}
		});	//获取个人详情的回调结束
        
        ImageButton back_to_login8=(ImageButton)findViewById(R.id.back_to_login8);
        back_to_login8.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
        
        handlerBitmap = new Handler() {
            @Override
            public void handleMessage(Message msg) {
              int i = msg.what;
              Bitmap b = (Bitmap)msg.obj;
             //   Log. i ("tag" , String.valueOf(msg.obj.toString()));
              mySettinghead[i].setImageBitmap(b);
            }
        };
	}//end of onCreate
	
	
	/**
	 * 下载图片并且显示到imageview上
	 * @param imgUrl 图片的url
	 * @param imgv	显示图片的ImageView对象
	 */
	public static void loadImageAndShow(final String imgUrl, final int i) {
		Runnable task = new Runnable() {
			public void run() {
				Log.i("tag", "头像加载开始：");
				InputStream is = null;
				HttpURLConnection conn = null;
				try {
					 conn = (HttpURLConnection) (new URL(imgUrl)).openConnection();
					 conn.setDoInput(true);
					 conn.connect();
					 is = conn.getInputStream();
					 final Bitmap b = BitmapFactory.decodeStream(is);
					 
					//发送消息
					 Message msg = new Message();
					 msg.obj=b;
					 msg.what = i;
					 handlerBitmap.sendMessage(msg); 

					// imgv.setImageBitmap(b);
					 Log.i("tag", "图片加载完毕!");
				} catch(Exception e) {
					e.printStackTrace();
				} finally {
					try {
						if(is != null)
							is.close();
						if(conn != null)
							conn.disconnect();
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		};
		executorService.execute(task);
	}
}