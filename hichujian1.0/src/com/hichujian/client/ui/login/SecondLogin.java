package com.hichujian.client.ui.login;

import com.example.hichujian.R;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SecondLogin extends Activity{
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.second_login);
        
        final EditText password3 = (EditText) findViewById(R.id.password3);
    	final Button login_clear_bt3 = (Button) findViewById(R.id.login_clear_bt3);
    	password3.addTextChangedListener(new TextWatcher() {
    		@Override
    		public void afterTextChanged(Editable s) {
    			if (password3.length() == 0) {
    				login_clear_bt3.setVisibility(View.GONE);
    			} else {
    				login_clear_bt3.setVisibility(View.VISIBLE);
    			}
    		}

    		@Override
    		public void beforeTextChanged(CharSequence s, int start, int count,
    				int after) {
    			// TODO Auto-generated method stub
    		}

    		@Override
    		public void onTextChanged(CharSequence s, int start, int before,
    				int count) {
    			// TODO Auto-generated method stub
    		}
    	});

    	login_clear_bt3.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View v) {
    			password3.setText(null);
    		}
    	});
    	
    	//失去焦点处理
    	password3.setOnFocusChangeListener( new android.view.View.OnFocusChangeListener() { 
                @Override 
                public void onFocusChange(View v, boolean hasFocus) { 
                             if(hasFocus) {
                            	// 此处为得到焦点时的处理内容
                                    if (password3.length() == 0) {
                                                  login_clear_bt3.setVisibility(View.GONE);
                                          } else {
                                                  login_clear_bt3.setVisibility(View.VISIBLE);
                                          }
                             } else {
                            	// 此处为失去焦点时的处理内容
                                    login_clear_bt3.setVisibility(View.GONE);
                             }
                         }
                     });

}
}