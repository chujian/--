package com.hichujian.client.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.UserAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class ForgetPassword extends Activity{
	private ImageButton back_to_login2;
	private TextView next_to_forgetpassword;
	private TimeCount time1;
	private Button btn_get_identifying_code1;
	private EditText forget_password_phone;
	private Button btn_get_identifying_code2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		setContentView(R.layout.forget_password);
		//主函数接受对象
		final Handler handler = new Handler() { 
		         @Override 
		         public void handleMessage(Message msg) { 
		             if(msg.what==States.AUTHCODE_CHECK_EXPIRED){	//验证码过期
		            	 Toast. makeText(ForgetPassword.this, "验证码过期" , Toast.LENGTH_SHORT).show(); 
		            	 Log.i ("tag" ,"验证码过期");
		             }
		             if(msg.what==States.AUTHCODE_CHECK_CODE_ERROR){	//输入验证码错误
		            	 Toast. makeText(ForgetPassword.this, "输入验证码错误" , Toast.LENGTH_SHORT).show(); 
		            	 Log.i ("tag" ,"输入验证码错误");
		             }
		             if(msg.what==States.REGISTER_PHONE_NOT_EXISTS){	//手机号未注册
		            	 Toast. makeText(ForgetPassword.this, "手机号未注册" , Toast.LENGTH_SHORT).show(); 
		            	 Log.i ("tag" ,"手机号未注册");
		             }
		         } 
		     }; 

		//返回
		ImageButton back_to_login8=(ImageButton) findViewById(R.id.back_to_login8);  
		back_to_login8.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
		//获取号码
		final EditText forget_password_phone=(EditText)findViewById(R.id.forget_password_phone);
		final EditText identifying_code2=(EditText)findViewById(R.id.identifying_code2);
		//下一步
		Button next_to_forgetpassword=(Button) findViewById(R.id.next_to_forgetpassword);  
		next_to_forgetpassword.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {		
				/*String phone = forget_password_phone.getText().toString();
				Intent intent=new Intent();
				intent.setClass( hichujian_forget_password. this,  hichujian_forget_second_password.class);
				Bundle bundle=new Bundle();
				bundle.putCharSequence("phone",phone);
				intent.putExtras(bundle);
				Log.i("tag", phone);
				hichujian_forget_password.this.startActivity(intent);*/

				
				if(forget_password_phone.length()==11){	//已经输入手机号
					if(identifying_code2.length()==6){	//输入验证码
						final UserAPI userAPI=UserAPI.getInstance();
						final String phone=forget_password_phone.getText().toString();
						String authCode=identifying_code2.getText().toString();
						userAPI.checkAuthCode(phone, authCode, new OnResponseListener() {

							@Override
							public void onResponse(String data) {
								// TODO Auto-generated method stub
								ResponseDO result=JSON.parseObject(data,ResponseDO.class);
								Log.i("tag", String.valueOf(result.getResultCode()));
								if(result.getResultCode()==States.AUTHCODE_CHECK_SUCCESSFUL){	//验证验证码成功
									Intent intent=new Intent();
									intent.setClass( ForgetPassword. this,  ForgetSecondPassword.class);
									Bundle bundle=new Bundle();
									bundle.putCharSequence("phone",phone);
									intent.putExtras(bundle);
									Log.i("tag", phone);
									ForgetPassword.this.startActivity(intent);
								}else if(result.getResultCode()==States.AUTHCODE_CHECK_EXPIRED){	//主线程提示验证码过期
									Log.i("tag", "验证码过期");
									Message msg = new Message(); 
									msg.what=States.AUTHCODE_CHECK_EXPIRED; 
									handler.sendMessage(msg); 
								}else if(result.getResultCode()==States.AUTHCODE_CHECK_CODE_ERROR){		//输入验证码错误提醒
									Log.i("tag", "验证码错误");
									Message msg = new Message(); 
									msg. what =States.AUTHCODE_CHECK_CODE_ERROR; 
									handler.sendMessage(msg); 

								}
							}

							@Override
							public void onError(String info) {
								// TODO Auto-generated method stub

							}
						});	//验证短信验证码是否正确
					}else{//验证码未输入或者不完整
						Toast. makeText(ForgetPassword.this, "请输入6位验证码" , Toast.LENGTH_SHORT).show();
					}
				}else {//手机号未输入或者不完整
					Toast. makeText(ForgetPassword.this, "请输入11位手机号" , Toast.LENGTH_SHORT).show();
				}//end of if
				 

			}
		});

		//构造倒计时
		time1=new TimeCount(60000, 1000);	

		//点击获取验证码按钮
		btn_get_identifying_code2=(Button)findViewById(R.id.btn_get_identifying_code2);
		btn_get_identifying_code2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				final UserAPI userAPI = UserAPI.getInstance();
				if(forget_password_phone.length()==11){
					String phoneNumber=forget_password_phone.getText().toString();
					//判断用户手机号是否已经注册
					userAPI.checkRegister(phoneNumber, new OnResponseListener() {
						@Override
						public void onResponse(String data) {
							// TODO Auto-generated method stub
							Log.i("tag","请求成功");
							ResponseDO result=JSON.parseObject(data, ResponseDO.class);
							Log.i("tag","验证是否注册"+String.valueOf(result.getResultCode()));
							//如果手机号已经注册过了
							if(result.getResultCode()==States.REGISTER_PHONE_EXISTS){	//已经注册才能忘记密码

								time1.start();
								String phoneNumber=forget_password_phone.getText().toString();

								userAPI.getAuthCode(phoneNumber, new OnResponseListener() {
									@Override
									public void onResponse(String data) {
										// TODO Auto-generated method stub
										ResponseDO result=JSON.parseObject(data, ResponseDO.class);
										Log.i("tag",String.valueOf(result.getResultCode()));
										if(result.getResultCode()==States.AUTHCODE_GET_SUCCESSFUL){
											Log.i("tag", "发送成功");
										}
										if(result.getResultCode()==States.AUTHCODE_GET_ERROR){
											Log.i("tag", "发送失败");
										}
									}

									@Override
									public void onError(String info) {
										// TODO Auto-generated method stub
										Log.i("tag", "请求发送失败");
									}
								});	//验证码回调


							}else{	//手机号未被注册过
								//未注册过要弹出提示窗口
								Message msg = new Message(); 
								msg.what=States.REGISTER_PHONE_NOT_EXISTS;//result.getData().toString(); 
								handler.sendMessage(msg); 
								Log.i("tag", "手机号未注册");

							}
						}

						@Override
						public void onError(String info) {
							// TODO Auto-generated method stub
							Log.i("tag","请求失败");
						}
					});




				}	//end of if  
				else if(forget_password_phone.length()==0){
					Toast. makeText(ForgetPassword.this, "手机号长度不能为空" , Toast.LENGTH_SHORT).show();
				}else{
					Toast. makeText(ForgetPassword.this, "请输入11位手机号" , Toast.LENGTH_SHORT).show();
				}
			}
		});	//获取验证码按钮结束



	}//end of onCreate

	/* 定义一个倒计时的内部类 */
	class TimeCount extends CountDownTimer {
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
		}
		@Override
		public void onFinish() {//计时完毕时触发
			btn_get_identifying_code2.setText("重新验证");
			btn_get_identifying_code2.setClickable(true);
		}
		@Override
		public void onTick(long millisUntilFinished){//计时过程显示
			btn_get_identifying_code2.setClickable(false);
			btn_get_identifying_code2.setText(millisUntilFinished /1000+"秒");
		}
	}
}
