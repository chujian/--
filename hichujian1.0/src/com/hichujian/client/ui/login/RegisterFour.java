package com.hichujian.client.ui.login;


import java.sql.Date;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.UserAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.ui.Jiugongge;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class RegisterFour extends Activity{
	private ImageButton back_to_login2;
	private TextView register_finish;
	private ImageView register_head;
	private boolean sexFlag=true;	//true 表示男， false表示女
	private String sex = "男";			//表示性别字符串
	private Bitmap bitmap;
	private int year1;	//年
	private int month1;//月
	private int day1;//日

	/*private BMapManager mapManager;
	private MKLocationManager mLocationManager = null;*/
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.register_four);
        
        /*// 初始化MapActivity
        mapManager = new BMapManager(getApplication());
        // init方法的第一个参数需填入申请的API Key
        mapManager.init("C66C0501D0280744759A6957C42543AE38F5D540", null);//C66C0501D0280744759A6957C42543AE38F5D540
        super.initMapActivity(mapManager);

        mLocationManager = mapManager.getLocationManager();
        // 注册位置更新事件
        mLocationManager.requestLocationUpdates(this);
        // 使用GPS定位
        mLocationManager.enableProvider((int) MKLocationManager.MK_GPS_PROVIDER);*/
        
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        final String phoneNumber = bundle.getString("phone");
        final String password = bundle.getString("password");
        final Handler handler = new Handler() { 
            @Override 
            public void handleMessage(Message msg) { 
            	if(msg.what ==States.REGISTER_ERROR){     //注册错误，失败
            		Toast. makeText(RegisterFour.this, "注册失败" , Toast.LENGTH_SHORT).show();
            		Log. i ("tag" , "注册失败" );
            		}
            	if(msg.what ==States.REGISTER_SUCCESSFUL){     //注册成功
            		Toast. makeText(RegisterFour.this, "注册成功了哈~" , Toast.LENGTH_SHORT).show();
            		Log. i ("tag" , "注册成功了哈~" );
            		}
            } 
        }; 

        //返回按钮
        ImageButton back_to_login7=(ImageButton) findViewById(R.id.back_to_login7);  
        back_to_login7.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	finish();
           }
        });
        
      //设置生日 
        final TextView date=(TextView) findViewById(R.id.date);  
         date.setOnClickListener( new OnClickListener() {
             public void onClick(View v) {
             	Calendar c = Calendar. getInstance();
                 // 直接创建一个DatePickerDialog对话框实例，并将它显示出来
                 new DatePickerDialog(RegisterFour.this,
                        // 绑定监听器
                        new DatePickerDialog.OnDateSetListener()
                       {
                               @Override
                               public void onDateSet(DatePicker dp, int year,
                                      int month, int dayOfMonth)
                              {
                            	     year1=year;
                            	     month1=month+1;
                            	     day1=dayOfMonth;
                                     TextView show = (TextView) findViewById(R.id.date );
                                     show.setText(  year1 + "年" + month1
                                            + "月" + day1 + "日" );
                                     
                              }
                       }
                 //设置初始日期
                , c.get(Calendar. YEAR)
                , c.get(Calendar. MONTH)
                , c.get(Calendar. DAY_OF_MONTH)).show();
             
            }
         });
         //选择男性
         final LinearLayout man=(LinearLayout)findViewById(R.id.man);
         final LinearLayout woman=(LinearLayout)findViewById(R.id.woman);
         final TextView man_text=(TextView)findViewById(R.id.man_text);
         final TextView woman_text=(TextView)findViewById(R.id.woman_text);
         final ImageView man_image=(ImageView)findViewById(R.id.man_image);
         final ImageView woman_image=(ImageView)findViewById(R.id.woman_image);
         man.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View arg0) {
 				sexFlag = true;
 				man.setBackgroundDrawable(getResources().getDrawable(R.drawable.man_sex_checked));
 				man_text.setTextColor(android.graphics.Color.parseColor("#FFFFFFFF"));
 				man_image.setBackgroundDrawable(getResources().getDrawable(R.drawable.male_select));
 				woman.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
 				woman_text.setTextColor(android.graphics.Color.parseColor("#9f9f9f"));
 				woman_image.setBackgroundDrawable(getResources().getDrawable(R.drawable.female_unselect));
 			}
 		});
       //选择女性
         woman.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View arg0) {
 				sexFlag = false;
 				woman.setBackgroundDrawable(getResources().getDrawable(R.drawable.women_sex_checked));
 				woman_text.setTextColor(android.graphics.Color.parseColor("#FFFFFFFF"));
 				woman_image.setBackgroundDrawable(getResources().getDrawable(R.drawable.female_select));
 				man.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
 				man_text.setTextColor(android.graphics.Color.parseColor("#9f9f9f"));
 				man_image.setBackgroundDrawable(getResources().getDrawable(R.drawable.male_unselect));
 			}
 		});
        
        //昵称
        final EditText nickname=(EditText)findViewById(R.id.nickname);
        //性别
        if(sexFlag==true){
        	sex = "男";
        }else{
        	sex = "女";
        }
        //生日
       // final Date date1 = new Date(year1,month1,day1);
       // final  Date date1 = Date.valueOf(year1 + "-" + month1 + "-" + day1);
        //完成按钮
        Button btn_register_finish=(Button) findViewById(R.id.btn_register_finish);  
        btn_register_finish.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	
            	
                final String nickname_string = nickname.getText().toString();
              //  String headPortrait=hichujian_register_three.picture_youbaiyun_uri;
            	final UserAPI userAPI = UserAPI.getInstance();
            	//注册接口，用于注册信息
            	
            	
            	String setbirdayString = date.getText().toString();
            	Date date1 = Date.valueOf(setbirdayString.trim());
            	
            	Log.i("tag", RegisterThree.picture_youbaiyun_uri);
            	while(RegisterThree.picture_youbaiyun_uri==null){
            		Log.i("tag", "dengdaishangchuang");
            		try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
                userAPI.register(phoneNumber, password, nickname_string, RegisterThree.picture_youbaiyun_uri, sex, date1, new OnResponseListener() {
					
					@Override
					public void onResponse(String data) {
						// TODO Auto-generated method stub
						Log.i("tag","请求成功");
						ResponseDO result=JSON.parseObject(data,ResponseDO.class);
						if(result.getResultCode()==States.REGISTER_SUCCESSFUL){	//注册成功
							//完成跳转九宫格
							Intent intent=new Intent();
							intent.setClass( RegisterFour. this,  Jiugongge.class);
							
							//把用户ID数据存入SharePreferences
                            SharedPreferences preferences = getSharedPreferences("User" , Context.MODE_PRIVATE);
                            Editor editor = preferences.edit();
                            editor.putString( "UserId", result.getData().toString().split(":", 2)[0]);
                            editor.commit();
                            Log. i("tag", "存入SharePreferences后值为" + result.getData().toString());
							
                          //把用户手机号数据存入SharePreferences
                            SharedPreferences preferences3 = getSharedPreferences("phonenumber" , Context.MODE_PRIVATE);
                            Editor editor3 = preferences3.edit();
                            editor.putString( "PhoneNumber", phoneNumber);
                            editor.commit();
                            Log. i("tag", "存入SharePreferences后值为" + phoneNumber);
                            RegisterFour.this.startActivity(intent);
						}
						if(result.getResultCode()==States.REGISTER_ERROR){	//注册失败
							Message msg = new Message();
							msg. what=States.REGISTER_ERROR ;
							handler.sendMessage(msg); 
						}
					}
					
					@Override
					public void onError(String info) {
						// TODO Auto-generated method stub
						Log.i("tag","请求失败");
					}
				});	//注册接口
              
                
            }
            });	//完成按钮
	}
	
	
	
	/*protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	protected void onDestroy() {
		if (mapManager != null) {
			mapManager.destroy();
			mapManager = null;
		}
		mLocationManager = null;
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		if (mapManager != null) {
			mapManager.stop();
		}
		super.onPause();
	}

	@Override
	protected void onResume() {
		if (mapManager != null) {
			mapManager.start();
		}
		super.onResume();
	}

	*//**
	 * 根据MyLocationOverlay配置的属性确定是否在地图上显示当前位置
	 *//*
	protected boolean isLocationDisplayed() {
		return false;
	}

	*//**
	 * 当位置发生变化时触发此方法
	 * 
	 * @param location 当前位置
	 *//*
	public void onLocationChanged(Location location) {
		if (location != null) {
			// 显示定位结果
			Log.i("tag","当前纬度：" + location.getLatitude());
			Log.i("tag","当前经度：" + location.getLongitude());
		}
	}*/
}
