package com.hichujian.client.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.UserAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.ClassPathResource;
import com.hichujian.client.utils.States;

public class RegisterFirst extends Activity{
	private ImageButton back_to_login;
	private TextView next_to_register;
	private TimeCount time;
	private  
	Button btn_get_identifying_code;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		setContentView(R.layout.register_first);
		//获取输入的验证码
		final EditText phonenumber=(EditText)findViewById(R.id.phonenumber);
		//验证码
		final EditText identifying_code=(EditText)findViewById(R.id.identifying_code);
		//返回
		ImageButton back_to_login4=(ImageButton) findViewById(R.id.back_to_login4);  
		back_to_login4.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				RegisterFirst.this.finish();
			}
		});
		//Handler更行UI线程
		final Handler handler = new Handler() { 
	         @Override 
	         public void handleMessage(Message msg) { 
	        	 if(msg.what ==States.AUTHCODE_CHECK_EXPIRED){     //验证码过期
	        		 Toast. makeText(RegisterFirst.this, "验证码过期" , Toast.LENGTH_SHORT).show();
	        		 Log. i ("tag" , "验证码过期" );
	        		 }
	        	 if(msg.what ==States.AUTHCODE_CHECK_CODE_ERROR){     //验证码错误
	        		 Toast. makeText(RegisterFirst.this, "验证码错误" , Toast.LENGTH_SHORT).show();
	        		 Log. i ("tag" , "验证码错误" );
	        		 } 
	        	 if(msg.what ==123456){     //不符合正则表达式
	        		 Toast. makeText(RegisterFirst.this, "输入的不是手机号" , Toast.LENGTH_SHORT).show();
	        		 Log. i ("tag" , "输入的不是手机号" );
	        		 } 
	        	 
	        	 if(msg.what ==States.REGISTER_PHONE_EXISTS){     //已经注册
	        		 Toast. makeText(RegisterFirst.this, "手机号已经注册" , Toast.LENGTH_SHORT).show();
	        		 Log. i ("tag" , "输入的不是手机号" );
	        		 } 
	         } 
	     }; 

		//下一步
		Button next_to_register=(Button) findViewById(R.id.next_to_register);  
		next_to_register.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				
				/*String phone = phonenumber.getText().toString();
				Intent intent=new Intent();
				intent.setClass( RegisterFirst. this,  RegisterSecond.class);
				Bundle bundle=new Bundle();
				bundle.putCharSequence("phone",phone);
				intent.putExtras(bundle);
				Log.i("tag", phone);
				RegisterFirst.this.startActivity(intent);*/
				
				if(phonenumber.length()==11){	//已经输入手机号
					if(identifying_code.length()==6){	//输入验证码
						final UserAPI userAPI=UserAPI.getInstance();
						final String phone=phonenumber.getText().toString();
						String authCode=identifying_code.getText().toString();
						userAPI.checkAuthCode(phone, authCode, new OnResponseListener() {

							@Override
							public void onResponse(String data) {
								// TODO Auto-generated method stub
								ResponseDO result=JSON.parseObject(data,ResponseDO.class);
								Log.i("tag",String.valueOf(result.getResultCode()));
								if(result.getResultCode()==States.AUTHCODE_CHECK_SUCCESSFUL){	//验证验证码成功
									Intent intent=new Intent();
									intent.setClass( RegisterFirst. this,  RegisterFirst.class);
									Bundle bundle=new Bundle();
									bundle.putCharSequence("phone",phone);
									intent.putExtras(bundle);
									RegisterFirst.this.startActivity(intent);
								}else if(result.getResultCode()==States.AUTHCODE_CHECK_EXPIRED){	//主线程提示验证码过期
									Message msg = new Message();
									msg. what=States.AUTHCODE_CHECK_EXPIRED ;
									handler.sendMessage(msg); 
								}else{		//输入验证码错误提醒
									Message msg = new Message();
									msg. what=States.AUTHCODE_CHECK_CODE_ERROR ;
									handler.sendMessage(msg);
								}
							}

							@Override
							public void onError(String info) {
								// TODO Auto-generated method stub

							}
						});	//验证短信验证码是否正确
					}else{//验证码未输入或者不完整
						Toast. makeText(RegisterFirst.this, "请输入6位验证码" , Toast.LENGTH_SHORT).show();
					}
				}else {//手机号未输入或者不完整
					Toast. makeText(RegisterFirst.this, "请输入11位手机号" , Toast.LENGTH_SHORT).show();
				}
			}
		});	//end of next_to_register button
		//构造倒计时
		time=new TimeCount(60000, 1000);

		//点击获取验证码按钮
		btn_get_identifying_code=(Button)findViewById(R.id.btn_get_identifying_code);
		btn_get_identifying_code.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				final UserAPI userAPI = UserAPI.getInstance();
				if(phonenumber.length()==11){
					String phoneNumber=phonenumber.getText().toString();
					if(ClassPathResource.isMobileNO(phoneNumber)==true){	//用正则表达式判断是否符合手机号的规范
					//判断用户手机号是否已经注册
					userAPI.checkRegister(phoneNumber, new OnResponseListener() {
						@Override
						public void onResponse(String data) {
							// TODO Auto-generated method stub
							Log.i("tag","请求成功");
							ResponseDO result=JSON.parseObject(data, ResponseDO.class);
							Log.i("tag","验证是否注册"+String.valueOf(result.getResultCode()));
							//如果手机号已经注册过了
							if(result.getResultCode()==States.REGISTER_PHONE_EXISTS){
								//已经注册过要弹出提示窗口	
								Message msg = new Message();
								msg. what=States.REGISTER_PHONE_EXISTS ;
								handler.sendMessage(msg); 
							}else{	//手机号未被注册过
								time.start();
								String phoneNumber=phonenumber.getText().toString();

								userAPI.getAuthCode(phoneNumber, new OnResponseListener() {
									@Override
									public void onResponse(String data) {
										// TODO Auto-generated method stub
										ResponseDO result=JSON.parseObject(data, ResponseDO.class);
										Log.i("tag",String.valueOf(result.getResultCode()));
										if(result.getResultCode()==States.AUTHCODE_GET_SUCCESSFUL){
											Log.i("tag", "发送成功");
										}
										if(result.getResultCode()==States.AUTHCODE_GET_ERROR){
											Log.i("tag", "发送失败");
										}
									}

									@Override
									public void onError(String info) {
										// TODO Auto-generated method stub
										Log.i("tag", "请求发送失败");
									}
								});	//验证码回调
							}
						}

						@Override
						public void onError(String info) {
							// TODO Auto-generated method stub
							Log.i("tag","请求失败");
						}
					});//
					}else{	//不符合正则表达式
						Message msg = new Message();
						msg.what=123456;
						handler.sendMessage(msg);
					}



				}	//end of if  
				else if(phonenumber.length()==0){
					Toast. makeText(RegisterFirst.this, "手机号长度不能为空" , Toast.LENGTH_SHORT).show();
				}else{
					Toast. makeText(RegisterFirst.this, "请输入11位手机号" , Toast.LENGTH_SHORT).show();
				}

			}
		});	//获取验证码按钮结束


	}//end of onCreate


	//隐藏软键盘
	@Override
	public boolean onTouchEvent(android.view.MotionEvent event) {
		InputMethodManager imm = (InputMethodManager)     getSystemService(INPUT_METHOD_SERVICE);
		return                       
				imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
	}


	/* 定义一个倒计时的内部类 */
	class TimeCount extends CountDownTimer {
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
		}
		@Override
		public void onFinish() {//计时完毕时触发
			btn_get_identifying_code.setText("重新验证");
			btn_get_identifying_code.setClickable(true);
		}
		@Override
		public void onTick(long millisUntilFinished){//计时过程显示
			btn_get_identifying_code.setClickable(false);
			btn_get_identifying_code.setText(millisUntilFinished /1000+"秒");
		}
	}

}
