package com.hichujian.client.ui.login;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.AccessTokenKeeper;
import com.hichujian.client.api.UserAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.ui.Tab;
import com.hichujian.client.utils.States;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuth;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;

public class LoginFirst extends Activity {
private Button login;
private Button look_look;
private Button register;
private TextView forget_password;
/** 注意：SsoHandler 仅当 SDK 支持 SSO 时有效 */
private SsoHandler mSsoHandler;
/** 微博 Web 授权类，提供登陆等功能  */
private WeiboAuth mWeiboAuth;
/** 封装了 "access_token"，"expires_in"，"refresh_token"，并提供了他们的管理功能  */
private Oauth2AccessToken mAccessToken;
/** 显示认证后的信息，如 AccessToken */
private TextView mTokenText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.login);
        InputMethodManager  manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
      
        
        //手机号码登录
        LinearLayout login_phone=(LinearLayout)findViewById(R.id.login_phone);
        login_phone.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setClass( LoginFirst.this,  LoginPhone.class);
				LoginFirst.this.startActivity(intent);
			}
		});
        
     // 创建微博实例
        mWeiboAuth = new WeiboAuth(this, com.hichujian.client.Constants.APP_KEY, com.hichujian.client.Constants.REDIRECT_URL, com.hichujian.client.Constants.SCOPE);
      //新浪微博
        LinearLayout login_sina=(LinearLayout)findViewById(R.id.login_sina);
        login_sina.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				mSsoHandler = new SsoHandler(LoginFirst.this, mWeiboAuth);
                mSsoHandler.authorize(new AuthListener());
			}
		});
        
        //QQ登录
        LinearLayout login_QQ=(LinearLayout)findViewById(R.id.login_QQ);
        login_QQ.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				/*final UserAPI userAPI=UserAPI.getInstance();
				userAPI.login("", "", new OnResponseListener() {
					@Override
					public void onResponse(String data) {
						ResponseDO result=JSON.parseObject(data,ResponseDO.class);
						Log.i("tag", result.getData()+"用户ID");
						if(result.getResultCode()==States.LOGIN_SUCCESSFUL){	//账号密码正确
							//获取用户信息
							userAPI.getUserInfo(Long.valueOf(result.getData().toString()), new OnResponseListener() {

								@Override
								public void onResponse(String data) {
									// TODO Auto-generated method stub
									ResponseDO result = JSON.parseObject(data,ResponseDO.class);
									if(result.getResultCode()==States.LOGIN_SUCCESSFUL){	//获取用户信息成功
									}
								}

								@Override
								public void onError(String info) {
									// TODO Auto-generated method stub

								}
							});
						}	//end of if
					}

					@Override
					public void onError(String info) {
						// TODO Auto-generated method stub
						
					}
		});*/
	}});		
    
        //注册
        LinearLayout login_register=(LinearLayout)findViewById(R.id.login_register);
        login_register.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setClass( LoginFirst.this,  RegisterFirst.class);
				LoginFirst.this.startActivity(intent);
			}
		});
        
        
        //QQ登录
        ImageView pic=(ImageView)findViewById(R.id.pic);
        pic.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setClass( LoginFirst.this,  Tab.class);
				LoginFirst.this.startActivity(intent);
			}
		});
   
        // 从 SharedPreferences 中读取上次已保存好 AccessToken 等信息，
        // 第一次启动本应用，AccessToken 不可用
        mAccessToken = AccessTokenKeeper.readAccessToken(this);
        if (mAccessToken.isSessionValid()) {
            updateTokenView(true);
        }
        
    }//end of onCreate
    /**
     * 当 SSO 授权 Activity 退出时，该函数被调用。
     * 
     * @see {@link Activity#onActivityResult}
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        // SSO 授权回调
        // 重要：发起 SSO 登陆的 Activity 必须重写 onActivityResult
        if (mSsoHandler != null) {
            mSsoHandler.authorizeCallBack(requestCode, resultCode, data);
        }
    }
    
    /**
     * 微博认证授权回调类。
     * 1. SSO 授权时，需要在 {@link #onActivityResult} 中调用 {@link SsoHandler#authorizeCallBack} 后，
     *    该回调才会被执行。
     * 2. 非 SSO 授权时，当授权结束后，该回调就会被执行。
     * 当授权成功后，请保存该 access_token、expires_in、uid 等信息到 SharedPreferences 中。
     */
    class AuthListener implements WeiboAuthListener {
        
        @Override
        public void onComplete(Bundle values) {
            // 从 Bundle 中解析 Token
            mAccessToken = Oauth2AccessToken.parseAccessToken(values);
            if (mAccessToken.isSessionValid()) {
                // 显示 Token
                updateTokenView(false);
                
                // 保存 Token 到 SharedPreferences
                AccessTokenKeeper.writeAccessToken(LoginFirst.this, mAccessToken);
                Toast.makeText(LoginFirst.this, 
                        R.string.weibosdk_demo_toast_auth_success, Toast.LENGTH_SHORT).show();
            } else {
                // 以下几种情况，您会收到 Code：
                // 1. 当您未在平台上注册的应用程序的包名与签名时；
                // 2. 当您注册的应用程序包名与签名不正确时；
                // 3. 当您在平台上注册的包名和签名与您当前测试的应用的包名和签名不匹配时。
                String code = values.getString("code");
                String message = getString(R.string.weibosdk_demo_toast_auth_failed);
                if (!TextUtils.isEmpty(code)) {
                    message = message + "\nObtained the code: " + code;
                }
                Toast.makeText(LoginFirst.this, message, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onCancel() {
            Toast.makeText(LoginFirst.this, 
                    R.string.weibosdk_demo_toast_auth_canceled, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onWeiboException(WeiboException e) {
            Toast.makeText(LoginFirst.this, 
                    "Auth exception : " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 显示当前 Token 信息。
     * 
     * @param hasExisted 配置文件中是否已存在 token 信息并且合法
     */
    private void updateTokenView(boolean hasExisted) {
        String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(
                new java.util.Date(mAccessToken.getExpiresTime()));
        String format = getString(R.string.weibosdk_demo_token_to_string_format_1);
        mTokenText.setText(String.format(format, mAccessToken.getToken(), date));
        
        String message = String.format(format, mAccessToken.getToken(), date);
        if (hasExisted) {
            message = getString(R.string.weibosdk_demo_token_has_existed) + "\n" + message;
        }
        mTokenText.setText(message);
    }
    
}
