package com.hichujian.client.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.UserAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.ui.Jiugongge;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.ClassPathResource;
import com.hichujian.client.utils.DatabaseKeeper;
import com.hichujian.client.utils.States;

public class LoginPhone extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		setContentView(R.layout.login_second);
		Log.i("tag", "DatabaseKeeper start");
		DatabaseKeeper.init(this);
		Log.i("tag", "DatabaseKeeper end ");
		//主函数接受对象
		final Handler handler = new Handler() { 
			@Override 
			public void handleMessage(Message msg) { 
				if(msg.what==101){
					Toast.makeText(LoginPhone.this, "账号未注册", Toast.LENGTH_SHORT).show();
					Log.i ("tag" , "账号未注册");
				}
				if(msg.what==102){
					Toast.makeText(LoginPhone.this, "密码错误", Toast.LENGTH_SHORT).show();
					Log.i ("tag" , "密码错误");
				}
				if(msg.what==103){
					Toast.makeText(LoginPhone.this, "登入账户被账户冻结", Toast.LENGTH_SHORT).show();
					Log.i ("tag" , "登入账户被账户冻结");
				}
				if(msg.what==104){
					Toast.makeText(LoginPhone.this, "服务器繁忙", Toast.LENGTH_SHORT).show();
					Log.i ("tag" , "服务器繁忙");
				}
			} 
		}; 


		//返回按钮
		ImageButton back_to_login1=(ImageButton)findViewById(R.id.back_to_login1);
		back_to_login1.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				LoginPhone.this.finish();
			}
		});
		//马上注册
		TextView textView_register=(TextView)findViewById(R.id.textView_register);
		textView_register.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setClass( LoginPhone.this,  RegisterFirst.class);
				LoginPhone.this.startActivity(intent);
			}
		});
		//忘记密码
		TextView forget_password=(TextView)findViewById(R.id.forget_password);
		forget_password.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setClass( LoginPhone.this,  ForgetPassword.class);
				LoginPhone.this.startActivity(intent);
			}
		});
		//捕获账号和密码
		final EditText login_count=(EditText)findViewById(R.id.login_count);
		final EditText login_password=(EditText)findViewById(R.id.login_password);

		//登录按钮
		Button login=(Button) findViewById(R.id.login);  
		login.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {	
				//账号
				final String count=login_count.getText().toString();
				//密码
				String password=login_password.getText().toString();
				if(count.length()!=0){	//账号不为空
					if(count.length()!=11){	//号码不等于11位
						Toast.makeText(LoginPhone.this, "请输入正确的11位手机号", Toast.LENGTH_SHORT).show();
					}else{	//号码等于11位
						if(ClassPathResource.isMobileNO(count)==true){	//用正则表达式判断是否符合手机号的规范
						if(password.length()!=0){	//密码不为空
							//发送验证信息的请求
							final UserAPI userAPI=UserAPI.getInstance();
							userAPI.login(count, password, new OnResponseListener() {
								@Override
								public void onResponse(String data) {
									((InputMethodManager)getSystemService( INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(LoginPhone.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS );  
									ResponseDO result=JSON.parseObject(data,ResponseDO.class);
									Log.i("tag", result.getData()+"用户ID");
									if(result.getResultCode()==States.LOGIN_SUCCESSFUL){	//账号密码正确
										//跳转到九宫格
										Intent intent=new Intent();
										intent.setClass( LoginPhone. this,  Jiugongge.class);
										//把用户ID数据存入SharePreferences
										SharedPreferences preferences = getSharedPreferences("User", Context.MODE_PRIVATE);
										Editor editor = preferences.edit();
										editor.putString("UserId", result.getData().toString().split(":", 2)[0]);
										editor.commit();
										Log.i("tag", "存入SharePreferences后值为"+ result.getData().toString());
										//把用户手机号数据存入SharePreferences
			                            SharedPreferences preferences3 = getSharedPreferences("phonenumber" , Context.MODE_PRIVATE);
			                            Editor editor3 = preferences3.edit();
			                            editor.putString( "PhoneNumber", count);
			                            editor.commit();
			                            Log. i("tag", "存入SharePreferences后值为" + count);
			                            LoginPhone.this.startActivity(intent);
										/*//获取用户信息
										userAPI.getUserInfo(Long.valueOf(result.getData().toString()), new OnResponseListener() {

											@Override
											public void onResponse(String data) {
												// TODO Auto-generated method stub
												ResponseDO result = JSON.parseObject(data,ResponseDO.class);
												if(result.getResultCode()==States.LOGIN_SUCCESSFUL){	//获取用户信息成功
												}
											}

											@Override
											public void onError(String info) {
												// TODO Auto-generated method stub

											}
										});*/
									}	//end of if
									else if(result.getResultCode()==101){	//账号未注册
										Log.i("tag", String.valueOf(result.getResultCode()));
										Log.i("tag", LoginPhone.this.getClass().getName());

										Message msg = new Message(); 
										msg.what =101;//result.getData().toString(); 
										handler.sendMessage(msg); 
									}else if(result.getResultCode()==102){	//密码错误
										Log.i("tag", String.valueOf(result.getResultCode()));
										Log.i("tag", LoginPhone.this.getClass().getName());

										Message msg = new Message(); 
										msg.what =102;//result.getData().toString(); 
										handler.sendMessage(msg); 
									}else if(result.getResultCode()==103){	//登入账户被账户冻结
										Log.i("tag", String.valueOf(result.getResultCode()));
										Log.i("tag", LoginPhone.this.getClass().getName());

										Message msg = new Message(); 
										msg.what =103;//result.getData().toString(); 
										handler.sendMessage(msg); 
									}else if(result.getResultCode()==104){	//服务器繁忙
										Log.i("tag", String.valueOf(result.getResultCode()));
										Log.i("tag", LoginPhone.this.getClass().getName());

										Message msg = new Message(); 
										msg.what =104;//result.getData().toString(); 
										handler.sendMessage(msg); 
									}
								}

								@Override
								public void onError(String info) {
									// TODO Auto-generated method stub
								}
							});	//end of userAPI
						
						}else{	//密码为空
							Toast.makeText(LoginPhone.this, "请输入密码", Toast.LENGTH_SHORT).show();
						}
						
						
					
					}else{//不符合正则表达式
						Toast.makeText(LoginPhone.this, "不是手机号哦~", Toast.LENGTH_SHORT).show();
					}
					}//手机号等于11位
				} else {	//账号为空
					Toast.makeText(LoginPhone.this, "请输入正确的11位手机号", Toast.LENGTH_SHORT).show();
				}

			}
		});//end of login Button
	}
}
