package com.hichujian.client.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.hichujian.R;

public class RegisterSecond extends Activity{
	private Button next_to_register;
	private EditText register_password,confirm_password;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.register_second);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        final String phone = bundle.getString("phone");
        //输入密码输入框
        final EditText register_password=(EditText)findViewById(R.id.register_password);
        //确认密码输入框
        final EditText confirm_password=(EditText)findViewById(R.id.confirm_password); 
        //返回
        ImageButton back_to_login10=(ImageButton) findViewById(R.id.back_to_login10);  
        back_to_login10.setOnClickListener( new OnClickListener() {
        	public void onClick(View v) {
        		RegisterSecond.this.finish();
        	}
        });
        //下一步按钮
        next_to_register=(Button) findViewById(R.id.next_to_register);  
        next_to_register.setOnClickListener( new OnClickListener() {
        	public void onClick(View v) {
        		String register_password_string = register_password.getText().toString();
        		String confirm_password_string = confirm_password.getText().toString();
        		if(register_password.length()==0){	//密码为空
        			Toast.makeText(RegisterSecond.this, "请输入密码", Toast.LENGTH_SHORT).show();
        		}else {
        			if(register_password.length()<6){	//密码小于6位
        				Toast.makeText(RegisterSecond.this, "输入的密码要在6-16位", Toast.LENGTH_SHORT).show();
        			}else{	//密码大于6位
        				if(confirm_password.length()!=0){	//确认密码不为空
        					if(register_password_string.equals(confirm_password_string)){	//两次密码相同
        						Intent intent=new Intent();
        						intent.setClass( RegisterSecond. this,  RegisterThree.class);
        						Bundle bundle=new Bundle();
        						bundle.putCharSequence("phone",phone);
        						bundle.putCharSequence("password", register_password_string);
        						Log.i("tag", phone);
        						Log.i("tag", register_password_string);
        						intent.putExtras(bundle);
        						RegisterSecond.this.startActivity(intent);
        					}else{	//输入的两次密码不一样
        						Toast.makeText(RegisterSecond.this, "输入的两次密码不一样", Toast.LENGTH_SHORT).show();
        					}
        				}else{
        					Toast.makeText(RegisterSecond.this, "请输入确认密码", Toast.LENGTH_SHORT).show();
        				}
        			}
        		}//end of if
        		
        	
        		
        	}
        });	//下一步按钮结束
	}
}