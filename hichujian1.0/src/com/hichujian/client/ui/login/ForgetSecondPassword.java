package com.hichujian.client.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.UserAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.ui.Jiugongge;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class ForgetSecondPassword extends Activity{
	private ImageButton back_to_login2;
	private TextView forget_password_finish;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.forget_second_password);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        final String phone = bundle.getString("phone");
        
        ImageButton back_to_login9=(ImageButton) findViewById(R.id.back_to_login9);  
        back_to_login9.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	finish();
           }
        });
        //新密码
        final EditText reset_password = (EditText)findViewById(R.id.reset_password);
        //确认新密码
        final EditText reset_confirm_password = (EditText)findViewById(R.id.reset_confirm_password);
        //完成按钮
        Button forget_password_finish=(Button) findViewById(R.id.forget_password_finish);  
        forget_password_finish.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	
            	final String register_password_string = reset_password.getText().toString();
        		String confirm_password_string = reset_confirm_password.getText().toString();
        		if(reset_password.length()==0){	//密码为空
        			Toast.makeText(ForgetSecondPassword.this, "请输入密码", Toast.LENGTH_SHORT).show();
        		}else {
        			if(reset_password.length()<6){	//密码小于6位
        				Toast.makeText(ForgetSecondPassword.this, "输入的密码要在6-16位", Toast.LENGTH_SHORT).show();
        			}else{	//密码大于6位
        				if(reset_confirm_password.length()!=0){	//确认密码不为空
        					if(register_password_string.equals(confirm_password_string)){	//两次密码相同
        						
        						UserAPI userAPI = UserAPI.getInstance();
        						userAPI.resetPassword(phone, register_password_string, new OnResponseListener() {
									
									@Override
									public void onResponse(String data) {
										// TODO Auto-generated method stub
										ResponseDO result = JSON.parseObject(data, ResponseDO.class);
										if(result.getResultCode()==States.PWD_RESET_SUCCESSFUL){	//密码重置成功
											Intent intent=new Intent();
			        						intent.setClass( ForgetSecondPassword. this,  Jiugongge.class);
			        						Log.i("tag", phone);
			        						Log.i("tag", register_password_string);
			        						
			        						//把用户ID数据存入SharePreferences
                                            SharedPreferences preferences = getSharedPreferences("User" , Context.MODE_PRIVATE);
                                            Editor editor = preferences.edit();
                                             //Long UserId = (Long) result.getData();
                                            editor.putString( "UserId", result.getData().toString());
                                            editor.commit();
                                            Log. i("tag", "存入SharePreferences后值为" + result.getData().toString());

			        						ForgetSecondPassword.this.startActivity(intent);
										}else{	//密码重置失败
											Log.i("tag", "重置失败");	
										}
									}
									
									@Override
									public void onError(String info) {
										// TODO Auto-generated method stub
										
									}
								});//确认密码回调
        					}else{	//输入的两次密码不一样
        						Toast.makeText(ForgetSecondPassword.this, "输入的两次密码不一样", Toast.LENGTH_SHORT).show();
        					}
        				}else{
        					Toast.makeText(ForgetSecondPassword.this, "请输入确认密码", Toast.LENGTH_SHORT).show();
        				}
        			}
        		}//end of if
        		
           }
        });
	}
}
