package com.hichujian.client.ui.login;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.Constants;
import com.hichujian.client.api.FileAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class RegisterThree extends Activity{
	public static String picture_youbaiyun_uri;
	private ImageButton uploading_head;
	private static final int PHOTO_REQUEST_TAKEPHOTO = 1;// 拍照
	private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
	private static final int PHOTO_REQUEST_CUT = 3;// 结果
	private String[] items;//各列表项要显示的内容
	private static String upyunPath = "http://chujian-files.b0.upaiyun.com";
	private static String path;	//头像存储路径
	private File file; 
	private Long  UserID;
	// 创建一个以当前时间为名称的文件
	File tempFile = new File(Environment.getExternalStorageDirectory(),getPhotoFileName());
	Uri uri=Uri.fromFile(tempFile);
	private String phone;
	//Preference 
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.register_third);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        phone = bundle.getString("phone");
        final String password = bundle.getString("password");
        
      //获取UserId
        SharedPreferences preferences = getSharedPreferences("User" , Context.MODE_PRIVATE);
        String UserId = preferences.getString( "UserId", "");
        UserID = Long. valueOf (UserId);
        Log. i("tag",Long. valueOf(UserId)+"Long型");
        Log. i("tag", UserId+ "");

      //取消按钮
        ImageButton back_to_login5=(ImageButton) findViewById(R.id.back_to_login5);  
        back_to_login5.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	finish();
           }
        });
        
        
      //下一步按钮
        Button second_register_next=(Button) findViewById(R.id.second_register_next);  
        second_register_next.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	Log.i("tag", String.valueOf(uri));
            	//跳转到注册第四步
            	Intent intent=new Intent();
                intent.setClass( RegisterThree. this,  RegisterFour.class);
                Bundle bundle=new Bundle();
				bundle.putCharSequence("phone",phone);
				bundle.putCharSequence("password", password);
				Log.i("tag", phone);
				Log.i("tag", password);
				intent.putExtras(bundle);
                RegisterThree.this.startActivity(intent);
                
            	FileAPI fileAPI = FileAPI.getInstance();
            	fileAPI.upload(UserID,Constants.ROOT_ADDRESS+"/"+phone+"/"+"hichujianHead.jpg" , new OnResponseListener() {	//"/storage/sdcard1/11.jpg"
					
					@Override
					public void onResponse(String data) {
						// TODO Auto-generated method stub
						Log.i("tag",data + "data");
							Log.i("tag", "上传成功");
							Log.i("tag", data);
							RegisterThree.picture_youbaiyun_uri= upyunPath + data;
							Log.i("tag", picture_youbaiyun_uri);
					}
					@Override
					public void onError(String info) {
						// TODO Auto-generated method stub
						Log.i("tag","上传失败");
					}
				});//文件回调
           }
        });
        
      //上传头像按钮
        uploading_head=(ImageButton)findViewById(R.id.uploading_head);
        uploading_head.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showDialog();
			}
		});
        
}
	
	
	//提示对话框方法
			private void showDialog() {
				new AlertDialog.Builder(this)
				.setTitle("头像设置")
				.setPositiveButton("拍照", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						// 调用系统的拍照功能
						Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						// 指定调用相机拍照后照片的储存路径
						intent.putExtra(MediaStore.EXTRA_OUTPUT,
								Uri.fromFile(tempFile));
						startActivityForResult(intent, PHOTO_REQUEST_TAKEPHOTO);
					}
				})
				.setNegativeButton("相册", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Intent intent = new Intent(Intent.ACTION_PICK, null);
						intent.setDataAndType(
								MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
								"image/*");
						startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
					}
				}).show();
			}
			//回调函数
			@Override
			protected void onActivityResult(int requestCode, int resultCode, Intent data) {
				// TODO Auto-generated method stub

				switch (requestCode) {
				case PHOTO_REQUEST_TAKEPHOTO://当选择拍照时调用
					startPhotoZoom(Uri.fromFile(tempFile), 200);
					break;

				case PHOTO_REQUEST_GALLERY://当选择从本地获取图片时
					//做非空判断，当我们觉得不满意想重新剪裁的时候便不会报异常，下同
					if (data != null)
						startPhotoZoom(data.getData(), 200);
					break;

				case PHOTO_REQUEST_CUT://返回的结果
					if (data != null) 
						setPicToView(data);
					break;
				}
				super.onActivityResult(requestCode, resultCode, data);

			}
			//照片裁剪
			private void startPhotoZoom(Uri uri, int size) {
				Intent intent = new Intent("com.android.camera.action.CROP");
				intent.setDataAndType(uri, "image/*");
				// crop为true是设置在开启的intent中设置显示的view可以剪裁
				intent.putExtra("crop", "true");

				// aspectX aspectY 是宽高的比例
				intent.putExtra("aspectX", 2);
				intent.putExtra("aspectY", 2);

				// outputX,outputY 是剪裁图片的宽高
//				intent.putExtra("outputX", size);
//				intent.putExtra("outputY", size);
				intent.putExtra("return-data", true);

				startActivityForResult(intent, PHOTO_REQUEST_CUT);
			}

			//将进行剪裁后的图片显示到UI界面上
			private void setPicToView(Intent picdata) {
				Bundle bundle = picdata.getExtras();
				if (bundle != null) {
					Bitmap photo = bundle.getParcelable("data");
					//photo.
					Drawable drawable = new BitmapDrawable(photo);
					uploading_head.setBackgroundDrawable(drawable);
					//  /storage/sdcard1/chujian/hichujianHead.jpg
					savePhotoToSDCard(Constants.ROOT_ADDRESS+"/"+phone+"/","hichujianHead.jpg",photo);
					
				}
			}

			// 使用系统当前日期加以调整作为照片的名称
			private String getPhotoFileName() {
				Date date = new Date(System.currentTimeMillis());
				SimpleDateFormat dateFormat = new SimpleDateFormat("'IMG'_yyyyMMdd_HHmmss");
				return dateFormat.format(date) + ".jpg";
			}
			//保存照片到SD卡上面
			public static void savePhotoToSDCard(String path, String photoName,
		            Bitmap photoBitmap) {
		        if (android.os.Environment.getExternalStorageState().equals(
		                android.os.Environment.MEDIA_MOUNTED)) {
		            File dir = new File(path);
		            if (!dir.exists()) {
		                dir.mkdirs();
		            }
		            File photoFile = new File(path, photoName); //在指定路径下创建文件
		            FileOutputStream fileOutputStream = null;
		            try {
		                fileOutputStream = new FileOutputStream(photoFile);
		                if (photoBitmap != null) {
		                    if (photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100,
		                            fileOutputStream)) {
		                        fileOutputStream.flush();
		                    }
		                }
		            } catch (FileNotFoundException e) {
		                photoFile.delete();
		                e.printStackTrace();
		            } catch (IOException e) {
		                photoFile.delete();
		                e.printStackTrace();
		            } finally {
		                try {
		                    fileOutputStream.close();
		                } catch (IOException e) {
		                    e.printStackTrace();
		                }
		            }
		        }
		    }
}