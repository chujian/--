package com.hichujian.client.ui.setting;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.UserAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.model.UserDO;
import com.hichujian.client.utils.AsyncRunner;
import com.hichujian.client.utils.ImageLoadUtils;
import com.hichujian.client.utils.ImageUtil;
import com.hichujian.client.utils.States;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;

public class MySetting extends Activity{
	private ImageButton back_to_login63;
	private ImageView my_setting_edit;
	private Long UserID;
	private int CODE_MY_SETTING = 0x168;//我的设置的请求返回
	private static final int MAX_THREAD_COUNT = 5;//最大线程数量
	private static Handler handlerBitmap;
	private String[] url;//图片的所有URL
	private static ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREAD_COUNT);
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.my_setting);
        
        SharedPreferences preferences = getSharedPreferences("User" , Context. MODE_PRIVATE);
        String UserId = preferences.getString( "UserId" , "" );
        UserID = Long. valueOf(UserId);
        Log. i( "tag",Long.valueOf(UserId)+ "Long型");
        Log. i( "tag", UserId+"hichujian_person_details 用户ID");
        
      //获取用户的一些信息
        final TextView nickname = (TextView)findViewById(R.id.person_nickname1);
        final TextView birthday = (TextView)findViewById(R.id.birthday1);
        final TextView location = (TextView)findViewById(R.id.location1);
        final TextView person_sex = (TextView)findViewById(R.id.person_sex1);
        final TextView emotionState = (TextView)findViewById(R.id.emotionState1);
        final TextView interestHobbies = (TextView)findViewById(R.id.interestHobbies1);
        final TextView usuallyAppearArea = (TextView)findViewById(R.id.usuallyAppearArea1);
        final TextView job = (TextView)findViewById(R.id.job1);
        final TextView age1 = (TextView)findViewById(R.id.age1);
        final TextView person_sex1 = (TextView)findViewById(R.id.person_sex1);
        final TextView constellation = (TextView)findViewById(R.id.constellation);
        
        
        
        final ImageView MySettinghead1 = (ImageView)findViewById(R.id.MySettinghead1);
        final ImageView MySettinghead2 = (ImageView)findViewById(R.id.MySettinghead2);
        final ImageView MySettinghead3 = (ImageView)findViewById(R.id.MySettinghead3);
        final ImageView MySettinghead4 = (ImageView)findViewById(R.id.MySettinghead4);
        final ImageView MySettinghead5 = (ImageView)findViewById(R.id.MySettinghead5);
        final ImageView MySettinghead6 = (ImageView)findViewById(R.id.MySettinghead6);
        final ImageView MySettinghead7 = (ImageView)findViewById(R.id.MySettinghead7);
        final ImageView MySettinghead8 = (ImageView)findViewById(R.id.MySettinghead8);
        
        final ImageView mySettinghead[] = {MySettinghead1,MySettinghead2,MySettinghead3,MySettinghead4,MySettinghead5,MySettinghead6,MySettinghead7,MySettinghead8};
        
        back_to_login63=(ImageButton)findViewById(R.id.back_to_login63);
        back_to_login63.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	finish();
           }
        });
        
        my_setting_edit=(ImageView)findViewById(R.id.my_setting_edit);
        my_setting_edit.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	Intent intent =new Intent();
            	
            	String nicknameString = nickname.getText().toString();
            	String birthdayString = birthday.getText().toString();
            	String locationString = location.getText().toString();
            	String emotionStateString = emotionState.getText().toString();
            	String interestHobbiesString = interestHobbies.getText().toString();
            	String jobString = job.getText().toString();
            	String usuallyAppearAreaString = usuallyAppearArea.getText().toString();
            	
            	Bundle bundle = new Bundle();
            	bundle.putString("nicknameString", nicknameString);
            	bundle.putString("birthdayString", birthdayString);
            	bundle.putString("locationString", locationString);
            	bundle.putString("emotionStateString", emotionStateString);
            	bundle.putString("interestHobbiesString", interestHobbiesString);
            	bundle.putString("jobString", jobString);
            	bundle.putString("usuallyAppearAreaString", usuallyAppearAreaString);
            	intent.putExtras(bundle);
            	intent.setClass(MySetting.this, MySettingEdit.class);
            	MySetting.this.startActivityForResult(intent, CODE_MY_SETTING);
            	Log.i("tag", "进入my_setting_edit");
            }
        });
        
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
               UserDO user = (UserDO)msg.obj;
               String Nickname = user.getNickname();//昵称
               Date Birthday = user.getBirthday();//生日
               Calendar c = Calendar.getInstance();
               int ageInt = c.get(Calendar.YEAR)-user.getBirthday().getYear();//2014-1992获得年龄
               String EmotionState = user.getEmotionState();	//情感状态
               String InterestHobbies = user.getInterestHobbies();//兴趣
               //String Units = user.getUnits();//单位
               String Sex = user.getSex();//性别
               String UsuallyAppearArea = user.getUsuallyAppearArea();
               String Location = user.getLocation();
               String HeadPortrait = user.getHeadPortrait();//头像URL
               String Job = user.getJob();
               String Imageurl = user.getPhoto();//获得图片的Url
               Log.i("tag",Imageurl+"Imageurl");
               
               url = Imageurl.split("\\s*;\\s*");
               
               Log.i("tag",String.valueOf(url.length));
               
				//把数据扔进去
                Drawable drawable;	//设置性别
				if(Sex.equals("男")){
					drawable= getResources().getDrawable(R.drawable.activity_man);  
				}else{
					drawable= getResources().getDrawable(R.drawable.activity_woman);  
				}
				/// 这一步必须要做,否则不会显示.  
				drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());  
				person_sex.setCompoundDrawables(drawable,null,null,null); 
                nickname.setText(Nickname);	//设置昵称
                person_sex.setText(ageInt+"");//设置年龄
                birthday.setText(Birthday+"");//设置生日
                								//语音
                location.setText(Location+"");		//地区
                emotionState.setText(EmotionState);//设置情感状态
                interestHobbies.setText(InterestHobbies);//设置兴趣爱好
                usuallyAppearArea.setText(UsuallyAppearArea);//常出没的地方    
                job.setText(Job);//设置job
                
               for(int i =0;i<url.length;i++){
            	   Log.i("tag", "图片"+i+"完成");
            	   //loadImageAndShow(url[i],i);
            	   ImageLoadUtils.loadImage(url[i], mySettinghead[i], null);
            	   Log.i("tag",url[i]+"mySettinghead"+i);
               }
            }
        };  
        

        UserAPI userAPI = UserAPI.getInstance();
        userAPI.getUserInfo(UserID, new OnResponseListener() {
			
			@Override
			public void onResponse(String data) {
				// TODO Auto-generated method stub
				Log.i("tag","请求成功");
				ResponseDO result = JSON.parseObject(data,ResponseDO.class);
				if(result.getResultCode()==States.GET_USER_INFO_SUCCESSFUL){	//获取用户信息成功
					UserDO user= JSON.parseObject(result.getData().toString(), UserDO.class);
					Log.i("tag", result.toString()+"result.getData().toString()");
					Log.i("tag", user.getNickname());
					
					//发送消息
					Message msg = new Message();
					msg.obj=user ;
					handler.sendMessage(msg); 
				}else if(result.getResultCode()==States.GET_USER_INFO_SUCCESSFUL){	//获取用户信息不存在
					Log.i("tag", "获取的用户不存在");
				}else if(result.getResultCode()==States.GET_USER_INFO_OTHER_ERROR){//获取用户信息其他错误
					Log.i("tag", "获取用户信息其他错误");
				}
			}
			
			@Override
			public void onError(String info) {
				// TODO Auto-generated method stub
				Log.i("tag","请求失败");
			}
		});	//获取个人详情的回调结束
        
        //点击第一张图片看大图
        MySettinghead1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent= new Intent();
				intent.setClass(MySetting.this, ImageViewShow.class);
				MySetting.this.startActivity(intent);
				
			}
		});
}//end of onCreate
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	Log.i("tag", "回调函数");
          if(requestCode==CODE_MY_SETTING && resultCode==CODE_MY_SETTING ){
        	  Bundle bundle = data.getExtras();
        	  
              String mySetNickNameString = bundle.getString("mySetNickName");
              String setbirdayString = bundle.getString("setbirdayString");
              String location = bundle.getString("location");
              String emotionState = bundle.getString("emotionState");
              String setInterest = bundle.getString("setInterest");
              String job = bundle.getString("job");
              String setalwayslocation = bundle.getString("setalwayslocation");
              
              TextView person_nickname1 = (TextView)findViewById(R.id.person_nickname1);
              TextView birthday1 = (TextView)findViewById(R.id.birthday1);
              TextView location1 = (TextView)findViewById(R.id.location1);
              TextView emotionState1 = (TextView)findViewById(R.id.emotionState1);
              TextView interestHobbies1 = (TextView)findViewById(R.id.interestHobbies1);
              TextView usuallyAppearArea1 = (TextView)findViewById(R.id.usuallyAppearArea1);
              TextView job1 = (TextView)findViewById(R.id.job1);
              
              person_nickname1.setText(mySetNickNameString+"");
              birthday1.setText(setbirdayString+"");
              location1.setText(location+"");
              emotionState1.setText(emotionState+"");
              interestHobbies1.setText(setInterest+"");
              usuallyAppearArea1.setText(setalwayslocation+"");
              job1.setText(job+"");
              Log.i("tag","回调结束");
         } 
    }

	
	
	
	
}