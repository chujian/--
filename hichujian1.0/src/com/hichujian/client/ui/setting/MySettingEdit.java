package com.hichujian.client.ui.setting;

import java.sql.Date;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.UserAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.model.UserDO;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class MySettingEdit extends Activity{
	private ImageButton back_to_login64;
	private ImageView personCommit;
	private Long  UserID ;
	private String setbirthday;
	private Handler handler;
	private int year1;	//年
	private int month1;//月
	private int day1;//日
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.my_setting_edit);
        
      //获取UserId
        SharedPreferences preferences = getSharedPreferences("User" , Context.MODE_PRIVATE);
        String UserId = preferences.getString( "UserId", "");
        UserID = Long. valueOf (UserId);
        Log. i("tag",Long. valueOf(UserId)+"Long型");
        Log. i("tag", UserId+ "");
        
        //找到布局
        final EditText MySetNickName = (EditText)findViewById(R.id.mySetNickName);//昵称
        final TextView Setbirthday = (TextView)findViewById(R.id.Setbirthday);//生日
        final EditText Setlocation = (EditText)findViewById(R.id.Setlocation);//地区
        final TextView Setemotion = (TextView)findViewById(R.id.Setemotion);//情感状态
        final EditText SetInterest = (EditText)findViewById(R.id.SetInterest);//兴趣爱好
        final EditText Setjob = (EditText)findViewById(R.id.Setjob);//职业
        final EditText Setalwayslocation = (EditText)findViewById(R.id.Setalwayslocation);//常出没的地方
        
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String nicknameString = bundle.getString("nicknameString");
        String birthdayString = bundle.getString("birthdayString");
        String locationString = bundle.getString("locationString");
        String emotionStateString = bundle.getString("emotionStateString");
        String interestHobbiesString = bundle.getString("interestHobbiesString");
        String jobString = bundle.getString("jobString");
        String usuallyAppearAreaString = bundle.getString("usuallyAppearAreaString");
        
        MySetNickName.setText(nicknameString);
        Setbirthday.setText(birthdayString);
        Setlocation.setText(locationString);
        Setemotion.setText(emotionStateString);
        SetInterest.setText(interestHobbiesString);
        Setjob.setText(jobString);
        Setalwayslocation.setText(usuallyAppearAreaString);
        
        back_to_login64=(ImageButton)findViewById(R.id.back_to_login64);
        back_to_login64.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	Log.i("tag", "start");
            	//发送给服务器
            	//获取所有信息
            	String mySetNickName = MySetNickName.getText().toString();
            	Log.i("tag", String.valueOf(year1)+";"+String.valueOf(month1)+";"+String.valueOf(day1));
            	Log.i("tag", "111");
            	
            	//Date birthdaydate = Date.valueOf(year1 + "-" + month1 + "-" + day1);
            	String location = Setlocation.getText().toString();
            	String emotionState = Setemotion.getText().toString();
            	String setInterest = SetInterest.getText().toString();
            	String job = Setjob.getText().toString();
            	String setalwayslocation = Setalwayslocation.getText().toString();
            	String setbirdayString = Setbirthday.getText().toString();
            	Log.i("tag", setbirdayString.trim());
            	Date birthdaydate1 = Date.valueOf(setbirdayString.trim());
            	
            	Log.i("tag", "222");
            	
            	UserDO user = new UserDO();
            	user.setUserID(UserID);
       	  		user.setNickname(mySetNickName);
       	  		user.setBirthday(birthdaydate1);
       	  		user.setLocation(location);
       	  		user.setEmotionState(emotionState);
       	  		user.setInterestHobbies(setInterest);
       	  		user.setJob(job);
       	  		user.setUsuallyAppearArea(setalwayslocation);
       	  		
       	  		Log.i("tag", "333");
       	  		
       	  		
       	  		String userInfo = JSON.toJSONString(user);
       	  		Log.i("tag", userInfo+"userInfo");
            	UserAPI userAPI = UserAPI.getInstance();
            	userAPI.setUserInfo(UserID, userInfo, new OnResponseListener() {
					
					@Override
					public void onResponse(String data) {
						// TODO Auto-generated method stub
						Log.i("tag", "setUserInfo请求成功");
						Log.i("tag", data);
						
						ResponseDO result = JSON.parseObject(data, ResponseDO.class);
			            if(result.getResultCode() == States.SETUP_SUCCESSFUL) {//修改成功
			            	Log.i("tag", "设置成功");
			            	//发送消息
			            	Message msg = new Message();
			            	msg. what=States.SETUP_SUCCESSFUL ;
			            	handler.sendMessage(msg); 
						}
			            if(result.getResultCode() == States.SETUP_INFO_ERROR) {//修改失败
			            	Log.i("tag", "设置信息错误");
			            	//发送消息
			            	Message msg = new Message();
			            	msg. what=States.SETUP_INFO_ERROR ;
			            	handler.sendMessage(msg); 
						}
			            if(result.getResultCode() == States.SETUP_ERROR) {//数据库错误
			            	Log.i("tag", "数据库错误");
			            	//发送消息
			            	Message msg = new Message();
			            	msg. what=States.SETUP_ERROR ;
			            	handler.sendMessage(msg); 
						}
					}
					
					@Override
					public void onError(String info) {
						// TODO Auto-generated method stub
						Log.i("tag", "setUserInfo请求失败");
						
					}
				});//提交回调
            	
            	Log.i("tag", "444");
            	
            	mySetNickName = MySetNickName.getText().toString();
            	setbirthday = Setbirthday.getText().toString();
            	//回调给前面显示页面
            	Intent intent = getIntent();
            	Bundle bundle = new Bundle();
                bundle.putString( "mySetNickName" , mySetNickName);
                bundle.putString( "setbirdayString" , setbirdayString);
                bundle.putString("location", location);
                bundle.putString("emotionState", emotionState);
                bundle.putString("setInterest", setInterest);
                bundle.putString("job", job);
                bundle.putString("setalwayslocation", setalwayslocation);
                Log.i("tag", mySetNickName+"hichujian_my_setting_edit  mySetNickName");
                intent.putExtras(bundle);
                setResult(0x168,intent);
                
                Log.i("tag", "555");
            	finish();
           }
        });
        
        Setbirthday.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	Calendar c = Calendar. getInstance();
                // 直接创建一个DatePickerDialog对话框实例，并将它显示出来
                new DatePickerDialog(MySettingEdit.this,
                       // 绑定监听器
                       new DatePickerDialog.OnDateSetListener()
                      {
                              @Override
                              public void onDateSet(DatePicker dp, int year,
                                     int month, int dayOfMonth)
                             {
                           	     year1=year;
                           	     month1=month + 1;
                           	     day1=dayOfMonth;
                           	  Setbirthday.setText(  year1 + "-" + month1
                                           + "-" + day1  );
                             }
                      }
                //设置初始日期
               , c.get(Calendar. YEAR)
               , c.get(Calendar. MONTH)
               , c.get(Calendar. DAY_OF_MONTH)).show();
           }
        });
        
        
        //选择地区
       /* Setlocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				final String[] items=new String[]{"北京","上海","杭州","阅读","聚餐","K歌","桌游","电影","网友"};
				Builder builder=new AlertDialog.Builder(MySettingEdit.this);
				builder.setIcon(R.drawable.cha);
				builder.setTitle("请选择地区:");
				//添加列表项
				builder.setItems(items,new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int which) {
						// TODO Auto-generated method stub
						Setlocation.setText(items[which]);
					}
				});
				builder.create().show();
			}
		});*/
        
        //选择情感
        Setemotion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				final String[] items=new String[]{"单身","已婚"};
				Builder builder=new AlertDialog.Builder(MySettingEdit.this);
				builder.setIcon(R.drawable.cha);
				builder.setTitle("请选择情感状态:");
				//添加列表项
				builder.setItems(items,new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int which) {
						// TODO Auto-generated method stub
						Setemotion.setText(items[which]);
					}
				});
				builder.create().show();
			}
		});
        
        //封装成JSON传输
        	/*UserDO user = new UserDO();
        	  user.setUserID(UserID);
   	  		  user.setPersonalSignature("我的新个性签名");
   	  		  userInfo = JSON.toJSONString(user);*/
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
            	if(msg.what == States.SETUP_SUCCESSFUL){
                    Toast. makeText(MySettingEdit.this, "设置成功" , Toast.LENGTH_SHORT).show();
              }
            	if(msg.what == States.SETUP_INFO_ERROR){
                    Toast. makeText(MySettingEdit.this, "设置错误" , Toast.LENGTH_SHORT).show();
              }
            	if(msg.what == States.SETUP_ERROR){
                    Toast. makeText(MySettingEdit.this, "数据库错误" , Toast.LENGTH_SHORT).show();
              }

            }
        };
        
        personCommit=(ImageView)findViewById(R.id.personCommit);
        personCommit.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	//获取所有信息
            	
            	String mySetNickName = MySetNickName.getText().toString();
            	Log.i("tag", String.valueOf(year1)+";"+String.valueOf(month1)+";"+String.valueOf(day1));
            	
            	///Date birthdaydate = Date.valueOf(year1 + "-" + month1 + "-" + day1);
            	String location = Setlocation.getText().toString();
            	String emotionState = Setemotion.getText().toString();
            	String setInterest = SetInterest.getText().toString();
            	String job = Setjob.getText().toString();
            	String setalwayslocation = Setalwayslocation.getText().toString();
            	String setbirdayString = Setbirthday.getText().toString();
            	Date birthdaydate1 = Date.valueOf(setbirdayString.trim());
            	
            	//Log.i("tag", String.valueOf(birthdaydate)+"birthdaydate");
            	
            	UserDO user = new UserDO();
            	user.setUserID(UserID);
       	  		user.setNickname(mySetNickName);
       	  		user.setBirthday(birthdaydate1);
       	  		user.setLocation(location);
       	  		user.setEmotionState(emotionState);
       	  		user.setInterestHobbies(setInterest);
       	  		user.setJob(job);
       	  		user.setUsuallyAppearArea(setalwayslocation);
       	  		
       	  		
       	  		
       	  		String userInfo = JSON.toJSONString(user);
       	  		Log.i("tag", userInfo+"userInfo");
            	UserAPI userAPI = UserAPI.getInstance();
            	userAPI.setUserInfo(UserID, userInfo, new OnResponseListener() {
					
					@Override
					public void onResponse(String data) {
						// TODO Auto-generated method stub
						Log.i("tag", "setUserInfo请求成功");
						Log.i("tag", data);
						
						ResponseDO result = JSON.parseObject(data, ResponseDO.class);
			            if(result.getResultCode() == States.SETUP_SUCCESSFUL) {//修改成功
			            	Log.i("tag", "设置成功");
			            	//发送消息
			            	Message msg = new Message();
			            	msg. what=States.SETUP_SUCCESSFUL ;
			            	handler.sendMessage(msg); 
						}
			            if(result.getResultCode() == States.SETUP_INFO_ERROR) {//修改失败
			            	Log.i("tag", "设置信息错误");
			            	//发送消息
			            	Message msg = new Message();
			            	msg. what=States.SETUP_INFO_ERROR ;
			            	handler.sendMessage(msg); 
						}
			            if(result.getResultCode() == States.SETUP_ERROR) {//数据库错误
			            	Log.i("tag", "数据库错误");
			            	//发送消息
			            	Message msg = new Message();
			            	msg. what=States.SETUP_ERROR ;
			            	handler.sendMessage(msg); 
						}
					}
					
					@Override
					public void onError(String info) {
						// TODO Auto-generated method stub
						Log.i("tag", "setUserInfo请求失败");
						
					}
				});//提交回调
            	
            	mySetNickName = MySetNickName.getText().toString();
            	//回调给前面显示页面
            	Intent intent = getIntent();
            	Bundle bundle = new Bundle();
                bundle.putString( "mySetNickName" , mySetNickName);
                bundle.putString( "setbirdayString" , setbirdayString);
                bundle.putString("location", location);
                bundle.putString("emotionState", emotionState);
                bundle.putString("setInterest", setInterest);
                bundle.putString("job", job);
                bundle.putString("setalwayslocation", setalwayslocation);
                Log.i("tag", mySetNickName+"hichujian_my_setting_edit  mySetNickName");
                intent.putExtras(bundle);
                setResult(0x168,intent);
            	finish();
            	
            	
            	
            	}
        });
        
        
	}
}
