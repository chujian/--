package com.hichujian.client.ui.setting;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.PushAPI;
import com.hichujian.client.model.PushSettingDO;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.ui.ActivityDetails;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class OrdPush extends Activity{
	private ImageButton back_to_login88;
	private ImageView image1;
	private String[] items,items1;//各列表项要显示的内容
	private Long  UserID;
	private static int value1=1;
	boolean flag1=false;		//all
	boolean flag2=false;		//sporting
	boolean flag3=false;		//traveling
	boolean flag4=false;		//reading
	boolean flag5=false;		//eating
	boolean flag6=false;		//singing
	boolean flag7=false;		//tablegame
	boolean flag8=false;		//moving
	boolean flag9=false;		//computergame
	private boolean flag[]=new boolean[9];
	private Handler handler;
	private LinearLayout all,sporting,traveling,reading,eating,singing,tablegame,moving,computergame;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.ord_push);
        final TextView order_distance_text = (TextView)findViewById(R.id.order_distance_text);
        final TextView order_frequency_text = (TextView)findViewById(R.id.order_frequency_text);
        //获取UserId
        SharedPreferences preferences = getSharedPreferences("User" , Context.MODE_PRIVATE);
        String UserId = preferences.getString( "UserId", "");
        UserID = Long. valueOf (UserId);
        Log. i("tag",Long. valueOf(UserId)+"Long型");
        Log. i("tag", UserId+ "");

        handler= new Handler(){
        	@Override 
            public void handleMessage(Message msg) { 
        		if(msg.what == States.PUSH_SET_SUCCESSFUL){
                    Toast. makeText(OrdPush.this, "推送设置成功" , Toast.LENGTH_SHORT).show();
        		}
        		if(msg.what == States.PUSH_SET_JSON_ERROR){
                    Toast. makeText(OrdPush.this, "推送设置信息json格式错误" , Toast.LENGTH_SHORT).show();
        		}
        		if(msg.what == States.PUSH_SET_OTHER_ERROR){
                    Toast. makeText(OrdPush.this, "推送设置其他错误" , Toast.LENGTH_SHORT).show();
        		}

            } 
        };
        
        //返回
        back_to_login88=(ImageButton)findViewById(R.id.back_to_login88);
        back_to_login88.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	PushSettingDO pushsettingdo = new PushSettingDO();
            	if(value1==1){
            		pushsettingdo.setSex("男");
            	}else if(value1==2){
            		pushsettingdo.setSex("女");
            	}else if(value1==3){
            		pushsettingdo.setSex("不限");
            	}
            	
            	//获取选中了哪几个活动，1为选中，0为未选中
    			flag[0] = flag1;
    			flag[1] = flag2;
    			flag[2] = flag3;
    			flag[3] = flag4;
    			flag[4] = flag5;
    			flag[5] = flag6;
    			flag[6] = flag7;
    			flag[7] = flag8;
    			flag[8] = flag9;
    			
            	String activityTheme[] = {"随便","运动","旅行","阅读","聚餐","K歌","桌游","电影","网游"};
    			List<String> activityTyps = new ArrayList<String>();
    			if(flag[0]==true){
    				activityTyps=null;
    			}else{
    				for(int i=1;i<=8;i++){
    					if(flag[i]==true)
    						activityTyps.add(activityTheme[i]);
    				}
    			}
        		String order_distance_text_string = order_distance_text.getText().toString();
        		double distance = 5000;
        		if(order_distance_text_string.equals("5公里")){
        			distance = 5000;
        		}else if(order_distance_text_string.equals("10公里")){
        			distance = 10000;
        		}else if(order_distance_text_string.equals("15公里")){
        			distance = 15000;
        		}else if(order_distance_text_string.equals("20公里")){
        			distance = 20000;
        		}else if(order_distance_text_string.equals("20公里以上")){
        			distance = 100000;
        		}
        		
        		String order_frequency_text_string = order_frequency_text.getText().toString();
        		int pushFrequency = 1; 
        		if(order_frequency_text_string.equals("实时推送")){
        			pushFrequency = 1;
        		}else if(order_frequency_text_string.equals("一小时一次")){
        			pushFrequency = 2;
        		}else if(order_frequency_text_string.equals("一天一次")){
        			pushFrequency = 3;
        		}else if(order_frequency_text_string.equals("不推送")){
        			pushFrequency = 4;
        		}else
            	pushsettingdo.setActivityType(String.valueOf(activityTyps));
            	pushsettingdo.setUserID(UserID);
            	pushsettingdo.setDistance(distance);
            	pushsettingdo.setPushFrequency(pushFrequency);
            	
            	
            	PushAPI pushAPI = PushAPI.getInstance();
            	pushAPI.chagePushSettings(UserID, pushsettingdo, new OnResponseListener() {

        			public void onResponse(String data) {
        				// TODO Auto-generated method stub
        				Log.i("tag","请求成功");
        				Log.i("tag",data);
        				final ResponseDO result=JSON.parseObject(data,ResponseDO.class);
        				if(result.getResultCode()==States.PUSH_SET_SUCCESSFUL){//设置成功
        					Message msg = new Message();  
            				msg.what =States.PUSH_SET_SUCCESSFUL;//
            				handler.sendMessage(msg); 
        				}
        				if(result.getResultCode()==States.PUSH_SET_JSON_ERROR){//推送设置信息json格式错误
        					Message msg = new Message();  
            				msg.what = States.PUSH_SET_JSON_ERROR;//
            				handler.sendMessage(msg); 
        				}
        				if(result.getResultCode()==States.PUSH_SET_OTHER_ERROR){//推送设置其他错误
        					Message msg = new Message();  
            				msg.what = States.PUSH_SET_OTHER_ERROR;//
            				handler.sendMessage(msg); 
        				}


        				
        			}
        			@Override
        			public void onError(String info) {
        				// TODO Auto-generated method stub
        				Log.i("tag",info);
        				Log.i("tag","请求失败");
        			}
        		});	//回调结束
            	finish();
           }
        });
        
      //获取不限，男，女3个
      		final TextView buxian2 = (TextView)findViewById(R.id.buxian3);
      		final TextView man2 = (TextView)findViewById(R.id.man3);
      		final TextView woman2 = (TextView)findViewById(R.id.woman3);
      		//不限
              buxian2.setOnClickListener(new ImageButton.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				value1=3;
      				//不限的设置背景
      				buxian2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_buxian_gray_shape));
      				man2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      				woman2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_woman_white_shape));
      			}
      		});
              
              //男
              man2.setOnClickListener(new TextView.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				value1=1;
      				//男的设置背景
      				buxian2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_buxian_white_shape));
      				man2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      				woman2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_woman_white_shape));

      			}
      		});
              //女
              woman2.setOnClickListener(new TextView.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				value1=2;
      				//女的设置背景
      				buxian2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_buxian_white_shape));
      				man2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      				woman2.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_woman_gray_shape));
      			}
      		});
              
              //筛选全部活动
              all=(LinearLayout)findViewById(R.id.all1);
              sporting=(LinearLayout)findViewById(R.id.sporting1);
              traveling=(LinearLayout)findViewById(R.id.traveling1);
              reading=(LinearLayout)findViewById(R.id.reading1);
              eating=(LinearLayout)findViewById(R.id.eating1);
              singing=(LinearLayout)findViewById(R.id.singing1);
              tablegame=(LinearLayout)findViewById(R.id.tablegame1);
              moving=(LinearLayout)findViewById(R.id.moving1);
              computergame=(LinearLayout)findViewById(R.id.computergame1);
              
              all.setOnClickListener(new LinearLayout.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				if(flag1==true){	//已经选中
      					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
      					sporting.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					traveling.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_right_white_shape));
      					reading.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					eating.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					singing.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					tablegame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_bottom_left_white_shape));
      					moving.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					computergame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_bottom_right_white_shape));
      					flag1=false;
      					flag2=false;
      					flag3=false;
      					flag4=false;
      					flag5=false;
      					flag6=false;
      					flag7=false;
      					flag8=false;
      					flag9=false;
      				}else{	//未选中
      					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_gray_shape));
      					sporting.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					traveling.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_right_gray_shape));
      					reading.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					eating.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					singing.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					tablegame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_bottom_left_gray_shape));
      					moving.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					computergame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_bottom_right_gray_shape));
      					flag1=true;
      					flag2=true;
      					flag3=true;
      					flag4=true;
      					flag5=true;
      					flag6=true;
      					flag7=true;
      					flag8=true;
      					flag9=true;
      				}
      			}
      		});
              
              sporting.setOnClickListener(new LinearLayout.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				if(flag2==true){	//已经选中
      					sporting.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
      					flag1=false;
      					flag2=false;
      				}else{	//未选中
      					sporting.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					flag2=true;
      					
      				}
      			}
      		});
              
              traveling.setOnClickListener(new LinearLayout.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				if(flag3==true){	//已经选中
      					traveling.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
      					flag1=false;
      					flag3=false;
      				}else{	//未选中
      					traveling.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					flag3=true;
      				}
      			}
      		});
              
              reading.setOnClickListener(new LinearLayout.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				if(flag4==true){	//已经选中
      					reading.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
      					flag1=false;
      					flag4=false;
      				}else{	//未选中
      					reading.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					flag4=true;
      				}
      			}
      		});
              
              eating.setOnClickListener(new LinearLayout.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				if(flag5==true){	//已经选中
      					eating.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
      					flag1=false;
      					flag5=false;
      				}else{	//未选中
      					eating.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					flag5=true;
      				}
      			}
      		});
              
              singing.setOnClickListener(new LinearLayout.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				if(flag6==true){	//已经选中
      					singing.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
      					flag1=false;
      					flag6=false;
      				}else{	//未选中
      					singing.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					flag6=true;
      				}
      			}
      		});
              
              tablegame.setOnClickListener(new LinearLayout.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				if(flag7==true){	//已经选中
      					tablegame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
      					flag1=false;
      					flag7=false;
      				}else{	//未选中
      					tablegame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					flag7=true;
      				}
      			}
      		});
              
              moving.setOnClickListener(new LinearLayout.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				if(flag8==true){	//已经选中
      					moving.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
      					flag1=false;
      					flag8=false;
      				}else{	//未选中
      					moving.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					flag8=true;
      				}
      			}
      		});
              
              computergame.setOnClickListener(new LinearLayout.OnClickListener() {
      			@Override
      			public void onClick(View arg0) {
      				if(flag9==true){	//已经选中
      					computergame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_white_shape));
      					all.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_shuaixuan_top_left_white_shape));
      					flag1=false;
      					flag9=false;
      				}else{	//未选中
      					computergame.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_order_object_man_gray_shape));
      					flag9=true;
      				}
      			}
      		});
              
              
              LinearLayout order_distance = (LinearLayout)findViewById(R.id.order_distance);
              order_distance.setOnClickListener(new LinearLayout.OnClickListener() {
        			@Override
        			public void onClick(View arg0) {
        				//
        				items=new String[]{"5公里","10公里","15公里","20公里","20公里以上"};
        				Builder builder=new AlertDialog.Builder(OrdPush.this);
        				builder.setIcon(R.drawable.cha);
        				builder.setTitle("订阅的范围:");
        				//添加列表项
        				builder.setItems(items,new DialogInterface.OnClickListener() {
        					
        					@Override
        					public void onClick(DialogInterface arg0, int which) {
        						// TODO Auto-generated method stub
        						order_distance_text.setText(items[which]);
        					}
        				});
        				builder.create().show();
        			}
        		});
              
              
              LinearLayout order_frequency = (LinearLayout)findViewById(R.id.order_frequency);
              order_frequency.setOnClickListener(new LinearLayout.OnClickListener() {
        			@Override
        			public void onClick(View arg0) {
        				//
        				items1=new String[]{"实时推送","一小时一次","一天一次","不推送"};
        				Builder builder=new AlertDialog.Builder(OrdPush.this);
        				builder.setIcon(R.drawable.cha);
        				builder.setTitle("订阅的推送的频率:");
        				//添加列表项
        				builder.setItems(items1,new DialogInterface.OnClickListener() {
        					
        					@Override
        					public void onClick(DialogInterface arg0, int which) {
        						// TODO Auto-generated method stub
        						order_frequency_text.setText(items1[which]);
        					}
        				});
        				builder.create().show();
        			}
        		});
              
      	}//end of onCreate
      	
      }