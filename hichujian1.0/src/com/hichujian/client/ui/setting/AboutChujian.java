package com.hichujian.client.ui.setting;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.example.hichujian.R;

public class AboutChujian extends Activity{
private ImageButton back_to_login92;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		setContentView(R.layout.aboutchujian);
		//设置按钮
		back_to_login92=(ImageButton)findViewById(R.id.back_to_login92);
		back_to_login92.setOnClickListener( new OnClickListener() {
					public void onClick(View v) {
						finish();
					}
				});
	}
}
