package com.hichujian.client.ui.setting;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.hichujian.R;

public class ImageViewShow extends Activity{
	private LinearLayout imageshowlayout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		setContentView(R.layout.imageviewshow);


		//点击缩小图片
		imageshowlayout=(LinearLayout)findViewById(R.id.imageshowlayout);
		imageshowlayout.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
	}
}
