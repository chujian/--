package com.hichujian.client.ui.setting;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.UserAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class ResetPsaaword extends Activity{
	private EditText ResetOldPassword , ResetNewPassword , ResetConfirmNewPassword;
	String ResetOldPasswordString,ResetNewPasswordString,ResetConfirmNewPasswordString;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		setContentView(R.layout.reset_password);
		//就密码，新密码，新的确认密码
		ResetOldPassword = (EditText)findViewById(R.id.ResetOldPassword);
		ResetNewPassword = (EditText)findViewById(R.id.ResetNewPassword);
		ResetConfirmNewPassword = (EditText)findViewById(R.id.ResetConfirmNewPassword);
		
		//获取用户手机号
        SharedPreferences preferences3 = getSharedPreferences("phonenumber" , Context.MODE_PRIVATE);
        final String PhoneNumber = preferences3.getString( "PhoneNumber", "");
        Log. i("tag", PhoneNumber.toString());

        //获取UserId
        SharedPreferences preferences = getSharedPreferences("User" , Context.MODE_PRIVATE);
        String UserId = preferences.getString( "UserId", "");
        final Long  UserID = Long. valueOf (UserId);
        Log. i("tag",Long. valueOf(UserId)+"Long型");
        Log. i("tag", UserId+ "");

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
               if(msg.what ==States.PWD_CHANGE_SUCCESSFUL){     //重置密码成功
                   Toast. makeText(ResetPsaaword.this, "重置密码成功哦~" , Toast.LENGTH_SHORT).show();
                   Log. i ("tag" , "重置密码成功" );
                   finish();
                }
               if(msg.what ==States.PWD_CHANGE_OLD_PWD_ERROR){     //旧密码错误
                   Toast. makeText(ResetPsaaword.this, "旧密码错误哦~" , Toast.LENGTH_SHORT).show();
                   Log. i ("tag" , "旧密码错误" );
                }
               if(msg.what ==States.PWD_CHANGE_OTHER_ERROR){     //验证密码其他错误
                   Toast. makeText(ResetPsaaword.this, "验证密码其他错误哦~" , Toast.LENGTH_SHORT).show();
                   Log. i ("tag" , "验证密码其他错误" );
                }
            }
        }; 

        Button ConfirmResetPassword = (Button)findViewById(R.id.ConfirmResetPassword);
        ConfirmResetPassword.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				//新密码的字符串
				ResetOldPasswordString = ResetOldPassword.getText().toString();
				ResetNewPasswordString = ResetNewPassword.getText().toString();
				ResetConfirmNewPasswordString = ResetConfirmNewPassword.getText().toString();
				
				if(ResetNewPassword.length()<6){//新密码小于6位
					 Toast. makeText(ResetPsaaword.this, "新密码必须要大于6位哦~" , Toast.LENGTH_SHORT).show();
				}else{//新密码大于6位
					if(ResetConfirmNewPassword.length()<6){//确认密码小于6位
						Toast. makeText(ResetPsaaword.this, "确认密码必须要大于6位哦~" , Toast.LENGTH_SHORT).show();
					}else{//确认密码大于6位
						if(!ResetNewPasswordString.equals(ResetConfirmNewPasswordString)){//新密码和确认密码不相等
							Toast. makeText(ResetPsaaword.this, "新密码和确认密码不相等哦~" , Toast.LENGTH_SHORT).show();
						}else{//所有条件都满足
							UserAPI userAPI = UserAPI.getInstance();
							userAPI.changePassword(UserID, ResetOldPasswordString, ResetNewPasswordString, new OnResponseListener() {
								
								@Override
								public void onResponse(String data) {
									Log.i("tag",PhoneNumber.toString());
									Log.i("tag", data);
									ResponseDO result=JSON.parseObject(data,ResponseDO. class);
									if(result.getResultCode()==States.PWD_CHANGE_SUCCESSFUL){//重置密码成功
										Message msg = new Message();
										msg. what=States.PWD_CHANGE_SUCCESSFUL ;
										handler.sendMessage(msg); 
									}
									if(result.getResultCode()==States.PWD_CHANGE_OLD_PWD_ERROR){//验证旧密码错误
										Message msg = new Message();
										msg. what=States.PWD_CHANGE_OLD_PWD_ERROR ;
										handler.sendMessage(msg); 
									}
									if(result.getResultCode()==States.PWD_CHANGE_OTHER_ERROR){//更改密码其他错误
										Message msg = new Message();
										msg. what=States.PWD_CHANGE_OTHER_ERROR ;
										handler.sendMessage(msg); 
									}
									
								}
								
								@Override
								public void onError(String info) {
									// TODO Auto-generated method stub
									Log.i("tag", "请求失败");
									
								}
							});
						}
					}
				}//if结束
				
			}//click
		});//button结束
		
	}//end of oncreate
}//end