package com.hichujian.client.ui;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.ActivityAPI;
import com.hichujian.client.model.ActivityDO;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.model.SignUpDO;
import com.hichujian.client.ui.TabSquare.ViewHolder;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.ImageLoadUtils;

public class TabMyActivity extends Fragment{
	SectionsPagerAdapter mSectionsPagerAdapter;
	ViewPager mViewPager;
	private TextView ivCursor;
	private static final int tabNum = 2;
	private static int selectedPage = 0;
	private static int preSelectedPage = 0;
	
	private static int scrollState;
	private static final int SCROLL_STATE_PRESS = 1;
	private static final int SCROLL_STATE_UP = 2;
	private static final int SCROLL_STATE_END = 0;
	private float unitWidth;
	private float currentPositionPix;
	private TextView tvTab0, tvTab1;//, tvTab2
	private boolean isClick = false;
	private ImageView  fabu_activity2;
	private static ListView ActivityMy,activityMySecond;
	private static Long  UserID;
	private static Handler handler;
	private static Handler handler1;
	private ArrayList<HashMap<String, Object>> IistItem,IistItem1;
	private List<ActivityDO> list;
	private List<SignUpDO> list1;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.dating02, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//获取UserId
        SharedPreferences preferences = getActivity().getSharedPreferences("User" , Context.MODE_PRIVATE);
        String UserId = preferences.getString( "UserId", "");
        UserID = Long. valueOf (UserId);
        Log. i("tag",Long. valueOf(UserId)+"Long型");
        Log. i("tag", UserId+ "");

		//发布活动
		fabu_activity2=(ImageView)getView().findViewById(R.id.fabu_activity2);
		fabu_activity2.setOnClickListener( new OnClickListener() {
            public void onClick(View v) {
            	Intent intent=new Intent();
                intent.setClass( getActivity(),  ActivityFabu.class);
                TabMyActivity.this.startActivity(intent);
           }
        });
		setCursorWidth();
		initTabListener();
		
		// Create the adapter that will return a fragment for each of the two
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getActivity().getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager)getView().findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
//						System.out.println("onPageSelected------position" + position);
						selectedPage = position;
					}
					@Override
					public void onPageScrolled(int position,float positionOffset, int positionOffsetPixels) {
//						System.out.println("onPageScrolled------position" + position);
//						System.out.println("onPageScrolled------position" + position);
//						System.out.println("onPageScrolled------positionOffset" + positionOffset);
//						System.out.println("onPageScrolled------positionOffsetPixels" + positionOffsetPixels);
						//当前的page和position相等，说明在向左滑动；vice versa
						if(!isClick){
							if(positionOffsetPixels != 0){
								if(scrollState == SCROLL_STATE_PRESS){//手指按下的状态
									if(selectedPage == position){//表示往左拉，相应的tab往右走
										ivCursor.setTranslationX(currentPositionPix + positionOffsetPixels / tabNum);
									}else{//表示往右拉
										ivCursor.setTranslationX(currentPositionPix - (unitWidth - positionOffsetPixels / tabNum));
									}
								}else if(scrollState == SCROLL_STATE_UP){//手指抬起的状态
//									System.out.println("preSelectedPage---" + preSelectedPage);
//									System.out.println("position---" + position);
									if(preSelectedPage == position){//往左拉
										ivCursor.setTranslationX(currentPositionPix + positionOffsetPixels / tabNum);
									}else{//表示往右拉
										ivCursor.setTranslationX(currentPositionPix - (unitWidth - positionOffsetPixels / tabNum));
									}
								}
							}
						}
					}
					@Override
					public void onPageScrollStateChanged(int state) {
//						System.out.println("onPageScrollStateChanged------state" + state);
						if(!isClick){
							currentPositionPix = selectedPage * unitWidth;
							scrollState = state;
							preSelectedPage = selectedPage;
						}
					}
				});
		
		handler1 = new Handler() {//解析成SignUpD
            @Override
            public void handleMessage(Message msg1) {
                list1 = (List<SignUpDO>)msg1.obj;
                Log. i ("tag" , "我报名的"+list1);
                Log.i("tag", "getDate1开始~~");
                IistItem1=getDate1();
                Log.i("tag", "结束");
                MyAdapter1 myAdaper1=new MyAdapter1(getActivity());
				Log.i("tag","创建MyAdapter");
				activityMySecond = (ListView)getView().findViewById(R.id.activityMySecond);
				activityMySecond.setAdapter(myAdaper1);
				
				activityMySecond.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
						// TODO Auto-generated method stub
						Log.v("MyListViewBase", "你点击了ListView条目" + position);// 在LogCat中输出信息
						Toast.makeText(getActivity(),"你点击了Item", Toast.LENGTH_SHORT).show();
						//position=position-1;
						Long UserIDActivity = UserID; //Long.valueOf(list1.get(position).getsgetsignUper().getUserID());
						Long ActivityID = Long.valueOf(list1.get(position).getActivity().getActivityID());

						Intent intent=new Intent();
						intent.setClass( getActivity(),  ActivityDetails.class);
						//把活动的用户ID和发起该活动的用户Id传入到活动详情中用于判断是不是他自己发起的活动
						Bundle bundle=new Bundle();
						bundle.putInt("position",position);
						bundle.putLong("UserIDActivity",UserIDActivity);
						bundle.putLong("ActivityID",ActivityID);
						Log.i("tag",String.valueOf(UserIDActivity)+"     TabMyActivity");
						Log.i("tag",String.valueOf(ActivityID)+"     TabMyActivity");
						intent.putExtras(bundle);
						TabMyActivity.this.startActivity(intent);
					}
				});//listView_activity_dating
            }
        };

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                list = (List<ActivityDO>)msg.obj;
                Log. i ("tag" , "我报名的"+list);
                Log.i("tag", "getDate开始");
                IistItem = getDate();
                Log.i("tag", "结束");
                MyAdapter myAdaper=new MyAdapter(getActivity());
				Log.i("tag","创建MyAdapter");
				ActivityMy = (ListView)getView().findViewById(R.id.activityMy);
				ActivityMy.setAdapter(myAdaper);
				
				ActivityMy.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
						// TODO Auto-generated method stub
						Log.v("MyListViewBase", "你点击了ListView条目" + position);// 在LogCat中输出信息
						Toast.makeText(getActivity(),"你点击了Item", Toast.LENGTH_SHORT).show();
						//position=position-1;
						Long UserIDActivity = Long.valueOf(list.get(position).getUserID());
						Long ActivityID = Long.valueOf(list.get(position).getActivityID());

						Intent intent=new Intent();
						intent.setClass( getActivity(),  ActivityDetails.class);
						//把活动的用户ID和发起该活动的用户Id传入到活动详情中用于判断是不是他自己发起的活动
						Bundle bundle=new Bundle();
						bundle.putInt("position",position);
						bundle.putLong("UserIDActivity",UserIDActivity);
						bundle.putLong("ActivityID",ActivityID);
						Log.i("tag",String.valueOf(UserIDActivity)+"     TabMyActivity");
						Log.i("tag",String.valueOf(ActivityID)+"     TabMyActivity");
						intent.putExtras(bundle);
						TabMyActivity.this.startActivity(intent);
					}
				});//listView_activity_dating
            }
        };

		}//end of onCreate 
	
	private void initTabListener() {
		tvTab0 = (TextView)getView().findViewById(R.id.tab1_text);
		tvTab1 = (TextView)getView().findViewById(R.id.tab2_text);
		
		tvTab0.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(selectedPage == 1){
					mViewPager.setCurrentItem(0, true);
				}
			}
		});
		tvTab1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(selectedPage == 0){
					mViewPager.setCurrentItem(1, true);
				}
			}
		});
		
	}
	public boolean dispatchTouchEvent(MotionEvent ev) {
		getActivity().onTouchEvent(ev);
		return super.getActivity().dispatchTouchEvent(ev);
	}
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getActivity().getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	//我报名的
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			/*Fragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position);
			fragment.setArguments(args);
			return fragment;*/
			Log.i("tag","获得position"+String.valueOf(position));
			Fragment fragment ;
			if(position==1){//我发起的(右边)
				fragment = new DummySectionFragment();
				Bundle args = new Bundle();
				args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position);
				fragment.setArguments(args);
				Log.i("tag","获得position1111111111");
			}else{//我报名的(左边)
				fragment = new MySetUpFragment();
				Bundle args = new Bundle();
				args.putInt(MySetUpFragment.ARG_SECTION_NUMBER, position);
				fragment.setArguments(args);
				Log.i("tag","获得position0000000000");
			}
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 2 total pages.
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			}
			return null;
		}
	}

	
	//我发起的(右边)
	public static class DummySectionFragment extends Fragment {
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@SuppressLint("ResourceAsColor")
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main_dummy,
					container, false);
			
			RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.fragment_layout);
			
				rl.setBackgroundResource(android.R.color.white);
				//获取我报名的活动详情
				ActivityAPI activityAPI = ActivityAPI.getInstance();
				activityAPI.getMyPublishedActivityList(UserID, 0, 10, new OnResponseListener() {
					
					@Override
					public void onResponse(String data) {
						// TODO Auto-generated method stub
						Log.i("tag", "请求成功");
						Log.i("tag", data+"获取我发起的活动");
						ResponseDO result=JSON.parseObject(data,ResponseDO. class);
                        List<ActivityDO> list=(List<ActivityDO>) JSON.parseArray(result.getData().toString(), ActivityDO.class);
                        Log.i("tag",list+"list");
                        //发送消息
                        Message msg = new Message();
                        msg.obj = list;
                        handler.sendMessage(msg);
					}
					
					@Override
					public void onError(String info) {
						// TODO Auto-generated method stub
						Log.i("tag", "我发起的活动请求失败");
						Log.i("tag", info+"我发起的活动");
						
					}
				});//获取我报名的活动详情
			return rootView;
		}
	}
	
	
	
	//我报名的(左边)
	public static class MySetUpFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public MySetUpFragment() {
		}

		@SuppressLint("ResourceAsColor")
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main_dummy_second,
					container, false);
			
			RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.fragment_layout_second);

				rl.setBackgroundResource(android.R.color.white);
				Log.i("tag", "请求MySetUpFragment功");
				//获取我报名的活动详情
				ActivityAPI activityAPI = ActivityAPI.getInstance();
				activityAPI.getMySignUpedActivityList(UserID, 0, 10, new OnResponseListener() {
					
					@Override
					public void onResponse(String data) {
						// TODO Auto-generated method stub
						Log.i("tag", "请求成功");
						Log.i("tag", data+"获取我报名的活动详情");
						ResponseDO result=JSON.parseObject(data,ResponseDO. class);
                        List<SignUpDO> list1=(List<SignUpDO>) JSON.parseArray(result.getData().toString(), SignUpDO.class);
                        Log.i("tag",list1+"list1");
                        //发送消息
                        Message msg1 = new Message();
                        msg1.obj = list1;
                        handler1.sendMessage(msg1);
					}
					
					@Override
					public void onError(String info) {
						// TODO Auto-generated method stub
						Log.i("tag", "获取我报名的活动详情失败");
						Log.i("tag", info+"我发起的活动");
						
					}
				});//获取我报名的活动详情
				
			return rootView;
		}
	}
	
	
	
	
	/**设置cursor的宽度，并获取移动的单位长度float**/
	public void setCursorWidth(){
		ivCursor = (TextView)getView().findViewById(R.id.cursor_text);
		int cursorWidth = getWindowWidth()/tabNum;
		unitWidth = (float)getWindowWidth()/tabNum;
		int cursorHeight = (int) getResources().getDimension(R.dimen.cursor_height);
		
		ViewGroup.LayoutParams params = ivCursor.getLayoutParams();
		params.height = cursorHeight;
		params.width = cursorWidth;
		
		ivCursor.setLayoutParams(params);
	}
	/**获取屏幕宽度**/
	public int getWindowWidth(){
		DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm.widthPixels;
	}
	
	
	//我发起的把数据传进去（右边）
		private ArrayList<HashMap<String, Object>> getDate() {
			ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
			Log.i("tag", String.valueOf(list.size())+"条记录");

			//把数据丢进去
			for (int i = 0; i < list.size(); i++) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("activity_theme", list.get(i).getActivityContent());
				//map.put("activity_nickname",list.get(i).getNickname());
				map.put("activity_place", list.get(i).getActivityPlace());
				map.put("activity_time",list.get(i).getActivityTime());
				map.put("activity_distance",new DecimalFormat("0.00").format(list.get(i).getDistance()));
				map.put("activity_setup",list.get(i).getSignUpNumber());
				map.put("activity_comment",list.get(i).getCommentNumber());
				//map.put("activity_sex",list.get(i).getSex());
				/*map.put("activity_head",list.get(i).getHeadPortrait());
				Log.i("tag",list.get(i).getActivityContent()+"\n");*/
				listItem.add(map);
			}
			return listItem;
		}
		
		
		//我报名的的把数据传进去（左边）
		private ArrayList<HashMap<String, Object>> getDate1() {
			ArrayList<HashMap<String, Object>> listItem1 = new ArrayList<HashMap<String, Object>>();
			Log.i("tag", String.valueOf(list1.size())+"条记录");
			Log.i("tag","进入getDate1");
			
			//把数据丢进去
			for (int i = 0; i < list1.size(); i++) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("activity_theme", list1.get(i).getActivity().getActivityContent());
				map.put("activity_nickname",list1.get(i).getActivity().getNickname());
				map.put("activity_place", list1.get(i).getActivity().getActivityPlace());
				map.put("activity_time",list1.get(i).getActivity().getActivityTime());
				map.put("activity_distance",new DecimalFormat("0.00").format(list1.get(i).getActivity().getDistance()));
				map.put("activity_setup",list1.get(i).getActivity().getSignUpNumber());
				map.put("activity_comment",list1.get(i).getActivity().getCommentNumber());
				map.put("activity_sex",list1.get(i).getActivity().getSex());
				map.put("activity_head",list1.get(i).getActivity().getHeadPortrait());
				listItem1.add(map);
			}
			Log.i("tag","out getDate1");
			Log.i("tag",String.valueOf(listItem1)+"listItem1内容");
			return listItem1;
		}
				
				
	//创建我发起的MyAdapter内部类(右边)
		private class MyAdapter extends BaseAdapter{
			private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
			ViewHolder holder;
			private MyAdapter(Context context){
				this.mInflater=LayoutInflater.from(context);
				Log.i("tag", "MyAdapter构造成功");
			}


			@Override
			public int getCount() {
				
				// TODO Auto-generated method stub
				Log.i("tag", "getCount");
				Log.i("tag", String.valueOf(getDate().size()));

				return getDate().size();
			}
			@Override
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				Log.i("tag", "getItem");
				return null;

			}

			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub.
				Log.i("tag", "getItemId");
				return 0;
			}

			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				Log.i("tag", "进入getView");
				// 观察convertView随ListView滚动情况
				Log.v("tag", "getView " + position + " " + convertView);
				if (convertView == null) {
					Log.i("tag", "得到各个控件的对象");
					convertView = mInflater.inflate(R.layout.dating01_item_picture, null);
					holder = new ViewHolder();
					/* 得到各个控件的对象 */
					holder.activity_theme = (TextView) convertView.findViewById(R.id.activity_theme1);
					holder.activity_place = (TextView) convertView.findViewById(R.id.activity_place1);
					holder.activity_time = (TextView) convertView.findViewById(R.id.activity_time1);
					holder.activity_distance = (TextView) convertView.findViewById(R.id.activity_distance1);
					holder.activity_setup = (TextView) convertView.findViewById(R.id.activity_setup1);
					holder.activity_comment = (TextView) convertView.findViewById(R.id.activity_comment1);
					//holder.activity_head = (ImageView) convertView.findViewById(R.id.activity_head);
					convertView.setTag(holder);
				}// 绑定ViewHolder对象 当convertView不为空时 不需要再fingViewById()来找每个控件了 只需要convertView.hol
				else {
					holder = (ViewHolder) convertView.getTag();
					Log.i("tag", "取出ViewHolder对象");
					// 取出ViewHolder对象
				}
				/* 设置TextView显示的内容，即我们存放在动态数组中的数据 */
				/*if(list.get(position).getSex().equals("女")){
					holder.activity_sex.setBackgroundDrawable(getResources().getDrawable(R.drawable.activity_woman));
				}else{
					holder.activity_sex.setBackgroundDrawable(getResources().getDrawable(R.drawable.activity_man));
				}*/
				holder.activity_theme.setText(IistItem.get(position).get("activity_theme").toString());
			//	holder.activity_nickname.setText(IistItem.get(position).get("activity_nickname").toString());
				holder.activity_place.setText(IistItem.get(position).get("activity_place").toString());
				holder.activity_time.setText(IistItem.get(position).get("activity_time").toString());
				holder.activity_distance.setText(IistItem.get(position).get("activity_distance").toString()+"km");
				holder.activity_place.setText(IistItem.get(position).get("activity_place").toString());
				holder.activity_setup.setText(IistItem.get(position).get("activity_setup").toString());
				holder.activity_comment.setText(IistItem.get(position).get("activity_comment").toString());
				Log.i("tag", "设置TextView显示的内容，即我们存放在动态数组中的数据");
				return convertView;
			}


		}
		/* 存放控件 */
		public class ViewHolder {
			//public ImageView activity_sex;
			public TextView activity_theme;
			//public TextView activity_nickname;
			public TextView activity_place;
			public TextView activity_time;
			public TextView activity_distance;
			public TextView activity_comment;
			public TextView activity_setup;
			//public ImageView activity_head;
		}
		
		
		
		//创建我报名的MyAdapter1内部类(左边)
		private class MyAdapter1 extends BaseAdapter{
			private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
			ViewHolder1 holder1;
			private MyAdapter1(Context context){
				this.mInflater=LayoutInflater.from(context);
				Log.i("tag", "MyAdapter1构造成功");
			}


			@Override
			public int getCount() {

				// TODO Auto-generated method stub
				Log.i("tag", "getCount");
				Log.i("tag", String.valueOf(getDate().size()));

				return getDate1().size();
			}
			@Override
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				Log.i("tag", "getItem");
				return null;

			}

			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub.
				Log.i("tag", "getItemId");
				return 0;
			}

			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				Log.i("tag", "进入getView 1");
				// 观察convertView随ListView滚动情况
				Log.v("tag", "getView1 " + position + " " + convertView);
				if (convertView == null) {
					Log.i("tag", "得到各个控件的对象 1");
					convertView = mInflater.inflate(R.layout.dating01_item_man, null);
					Log.i("tag", "12345");
					holder1 = new ViewHolder1();
					Log.i("tag", "12346");
					
					/* 得到各个控件的对象 */
					holder1.activity_theme = (TextView) convertView.findViewById(R.id.activity_theme);
					holder1.activity_place = (TextView) convertView.findViewById(R.id.activity_place);
					holder1.activity_time = (TextView) convertView.findViewById(R.id.activity_time);
					holder1.activity_distance = (TextView) convertView.findViewById(R.id.activity_distance);
					holder1.activity_setup = (TextView) convertView.findViewById(R.id.activity_setup);
					holder1.activity_comment = (TextView) convertView.findViewById(R.id.activity_comment);
					holder1.activity_head = (ImageView) convertView.findViewById(R.id.activity_head);
					holder1.activity_sex = (ImageView) convertView.findViewById(R.id.activity_sex);
					holder1.activity_nickname = (TextView) convertView.findViewById(R.id.activity_nickname);
					convertView.setTag(holder1);
				}// 绑定ViewHolder1对象 当convertView不为空时 不需要再fingViewById()来找每个控件了 只需要convertView.hol
				else {
					holder1 = (ViewHolder1) convertView.getTag();
					Log.i("tag", "取出ViewHolder1对象");
					// 取出ViewHolder1对象
				}
				Log.i("tag", "12346out");
				/* 设置TextView显示的内容，即我们存放在动态数组中的数据 */
				if(list.get(position).getSex().equals("女")){
					holder1.activity_sex.setBackgroundDrawable(getResources().getDrawable(R.drawable.activity_woman));
				}else{
					holder1.activity_sex.setBackgroundDrawable(getResources().getDrawable(R.drawable.activity_man));
				}
				Log.i("tag", "sex out");
				holder1.activity_theme.setText(IistItem1.get(position).get("activity_theme").toString());
				Log.i("tag", "sex out1");
				holder1.activity_nickname.setText(IistItem1.get(position).get("activity_nickname").toString());
				Log.i("tag", "sex out2");
				holder1.activity_place.setText(IistItem1.get(position).get("activity_place").toString());
				Log.i("tag", "sex out3");
				holder1.activity_time.setText(IistItem1.get(position).get("activity_time").toString());
				Log.i("tag", "sex out4");
				holder1.activity_distance.setText(IistItem1.get(position).get("activity_distance").toString()+"km");
				Log.i("tag", "sex out5");
				holder1.activity_place.setText(IistItem1.get(position).get("activity_place").toString());
				Log.i("tag", "sex out6");
				holder1.activity_setup.setText(IistItem1.get(position).get("activity_setup").toString());
				Log.i("tag", "sex out7");
				holder1.activity_comment.setText(IistItem.get(position).get("activity_comment").toString());
				Log.i("tag", "sex out8");
				ImageLoadUtils.loadImage(IistItem1.get(position).get("activity_head").toString(), holder1.activity_head, null);
				Log.i("tag", "设置TextView显示的内容，即我们存放在动态数组中的数据");
				return convertView;
			}
		}
		/* 存放控件 */
		public class ViewHolder1 {
			public ImageView activity_sex;
			public TextView activity_theme;
			public TextView activity_nickname;
			public TextView activity_place;
			public TextView activity_time;
			public TextView activity_distance;
			public TextView activity_comment;
			public TextView activity_setup;
			public ImageView activity_head;
		}	

}//end 1

