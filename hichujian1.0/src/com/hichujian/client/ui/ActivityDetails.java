package com.hichujian.client.ui;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.ActivityAPI;
import com.hichujian.client.api.CommentAPI;
import com.hichujian.client.model.ActivityDO;
import com.hichujian.client.model.CommentDO;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.ui.dialog.AlreadySetupDialog;
import com.hichujian.client.ui.dialog.DissolveDialog;
import com.hichujian.client.ui.dialog.ReportDialog;
import com.hichujian.client.ui.dialog.SetupDialog;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.AsyncRunner;
import com.hichujian.client.utils.States;

public class ActivityDetails extends Activity{
	//private ImageButton head_to_person_details;
	private ReportDialog report_dialog;
	private ImageButton back_to_login89;
	private Long UserIDActivity,ActivityID;
	public static int SetUpFlag = 0;
	private String ActivityContent,ActivityPlace,ActivityTime;
	private int  CommentNumber , SignUpNumber;
	private double Distance;
	private Long UserID;
	private int CODE_CANEL_SETUP = 0x717  ;//取消报名回调
	private int CODE_WANT_SETUP = 0x718; //我要报名回调
	private int CODE_DISSOLVE_SETUP = 0x719;//解散活动
	private int CODE_REPORT = 0x720;	//举报活动
	private int CODE_ALREADY_REPORT = 0x721; //取消举报活动
	private boolean isReported;//是否已经举报
	private List<CommentDO> listComment;
	private ArrayList<HashMap<String, Object>> IistItem;
	private ListView listViewComment;
	private MyAdapter myAdaper = null;
	private static Handler handlerBitmap;
	private static final int MAX_THREAD_COUNT = 5;//最大线程数量
	private static ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREAD_COUNT);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		setContentView(R.layout.activity_details);
		
		 SharedPreferences preferences = getSharedPreferences("User" , Context. MODE_PRIVATE);
         String UserId = preferences.getString( "UserId" , "" );
         UserID = Long. valueOf(UserId);
         Log. i( "tag",Long.valueOf(UserId)+ "Long型");
         Log. i( "tag", UserId+"hichujian_activity_details 用户ID");
         
		//获取活动详情ID和发起活动用户ID
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		UserIDActivity = bundle.getLong("UserIDActivity");
		ActivityID = bundle.getLong("ActivityID");
		Log.i("tag",String.valueOf(UserIDActivity)+"     hichujian_activity_details");
		Log.i("tag",String.valueOf(ActivityID)+"      hichujian_activity_details");
		
		//把一个布局扔进ListView里面
		listViewComment = (ListView)findViewById(R.id.listViewComment);
		LayoutInflater lif = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View headerView = lif.inflate(R.layout.activity_details_item_top, null, false);
		listViewComment.addHeaderView(headerView);
		
		Log.i("tag","捕获ActivityDetails Picture");
		
		LinearLayout activityDetailBackground = (LinearLayout) findViewById(R.id.activityDetailBackground);   //设置透明度
		activityDetailBackground.getBackground().setAlpha(77);

		final TextView nickname = (TextView)findViewById(R.id.activityDetailnNickname);	//昵称
		final TextView sex = (TextView)findViewById(R.id.sex);	//性别
		final TextView activityPlace = (TextView)findViewById(R.id.ActivityPlace);	//活动地点
		final ImageView head_to_person_details = (ImageView)findViewById(R.id.head_to_person_details);//头像
		final TextView distance = (TextView)findViewById(R.id.Distance);		//距离
		final TextView activityTime = (TextView)findViewById(R.id.ActivityTime);	//活动时间
		final TextView invitedObjectSex = (TextView)findViewById(R.id.InvitedObjectSex);	//邀请对象
		final TextView activityExplain = (TextView)findViewById(R.id.ActivityExplain);	//活动说明
		final Button setup = (Button)findViewById(R.id.setup);	//报名
		final Button comment = (Button)findViewById(R.id.comment);	//评论
		final TextView signUpNumber = (TextView)findViewById(R.id.SignUpNumber);	//报名数
		final TextView commentNumber = (TextView)findViewById(R.id.CommentNumber);	//评论数
		final TextView activityContent = (TextView)findViewById(R.id.ActivityContent);	//活动主题
		final TextView report = (TextView)findViewById(R.id.report);	//举报
		final LinearLayout ActivityCommentInput = (LinearLayout)findViewById(R.id.ActivityCommentInput);//输入总框
		final EditText ActivityCommentInputEdittext = (EditText)findViewById(R.id.ActivityCommentInputEdittext);//输入框
		final Button ActivityCommentCommit = (Button)findViewById(R.id.ActivityCommentCommit);	//评论
		final LinearLayout AlreadySetUp = (LinearLayout)findViewById(R.id.AlreadySetUp);	//已报名人跳转
		
		
		final Handler handler = new Handler() { 	//接受活动，用来更新主线程
			@Override 
			public void handleMessage(Message msg) { 
				ActivityDO activity = (ActivityDO)msg.obj;
				long activityID = activity.getActivityID();
				String Nickname = activity.getNickname();	//昵称
				ActivityContent = activity.getActivityContent();	//活动内容
				ActivityPlace = activity.getActivityPlace();	//获取活动地方
				ActivityTime = activity.getActivityTime().toString();	//获取活动时间
				String ActivityExplain = activity.getActivityExplain();	//获取活动说明
			    CommentNumber = activity.getCommentNumber();	//获取活动评论数
				SignUpNumber  = activity.getSignUpNumber();	//获取评论报名数
				String InvitedObjectSex = activity.getInvitedObjectSex();	//获取邀请的对象
				int    ExpectedNumber = activity.getExpectedNumber();	//获取邀请的人数
				Distance = activity.getDistance();
				String Sex = activity.getSex();				//获取性别
				String HeadPortrait = activity.getHeadPortrait();		//获取头像
				
				loadImageAndShow(HeadPortrait);
				
				if(UserIDActivity==UserID){	//活动是自己的
					Log.i("tag","活动是自己的，获取的内容");
					int activityDeleteStatus = activity.getActivityDeleteStatus();//获取活动是否被删除状态
					Log.i ("tag" ,String.valueOf(activity.getActivityDeleteStatus())+"活动删除状态");
					Log.i ("tag" , String.valueOf(activity.toString()));
					Log.i("tag", "昵称"+Nickname+"活动内容"+ActivityContent+"CommentNumber"+"评论数");

					Drawable drawable;
					//把数据扔进去
					nickname.setText(Nickname);
					sex.setText(Sex);
					//设置名字左边的
					if(Sex.equals("男")){
						drawable= getResources().getDrawable(R.drawable.activity_man);  
					}else{
						drawable= getResources().getDrawable(R.drawable.activity_woman);  
					}
					/// 这一步必须要做,否则不会显示.  
					drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());  
					sex.setCompoundDrawables(drawable,null,null,null); 
					activityPlace.setText(ActivityPlace);
					//head_to_person_details
					distance.setText(Distance+"");
					activityTime.setText(ActivityTime);
					invitedObjectSex.setText(InvitedObjectSex);
					activityExplain.setText(ActivityExplain);
					if(activityDeleteStatus==0){//未被解散
						setup.setText("解散活动");
						setup.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_setup_green_shape));
					}else{
						setup.setText("活动已解散");
						setup.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_setup_gray_shape));
					}
					comment.setText("评论");
					signUpNumber.setText(SignUpNumber+"");
					commentNumber.setText(CommentNumber+"");
					activityContent.setText(ActivityContent);
					AlreadySetUp.setVisibility(View.VISIBLE);//已报名人可见
					AlreadySetUp.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							 Intent intent=new Intent();
			                 intent.setClass(ActivityDetails.this, ActivityAlreadySetUp.class);
			               //把活动的用户ID和发起该活动的用户Id传入到活动详情中用于判断是不是他自己发起的活动
			                 Bundle bundle=new Bundle();
			                 bundle.putLong("UserIDActivity",UserIDActivity);
			                 bundle.putLong("ActivityID",ActivityID);
			                 Log.i("tag",String.valueOf(UserIDActivity)+"     hichujian_dating01");
			                 Log.i("tag",String.valueOf(ActivityID)+"     hichujian_dating01");
			                 intent.putExtras(bundle);
			                 ActivityDetails.this.startActivity(intent);
						}
					});
				}else{	//活动不是自己的
					Log.i("tag","活动不是自己的，获取的内容");
					isReported =  activity.isReported();		//用户是否举报该活动
					boolean isSignUped =  activity.isSignUped();	//用户是否报名该活动
					Log.i ("tag" , String.valueOf(activity.toString()));
					Log.i("tag", "昵称"+Nickname+"活动内容"+ActivityContent+"CommentNumber"+"评论数");


					Drawable drawable;
					//把数据扔进去
					nickname.setText(Nickname);
					sex.setText(Sex);
					//设置名字左边的
					if(Sex.equals("男")){
						drawable= getResources().getDrawable(R.drawable.activity_man);  
					}else{
						drawable= getResources().getDrawable(R.drawable.activity_woman);  
					}
					/// 这一步必须要做,否则不会显示.  
					drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());  
					sex.setCompoundDrawables(drawable,null,null,null); 
					activityPlace.setText(ActivityPlace);
					//head_to_person_details
					distance.setText(Distance+"");
					activityTime.setText(ActivityTime);
					invitedObjectSex.setText(InvitedObjectSex);
					activityExplain.setText(ActivityExplain);
					if(isSignUped){//已经报名
						setup.setText("已报名");
						setup.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_setup_gray_shape));
					}else{//未报名
						setup.setText("我要报名");
						setup.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_setup_green_shape));
					}
					comment.setText("评论");
					signUpNumber.setText(SignUpNumber+"");
					commentNumber.setText(CommentNumber+"");
					activityContent.setText(ActivityContent);
					if(isReported){
						report.setText("已举报");
					}else{
						report.setText("举报");
					}
				}
				
			} //end of handleMessage
		}; //end of Handler 

		handlerBitmap = new Handler() {
            @Override
            public void handleMessage(Message msg) {
              Bitmap b = (Bitmap)msg.obj;
             //   Log. i ("tag" , String.valueOf(msg.obj.toString()));
              head_to_person_details.setImageBitmap(b);
            }
        };
		
		final Handler handler1 = new Handler() { 	//接受活动，用来更新主线程
			@Override 
			public void handleMessage(Message msg1) { 
				Log.i("tag","获得Comment handler1成功");
				//获取评论列表
				listComment = (List<CommentDO>)msg1.obj;
				IistItem=getDate();
				Log.i("tag", String.valueOf(listComment));
				Log.i("tag", String.valueOf(IistItem));
				listViewComment = (ListView)findViewById(R.id.listViewComment);
				//创建适配器
				myAdaper=new MyAdapter(ActivityDetails.this);
				Log.i("tag","创建MyAdapter");
				listViewComment.setAdapter(myAdaper);
			}
		};
		
		final Handler handler2 = new Handler() { 	//接受活动，用来更新主线程
			@Override 
			public void handleMessage(Message msg2) { 
				if(msg2.what == States.COMMENT_QUERY_USER_FAILED){
					String content = (String) msg2.obj;
					HashMap<String, Object> map = new HashMap<String, Object>();
					map.put("ReviewerHeadPortrait", "");
					map.put("ReviewerNickname","我");//TODO
					map.put("Content",content);
					map.put("CreateTime",System.currentTimeMillis());
					map.put("Floor",listComment.size());
					IistItem.add(map);
					myAdaper.notifyDataSetChanged();
					Toast. makeText(ActivityDetails.this, "评论发布成功" , Toast.LENGTH_SHORT).show();
				}
				if(msg2.what == States.COMMENT_QUERY_USER_FAILED){
					 Toast. makeText(ActivityDetails.this, "评论发布失败" , Toast.LENGTH_SHORT).show();
				}
				if(msg2.what == States.COMMENT_OTHER_ERROR){
					 Toast. makeText(ActivityDetails.this, "评论错误 " , Toast.LENGTH_SHORT).show();
				}
			}
		};

		//点击头像，查看个人详情
		head_to_person_details.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setClass( ActivityDetails. this,  PersonDetails.class);
				Bundle bundle = new Bundle();
				bundle.putLong( "UserIDActivity", UserIDActivity);
				bundle.putCharSequence("ActivityContent",ActivityContent);
				bundle.putCharSequence("ActivityTime",ActivityTime);
				bundle.putCharSequence("ActivityPlace",ActivityPlace);
				bundle.putInt("CommentNumber",CommentNumber);
				bundle.putInt("SignUpNumber",SignUpNumber);
				bundle.putDouble("Distance",Distance);
				intent.putExtras(bundle);
				ActivityDetails.this.startActivity(intent);
			}
		});
		//点击举报按钮触发相应
		report.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Log.i("tag","report");
				if(report.getText().toString().equals("举报")){//未举报，字体显示举报两个字
					Log.i("tag","report if 未举报");
					Intent intent = new Intent();
					intent.setClass(ActivityDetails.this, ReportDialog.class);
					Bundle bundle = new Bundle();                                           
					bundle.putCharSequence( "ActivityID", String.valueOf(ActivityID));                                   
					intent.putExtras(bundle);
					ActivityDetails.this.startActivityForResult(intent, CODE_REPORT);
				}else if(report.getText().toString().equals("已举报")){//未举报，字体显示举报两个字
					Log.i("tag","report if 已举报");
					Toast. makeText(ActivityDetails.this, "已经举报过了哦~" , Toast.LENGTH_SHORT).show();
				}
			}
		});
		//点击评论按钮触发相应
		comment.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				 ActivityCommentInput.setVisibility(View.VISIBLE);
				 ActivityCommentInputEdittext.setFocusable(true);
				 InputMethodManager inputMethodManager=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			     inputMethodManager.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
			}
		});
		//提交按钮
		 ActivityCommentCommit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				final String content = ActivityCommentInputEdittext.getText().toString();
				Log.i("tag", content);
				//发表一跳评论
				CommentAPI comment1 = CommentAPI.getInstance();
				comment1.createComment(UserID, ActivityID, content, new OnResponseListener() {
					@Override
					public void onResponse(String data) {
						myAdaper.notifyDataSetChanged();        
						Log.i("tag", "请求成功");
						Log.i("tag", data);
						ResponseDO result = JSON.parseObject(data,ResponseDO.class);
						if(result.getResultCode()==States.COMMENT_SUCCESSFUL){//评论成功
							//发送消息
							Message msg2 = new Message();
							msg2. what=States.AUTHCODE_CHECK_EXPIRED ;
							msg2.obj = content;
							handler2.sendMessage(msg2); 
							Log.i("tag", "评论成功");
						}
						if(result.getResultCode()==States.COMMENT_QUERY_USER_FAILED){//评论失败
							//发送消息
							Message msg2 = new Message();
							msg2. what=States.COMMENT_QUERY_USER_FAILED ;
							handler2.sendMessage(msg2); 
							Log.i("tag", "评论失败");
						}
						if(result.getResultCode()==States.COMMENT_OTHER_ERROR){//评论错误 
							//发送消息
							Message msg2 = new Message();
							msg2. what=States.COMMENT_OTHER_ERROR ;
							handler2.sendMessage(msg2); 
							Log.i("tag", "评论错误 ");
						}
					}
					
					@Override
					public void onError(String info) {
						Log.i("tag", "请求失败");
						
					}
				});//发表一条评论结束
				InputMethodManager imm =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);  
				imm.hideSoftInputFromWindow(ActivityCommentInputEdittext.getWindowToken(), 0); 
				ActivityCommentInput.setVisibility(View.GONE);
			}
		});
		  //
		



		//返回
		back_to_login89=(ImageButton)findViewById(R.id.back_to_login89);
		back_to_login89.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
		//报名按钮
		setup.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				
				if(UserID!=UserIDActivity){//不是用户自己的活动，分为我要报名和已报名
					
					if(setup.getText()=="我要报名"){
						Intent intent = new Intent();
						intent.setClass(ActivityDetails.this, SetupDialog.class);
						Bundle bundle = new Bundle();                                           
						bundle.putCharSequence( "ActivityID", String.valueOf(ActivityID));                                   
						intent.putExtras(bundle);
						ActivityDetails.this.startActivityForResult(intent, CODE_WANT_SETUP);
						
					}//我要报名结束
					else if(setup.getText()=="已报名"){
						Intent intent = new Intent();
						intent.setClass(ActivityDetails.this, AlreadySetupDialog.class);
						Bundle bundle = new Bundle();                                           
						bundle.putCharSequence( "ActivityID", String.valueOf(ActivityID));                                   
						intent.putExtras(bundle);
						ActivityDetails.this.startActivityForResult(intent, CODE_CANEL_SETUP);
            	
					}//已报名结束
					           
			}else{//是用户自己的活动，分为解散活动和已解散
				if(setup.getText()=="解散活动"){//解散活动
					Intent intent = new Intent();
					intent.setClass(ActivityDetails.this, DissolveDialog.class);
					Bundle bundle = new Bundle();                                           
					bundle.putCharSequence( "ActivityID", String.valueOf(ActivityID));                                   
					intent.putExtras(bundle);
					ActivityDetails.this.startActivityForResult(intent, CODE_DISSOLVE_SETUP);
					
				}//我要报名结束
				
			}
			}//onClick结束
		});//报名按钮结束
				
		
		//获取这个ID的活动详情
		if(UserIDActivity==UserID){	//活动是自己的
			Log.i("tag","活动是自己的");

			ActivityAPI activityAPI=ActivityAPI.getInstance();
			activityAPI.getActivityInfo(UserID, ActivityID, ActivityAPI.SELF, new OnResponseListener() {

				@Override
				public void onResponse(String data) {	//请求成功
					Log.i("tag", "activity info request succuss:" + data);
					Log.i("tag",String.valueOf(UserIDActivity)+"     用户ID8");
					// TODO Auto-generated method stub
					final ResponseDO result=JSON.parseObject(data,ResponseDO.class);
					Log.i("tag",String.valueOf(result.getResultCode()));
					if(result.getResultCode()==States.GET_ACTI_INFO_SUCCESSFUL){//获取成功
						Log.i("tag", "获取成功");
						final ActivityDO activity=(ActivityDO) JSON.parseObject(result.getData().toString(), ActivityDO.class);
						Log.i("tag", activity.toString());
						Log.i("tag", activity.getActivityContent());
						Log.i("tag", activity.getActivityContent());
						Log.i("tag", activity.getActivityContent());

						Message msg = new Message();  
						msg.obj = activity;//result.getData().toString();  
						handler.sendMessage(msg); 
					}
					if(result.getResultCode()==States.GET_ACTI_NOT_EXISTS){	//活动信息不存在
						Log.i("tag", "活动信息不存在");
					}
					if(result.getResultCode()==States.GET_ACTI_INFO_OTHER_ERROR){//获取活动信息其他错误
						Log.i("tag", "获取活动信息其他错误");
					}
				}

				@Override
				public void onError(String info) {//请求失败
					// TODO Auto-generated method stub

					Log.i("tag", "activity info 请求失败" + info);
					Log.i("tag",String.valueOf(UserIDActivity)+"     用户ID9");
				}
			});//获取活动详情回调结束



		}else{	//活动不是自己的
			Log.i("tag",String.valueOf(UserIDActivity)+"     用户ID3");
			Log.i("tag","活动不是自己的");

			ActivityAPI activityAPI=ActivityAPI.getInstance();
			activityAPI.getActivityInfo(UserID, ActivityID, ActivityAPI.OTHERS, new OnResponseListener() {

				@Override
				public void onResponse(String data) {	//请求成功
					// TODO Auto-generated method stub
					final ResponseDO result=JSON.parseObject(data,ResponseDO.class);
					Log.i("tag",String.valueOf(result.getResultCode()));
					if(result.getResultCode()==States.GET_ACTI_INFO_SUCCESSFUL){//获取成功
						Log.i("tag", "获取成功");
						final ActivityDO activity=(ActivityDO) JSON.parseObject(result.getData().toString(), ActivityDO.class);
						Log.i("tag", activity.toString());

						Message msg = new Message();  
						msg.obj = activity;//result.getData().toString();  
						handler.sendMessage(msg);
						
					}
					if(result.getResultCode()==States.GET_ACTI_NOT_EXISTS){	//活动信息不存在
						Log.i("tag", "活动信息不存在");
					}
					if(result.getResultCode()==States.GET_ACTI_INFO_OTHER_ERROR){//获取活动信息其他错误
						Log.i("tag", "获取活动信息其他错误");
					}
				}

				@Override
				public void onError(String info) {//请求失败
					// TODO Auto-generated method stub
					Log.i("tag", "请求失败");
				}
			});//获取活动详情回调结束
		}

		//获取评论列表
		CommentAPI commentAPI = CommentAPI.getInstance();
		commentAPI.getCommnentList(UserID, ActivityID, 0, 10, 0, new OnResponseListener() {
			
			@Override
			public void onResponse(String data) {
				// TODO Auto-generated method stub
				Log.i("tag", "请求成功");
				ResponseDO result = JSON.parseObject(data,ResponseDO.class);
				Log.i("tag", data+"getComment");
				if(result.getResultCode()==States.LIST_COMMENTS_SUCCESSFUL){//获取评论列表成功
					Log.i("tag", data.toString()+"活动列表获取成功");
					List<CommentDO> listComment = (List<CommentDO>)JSON.parseArray(result.getData().toString(),CommentDO.class);
					//发送消息
					Message msg1 = new Message();
					msg1.obj = listComment; 
					handler1.sendMessage(msg1);
				}
				if(result.getResultCode()==States.LIST_COMMENTS_IS_NULL){//获取活动评论列表为空
					Log.i("tag", "获取活动评论列表为空");
				}
				if(result.getResultCode()==States.LIST_COMMENTS_OTHER_ERROR){//获取活动评论列表错误
					Log.i("tag", "获取活动评论列表错误");
				}
			}
			
			@Override
			public void onError(String info) {
				// TODO Auto-generated method stub
				Log.i("tag", "请求失败getComment");
				Log.i("tag", info+"getComment");
				
			}
		});//获取评论列表
		
		
		//InputMethodManager  manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
	}//end of Oncreate
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//取消报名回调
		if(requestCode==CODE_CANEL_SETUP && resultCode==CODE_CANEL_SETUP){
			((Button)findViewById(R.id.setup)).setText("我要报名");
			((Button)findViewById(R.id.setup)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_setup_green_shape));
		}
		//我要报名回调
		if(requestCode==CODE_WANT_SETUP && resultCode==CODE_WANT_SETUP){
			((Button)findViewById(R.id.setup)).setText("已报名");
			((Button)findViewById(R.id.setup)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_setup_gray_shape));
		}
		//解散活动回调
		if(requestCode==CODE_DISSOLVE_SETUP && resultCode==CODE_DISSOLVE_SETUP){
			((Button)findViewById(R.id.setup)).setText("已解散");
			((Button)findViewById(R.id.setup)).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_setup_gray_shape));
		}
		//举报活动回调
		if(requestCode==CODE_REPORT && resultCode==CODE_REPORT){
			((TextView)findViewById(R.id.report)).setText("已举报");
		}
	}
	
	

	 
	//把数据传进去
		private ArrayList<HashMap<String, Object>> getDate() {
			ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
		//	Log.i("tag", String.valueOf(listComment.size())+"条记录");

			//把数据丢进去
			for (int i = 0; i < listComment.size(); i++) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("ReviewerHeadPortrait", listComment.get(i).getReviewerHeadPortrait());
				map.put("ReviewerNickname",listComment.get(i).getReviewerNickname());
				map.put("Content", listComment.get(i).getContent());
				map.put("CreateTime",listComment.get(i).getCreateTime());
				map.put("Floor",listComment.get(i).getFloor());
				//Log.i("tag",listComment.get(i).getReviewerNickname()+"getReviewerNickname");
				listItem.add(map);
			}
			return listItem;
		}
		
	//创建MyAdapter内部类
		private class MyAdapter extends BaseAdapter{
			private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
			ViewHolder holder;
			private MyAdapter(Context context){
				this.mInflater=LayoutInflater.from(context);
				Log.i("tag", "MyAdapter构造成功");
			}

			public void refresh(){
				notifyDataSetChanged();
			}
			
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				Log.i("tag", "getCount");
				Log.i("tag", String.valueOf(getDate().size()));

				return getDate().size();
			}
			@Override
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				Log.i("tag", "getItem");
				return null;

			}

			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub.
				Log.i("tag", "getItemId");
				return 0;
			}

			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				Log.i("tag", "进入getView");
				// 观察convertView随ListView滚动情况
				Log.v("tag", "getView " + position + " " + convertView);
				if (convertView == null) {
					Log.i("tag", "得到各个控件的对象");
					convertView = mInflater.inflate(R.layout.activity_details_item_comment, null);
					holder = new ViewHolder();
					/* 得到各个控件的对象 */
					holder.CommentHead = (ImageView) convertView.findViewById(R.id.CommentHead);
					holder.CommentNickName = (TextView) convertView.findViewById(R.id.CommentNickName);
					holder.CommentFoor = (TextView) convertView.findViewById(R.id.CommentFoor);
					holder.CommentContent = (TextView) convertView.findViewById(R.id.CommentContent);
					holder.CommentTime = (TextView) convertView.findViewById(R.id.CommentTime);
					convertView.setTag(holder);
				}// 绑定ViewHolder对象 当convertView不为空时 不需要再fingViewById()来找每个控件了 只需要convertView.hol
				else {
					holder = (ViewHolder) convertView.getTag();
					Log.i("tag", "取出ViewHolder对象");
					// 取出ViewHolder对象
				}
				/* 设置TextView显示的内容，即我们存放在动态数组中的数据 */
				
				//holder.CommentHead.setText(IistItem.get(position).get("ReviewerHeadPortrait").toString());
				holder.CommentNickName.setText(IistItem.get(position).get("ReviewerNickname").toString());
				holder.CommentFoor.setText(IistItem.get(position).get("Floor").toString()+"楼");
				holder.CommentContent.setText(IistItem.get(position).get("Content").toString());
				holder.CommentTime.setText(IistItem.get(position).get("CreateTime").toString());
				
				AsyncRunner.loadImageAndShow(IistItem.get(position).get("ReviewerHeadPortrait").toString(), holder.CommentHead);
				Log.i("tag", "设置TextView显示的内容，即我们存放在动态数组中的数据");
				return convertView;
			}


		}
		/* 存放控件 */
		public class ViewHolder {
			public ImageView CommentHead;
			public TextView CommentNickName;
			public TextView CommentFoor;
			public TextView CommentContent;
			public TextView CommentTime;
		}
		
		@Override
	    public boolean onTouchEvent(android.view.MotionEvent event) {
	      InputMethodManager imm = (InputMethodManager)     getSystemService(INPUT_METHOD_SERVICE);
	           return                       
	              imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
	 }
		
		
		
		

		/**
		 * 下载图片并且显示到imageview上
		 * @param imgUrl 图片的url
		 * @param imgv	显示图片的ImageView对象
		 */
		public static void loadImageAndShow(final String imgUrl) {
			Runnable task = new Runnable() {
				public void run() {
					Log.i("tag", "头像加载开始：");
					InputStream is = null;
					HttpURLConnection conn = null;
					try {
						 conn = (HttpURLConnection) (new URL(imgUrl)).openConnection();
						 conn.setDoInput(true);
						 conn.connect();
						 is = conn.getInputStream();
						 final Bitmap b = BitmapFactory.decodeStream(is);
						 
						//发送消息
						 Message msg = new Message();
						 msg.obj=b;
						 handlerBitmap.sendMessage(msg); 

						// imgv.setImageBitmap(b);
						 Log.i("tag", "图片加载完毕!");
					} catch(Exception e) {
						e.printStackTrace();
					} finally {
						try {
							if(is != null)
								is.close();
							if(conn != null)
								conn.disconnect();
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
			};
			executorService.execute(task);
		}
   

}//end