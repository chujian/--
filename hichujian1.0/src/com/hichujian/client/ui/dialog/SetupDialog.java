package com.hichujian.client.ui.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.ActivityAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.ui.ActivityDetails;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class SetupDialog extends Activity{
	private Long  UserID;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.dialog_for_setup);
        
        //获取UserId
        SharedPreferences preferences = getSharedPreferences("User" , Context.MODE_PRIVATE);
        String UserId = preferences.getString( "UserId", "");
        UserID = Long. valueOf(UserId);
        Log. i("tag",Long. valueOf(UserId)+"Long型");
        Log. i("tag", UserId+"");
                  
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String ActivityIDString = bundle.getString( "ActivityID");
        final Long ActivityID =Long.valueOf(ActivityIDString);
        Log. i("tag",String.valueOf(ActivityID));
        
        Toast.makeText(SetupDialog.this, "打开报名对话框", Toast.LENGTH_SHORT).show();
        //主函数接受对象
        final Handler handler = new Handler() { 
        	@Override 
        	public void handleMessage(Message msg) { 
        		if(msg.what==States.SINGUP_SUCCESSFUL){
        			setResult(0x718);
        			finish();
        			Toast.makeText(SetupDialog.this, "报名成功", Toast.LENGTH_SHORT).show();
        		}else if(msg.what==States.SINGUP_OTHER_ERROR) {
        			finish();
        			Toast.makeText(SetupDialog.this, "报名失败", Toast.LENGTH_SHORT).show();
        		}
        	} 
        };


      //取消按钮
        Button cannel_report_setup = (Button) findViewById(R.id.btn_cancel_setup);  
        cannel_report_setup.setOnClickListener(new Button.OnClickListener(){  
 
               public void onClick(View v) {  
                   // TODO Auto-generated method stub  
                   //dismiss();  
            	   SetupDialog.this.finish();
               }  
           });
        
        final EditText edit_setup = (EditText)findViewById(R.id.edit_setup);
      //确定按钮
        Button confirm_report_setup = (Button) findViewById(R.id.confirm_report_setup);  
        confirm_report_setup.setOnClickListener(new Button.OnClickListener(){  
 
               public void onClick(View v) {  
                   // TODO Auto-generated method stub  
            	   Log.i("tag", "报名请求启动");
            	   //报名
            	   String edit_setupString = edit_setup.getText().toString();//获取用户报名的一句话
            	   if(edit_setupString.equals("")){//对话框内的内容为空
            		   Toast.makeText(SetupDialog.this, "输入的内容不能为空哈~", Toast.LENGTH_SHORT).show();
            	   }else{
            		   ActivityAPI activityAPI = ActivityAPI.getInstance();
            		   activityAPI.signUpActivity(UserID, ActivityID, edit_setupString, new OnResponseListener() {

            			   @Override
            			   public void onResponse(String data) {
            				   Log.i("tag", "请求成功");
            				   ResponseDO result = JSON.parseObject(data,ResponseDO.class);
            				   Log.i("tag",String.valueOf(result.getResultCode()));
            				   if(result.getResultCode()==States.SINGUP_SUCCESSFUL){//报名成功
            					   Log.i("tag", "报名成功");
            					   ActivityDetails.SetUpFlag=9;
            					   Message msg = new Message();
            					   msg. what=States.SINGUP_SUCCESSFUL ;
            					   handler.sendMessage(msg); 

            				   }else if(result.getResultCode()==States.SINGUP_OTHER_ERROR){//报名错误 
            					   Log.i("tag", "报名错误");
            				   }else if(result.getResultCode()==States.SGIN_CHECK_ERROR){//签名错误 
            					   Log.i("tag", "签名错误");
            				   }
            			   }

            			   @Override
            			   public void onError(String info) {
            				   // TODO Auto-generated method stub
            				   Log.i("tag", "请求失败");
            			   }
            		   });//回调结束
            	   }     
               }  
           });  
}
}