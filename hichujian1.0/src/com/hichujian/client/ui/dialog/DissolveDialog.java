package com.hichujian.client.ui.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.ActivityAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class DissolveDialog extends Activity{
	private Long  UserID ;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.dialog_for_dissolve);
        
        //获取UserId
        SharedPreferences preferences = getSharedPreferences("User" , Context.MODE_PRIVATE);
        String UserId = preferences.getString( "UserId", "");
        UserID = Long. valueOf(UserId);
        Log. i("tag",Long. valueOf(UserId)+"Long型");
        Log. i("tag", UserId+"");

                  
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String ActivityIDString = bundle.getString( "ActivityID");
        final Long ActivityID =Long.valueOf(ActivityIDString);
        Log. i("tag",String.valueOf(ActivityID));
        
        Toast.makeText(DissolveDialog.this, "打开报名对话框", Toast.LENGTH_SHORT).show();
        //主函数接受对象
       final Handler handler = new Handler() { 
        	@Override 
        	public void handleMessage(Message msg) { 
        		if(msg.what==States.DLETE_ACTI_SUCCESSFUL){
        			setResult(0x719);
        			DissolveDialog.this.finish();
        			Toast.makeText(DissolveDialog.this, "活动删除成功2", Toast.LENGTH_SHORT).show();
        		}
        	} 
        };


      //取消按钮
        Button btn_cancel_dissolve_activity = (Button) findViewById(R.id.btn_cancel_dissolve_activity);  
        btn_cancel_dissolve_activity.setOnClickListener(new Button.OnClickListener(){  
 
               public void onClick(View v) {  
                   // TODO Auto-generated method stub  
                   //dismiss();  
            	   DissolveDialog.this.finish();
               }  
           });
        
      //确定按钮
       Button confirm_report_dissolve_activity = (Button) findViewById(R.id.confirm_report_dissolve_activity);  
        confirm_report_dissolve_activity.setOnClickListener(new Button.OnClickListener(){  
 
               public void onClick(View v) {  
            		 //解散活动
       				ActivityAPI activity = ActivityAPI.getInstance();
       				activity.deleteActivity(UserID, ActivityID, new OnResponseListener() {
       					@Override
       					public void onResponse(String data) {
       						// TODO Auto-generated method stub
       						ResponseDO result = JSON.parseObject(data,ResponseDO.class);
            				   Log.i("tag",String.valueOf(result.getResultCode()));
            				   if(result.getResultCode()==States.DLETE_ACTI_SUCCESSFUL){//取消报名成功
            					   Log.i("tag", "活动删除成功1");
            					   
            					   Message msg = new Message();
            					   msg. what=States.DLETE_ACTI_SUCCESSFUL ;
            					   handler.sendMessage(msg); 

            				   }else if(result.getResultCode()==States.DLETE_ACTI_ERROR){//取消报名错误 
            					   Log.i("tag", "活动删除错误");
            				   }else if(result.getResultCode()==States.SGIN_CHECK_ERROR){//签名错误 
            					   Log.i("tag", "签名错误");
            				   }
       					}
       					
       					@Override
       					public void onError(String info) {
       						// TODO Auto-generated method stub
       						  Log.i("tag", "请求失败");
       						  Log.i("tag", info);
       						  
       					}
       				});//取消报名回调结束
            	   }
            	       
                
           });  
}
}