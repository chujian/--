package com.hichujian.client.ui;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.ActivityAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.model.SignUpDO;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class ActivityAlreadySetUp extends Activity{
	private Long UserIDActivity,ActivityID;
	private ImageButton back_to_login90;
	private Long UserID;
	private List<SignUpDO> listSignUpList;
	private ArrayList<HashMap<String, Object>> IistItem;
	private MyAdapter myAdaper;
	private ListView listViewSignUp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		setContentView(R.layout.activityclreadysetup);
		
		SharedPreferences preferences = getSharedPreferences("User" , Context. MODE_PRIVATE);
        String UserId = preferences.getString( "UserId" , "" );
        UserID = Long. valueOf(UserId);
        Log. i( "tag",Long.valueOf(UserId)+ "Long型");
        Log. i( "tag", UserId+"hichujian_activity_SetUp 用户ID");
        
		//获取活动详情ID和发起活动用户ID
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		UserIDActivity = bundle.getLong("UserIDActivity");
		ActivityID = bundle.getLong("ActivityID");
		Log.i("tag",String.valueOf(UserIDActivity)+"     hichujian_activity_SetUp");
		Log.i("tag",String.valueOf(ActivityID)+"      hichujian_activity_SetUp");
		
		//返回
		back_to_login90=(ImageButton)findViewById(R.id.back_to_login90);
		back_to_login90.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
		
		final Handler handler = new Handler() { 	//接受活动，用来更新主线程
			@Override 
			public void handleMessage(Message msg) { 
				Log.i("tag","获得SignUpDO handler成功");
				//获取用户报名列表
				listSignUpList = (List<SignUpDO>)msg.obj;
				IistItem=getDate();
				Log.i("tag", String.valueOf(listSignUpList)+"1");
				Log.i("tag", String.valueOf(IistItem)+"2");
				listViewSignUp = (ListView)findViewById(R.id.listViewSignUp);
				//创建适配器
				myAdaper=new MyAdapter(ActivityAlreadySetUp.this);
				Log.i("tag","创建MyAdapter");
				listViewSignUp.setAdapter(myAdaper);
			}
		};
		
		ActivityAPI activityAPI = ActivityAPI.getInstance();
		activityAPI.getActivitySignUperList(UserID, ActivityID, 0, 10, null, new OnResponseListener() {
			
			@Override
			public void onResponse(String data) {
				// TODO Auto-generated method stub
				Log.i("tag","请求活动报名者列表成功");
				Log.i("tag",data);
				ResponseDO result = JSON.parseObject(data,ResponseDO.class);
				if(result.getResultCode()==States.GET_ACTI_SIGN_UPER_SUCCESSFUL){//获取活动评论列表成功
					List<SignUpDO> listSignUp = null;
					if(result.getData() != null) {
						listSignUp=(List<SignUpDO>) JSON.parseArray(result.getData().toString(), SignUpDO.class);
					}
					
					Log.i("tag","获取活动评论列表成功");
					Log.i("tag",String.valueOf(listSignUp));
							 
					//发送消息
					 Message msg = new Message();
					 msg.obj=listSignUp;
					 handler.sendMessage(msg); 
				}
				if(result.getResultCode()==States.GET_ACTI_SIGN_UPER_ERROR){//获取活动评论列表错误
					Log.i("tag","获取活动评论列表错误");
				}
			}
			
			@Override
			public void onError(String info) {
				// TODO Auto-generated method stub
				Log.i("tag","请求失败");
				Log.i("tag",info);
				
			}
		});//获取活动报名者信息
		
		
		
	}//end of onCreate
	
	//把数据传进去
		private ArrayList<HashMap<String, Object>> getDate() {
			ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
			Log.i("tag", String.valueOf(IistItem)+"listItem");

			//把数据丢进去
			for (int i = 0; i < listSignUpList.size(); i++) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				//map.put("activitySignUpHead", listSignUpList.get(i).getSignUper().getHeadPortrait());
				map.put("sex",listSignUpList.get(i).getSignUper().getSex());
				map.put("nickname", listSignUpList.get(i).getSignUper().getNickname());
				map.put("receiveStatus",listSignUpList.get(i).getReceiveStatus());
				map.put("signUpTime", listSignUpList.get(i).getSignUpTime());
				map.put("signUpInfo",listSignUpList.get(i).getSignUpInfo());
				listItem.add(map);
			}
			Log.i("tag", String.valueOf(IistItem)+"listItem");
			
			return listItem;

		}

		//创建MyAdapter内部类
		private class MyAdapter extends BaseAdapter{
			private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 
			ViewHolder holder;
			private MyAdapter(Context context){
				this.mInflater=LayoutInflater.from(context);
				Log.i("tag", "MyAdapter构造成功");
			}


			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				Log.i("tag", "getCount");
				Log.i("tag", String.valueOf(getDate().size()));

				return getDate().size();
			}
			@Override
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				Log.i("tag", "getItem");
				return null;

			}

			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub.
				Log.i("tag", "getItemId");
				return 0;
			}

			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				Log.i("tag", "进入getView");
				// 观察convertView随ListView滚动情况
				Log.v("tag", "getView " + position + " " + convertView);
				if (convertView == null) {
					Log.i("tag", "得到各个控件的对象");
					convertView = mInflater.inflate(R.layout.activity_details_item_signup, null);
					holder = new ViewHolder();
					// 得到各个控件的对象 
					holder.SignUpHead = (ImageView) convertView.findViewById(R.id.SignUpHead);
					holder.SignSex = (TextView) convertView.findViewById(R.id.SignSex);
					holder.SignUpNickName = (TextView) convertView.findViewById(R.id.SignUpNickName);
					holder.SignUpIsAccept = (TextView) convertView.findViewById(R.id.SignUpIsAccept);
					holder.SignUpTime = (TextView) convertView.findViewById(R.id.SignUpTime);
					holder.SignUpContent = (TextView) convertView.findViewById(R.id.SignUpContent);
					convertView.setTag(holder);
				}// 绑定ViewHolder对象 当convertView不为空时 不需要再fingViewById()来找每个控件了 只需要convertView.hol
				else {
					holder = (ViewHolder) convertView.getTag();
					Log.i("tag", "取出ViewHolder对象");
					// 取出ViewHolder对象
				}
				 //设置TextView显示的内容，即我们存放在动态数组中的数据 
				//holder.SignUpHead.setImageURI(uri)//setText(IistItem.get(position).get("activity_theme").toString());
				holder.SignSex.setText(IistItem.get(position).get("sex").toString());
				holder.SignUpNickName.setText(IistItem.get(position).get("nickname").toString());
				holder.SignUpIsAccept.setText(IistItem.get(position).get("receiveStatus").toString());
				holder.SignUpTime.setText(IistItem.get(position).get("signUpTime").toString());
				holder.SignUpContent.setText(IistItem.get(position).get("signUpInfo").toString());
				Log.i("tag", "设置TextView显示的内容，即我们存放在动态数组中的数据");
				return convertView;
			}


		}
		 //存放控件 
		public class ViewHolder {
			public ImageView SignUpHead;
			public TextView SignSex;
			public TextView SignUpNickName;
			public TextView SignUpIsAccept;
			public TextView SignUpTime;
			public TextView SignUpContent;
		}
}//end  
