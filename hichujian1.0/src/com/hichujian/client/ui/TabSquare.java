package com.hichujian.client.ui;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.ActivityAPI;
import com.hichujian.client.model.ActivityDO;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.utils.AsyncRunner;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.ImageLoadUtils;
public class TabSquare extends Fragment{
	private ImageView add_activity;
	private ImageButton shaixuan;
	private TextView time;
	private com.chujian.jishiqi.CustomDigitalClock timeClock1;
	private com.chujian.jishiqi.CustomDigitalClock timeClock2;
	private ListView listView_activity_dating;
	private ImageView activity_head;
	private ArrayList<HashMap<String, Object>> IistItem;
	private List<ActivityDO> list;
	private Long UserID;
	private int secletNumberInt=-1;//筛选
	private int CODE_SHAIXUAN = 0x11;//主界面筛选
	private boolean flag[]=new boolean[9];
	private Handler handler,handler2;
	private static final int MAX_THREAD_COUNT = 5;//最大线程数量
	private static Handler handlerBitmap;
	private static ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREAD_COUNT);
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.dating01, container, false);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.i("tag", "free memory:" + Runtime.getRuntime().totalMemory());
		//获取secletNumber从9宫格里面筛选的数
		SharedPreferences preferences1 = getActivity().getSharedPreferences("secletNumber" , Context. MODE_PRIVATE);
		String SecletNumber = preferences1.getString( "SecletNumber" , "" );
		Log. i( "tag",String.valueOf(SecletNumber)+ "String型");
		secletNumberInt = Integer.valueOf(SecletNumber);
		Log. i( "tag",Integer.valueOf(SecletNumber)+ "Integer型");
		Log. i( "tag", secletNumberInt+"hichujian_dating01_new 获取secletNumber从9宫格里面帅选的数");

		//获取UserId
		SharedPreferences preferences = getActivity().getSharedPreferences("User" , Context. MODE_PRIVATE);
		String UserId = preferences.getString( "UserId" , "" );
		UserID = Long. valueOf(UserId);
		Log. i( "tag",Long.valueOf(UserId)+ "Long型");
		Log. i( "tag", UserId+" hichujian_dating01_new 用户ID");

		handler = new Handler() {  
			@Override  
			public void handleMessage(final Message msg) { 
				list = (List<ActivityDO>)msg.obj;
				Log.i("tag", String.valueOf(msg.obj.toString()));
				if(list==null){
					IistItem=null;
				}else{
				IistItem=getDate();
				Log.i("tag", String.valueOf(list));
				Log.i("tag", String.valueOf(IistItem));
				listView_activity_dating = (ListView)getView().findViewById(R.id.listView_activity_dating);
				//头部添加一张图片
				TextView textView=new TextView(getActivity());//实例化一个View
				textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.eat_picture));
				textView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Log.i("tag", "不执行");
					}
				});
				listView_activity_dating.addHeaderView(textView);
				Log.i("tag","捕获listView_activity_dating");
				
				}//if结束
				
				//创建适配器
				MyAdapter myAdaper=new MyAdapter(getActivity());
				Log.i("tag","创建MyAdapter");
				listView_activity_dating.setAdapter(myAdaper);
				
			
				
				listView_activity_dating.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
						// TODO Auto-generated method stub
						Log.v("MyListViewBase", "你点击了ListView条目" + position);// 在LogCat中输出信息
						Toast.makeText(getActivity(),"你点击了Item", Toast.LENGTH_SHORT).show();
						position=position-1;
						Long UserIDActivity = Long.valueOf(list.get(position).getUserID());
						Long ActivityID = Long.valueOf(list.get(position).getActivityID());

						Intent intent=new Intent();
						intent.setClass( getActivity(),  ActivityDetails.class);
						//把活动的用户ID和发起该活动的用户Id传入到活动详情中用于判断是不是他自己发起的活动
						Bundle bundle=new Bundle();
						bundle.putInt("position",position);
						bundle.putLong("UserIDActivity",UserIDActivity);
						bundle.putLong("ActivityID",ActivityID);
						Log.i("tag",String.valueOf(UserIDActivity)+"     hichujian_dating01");
						Log.i("tag",String.valueOf(ActivityID)+"     hichujian_dating01");
						intent.putExtras(bundle);
						TabSquare.this.startActivity(intent);
					}
				});//listView_activity_dating
				
			}  
		}; 
		
		handler2 = new Handler() {  
			@Override  
			public void handleMessage(final Message msg) { 
				if(list==null){
					IistItem=null;
				}else{
				list = (List<ActivityDO>)msg.obj;
				IistItem=getDate();
				Log.i("tag", String.valueOf(list));
				Log.i("tag", String.valueOf(IistItem));
				listView_activity_dating = (ListView)getView().findViewById(R.id.listView_activity_dating);
				/*//头部添加一张图片
				TextView textView=new TextView(getActivity());//实例化一个View
				textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.eat_picture));
				textView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Log.i("tag", "不执行");
					}
				});
				listView_activity_dating.addHeaderView(textView);*/
				Log.i("tag","捕获listView_activity_dating");
			}//if结束
				//创建适配器
				MyAdapter myAdaper=new MyAdapter(getActivity());
				Log.i("tag","创建MyAdapter");
				listView_activity_dating.setAdapter(myAdaper);
				listView_activity_dating.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
						// TODO Auto-generated method stub
						Log.v("MyListViewBase", "你点击了ListView条目" + position);// 在LogCat中输出信息
						Toast.makeText(getActivity(),"你点击了Item", Toast.LENGTH_SHORT).show();
						position=position-1;
						Long UserIDActivity = Long.valueOf(list.get(position).getUserID());
						Long ActivityID = Long.valueOf(list.get(position).getActivityID());

						Intent intent=new Intent();
						intent.setClass( getActivity(),  ActivityDetails.class);
						//把活动的用户ID和发起该活动的用户Id传入到活动详情中用于判断是不是他自己发起的活动
						Bundle bundle=new Bundle();
						bundle.putInt("position",position);
						bundle.putLong("UserIDActivity",UserIDActivity);
						bundle.putLong("ActivityID",ActivityID);
						Log.i("tag",String.valueOf(UserIDActivity)+"     hichujian_dating01");
						Log.i("tag",String.valueOf(ActivityID)+"     hichujian_dating01");
						intent.putExtras(bundle);
						TabSquare.this.startActivity(intent);
					}
				});
			}  
		}; 

		String activityTheme[]={"全部","运动","旅行","阅读","聚餐","K歌","桌游","电影","网游"};
		List<String> activityTypes = null;
		if(secletNumberInt!=0){
			activityTypes=new ArrayList<String>();
			activityTypes.add(activityTheme[secletNumberInt]);
		}
		Log.i("tag", String.valueOf(activityTypes));

		ActivityAPI activityAPI=ActivityAPI.getInstance();	//Long.valueOf(result.getData().toString())
		activityAPI.getActivityAround(UserID, 38, 38, 10000, null, 0, 0, activityTypes, null, 0, 10, null, new OnResponseListener() {

			public void onResponse(String data) {
				// TODO Auto-generated method stub
				Log.i("tag","获取成功");
				Log.i("tag",data);
				final ResponseDO result=JSON.parseObject(data,ResponseDO.class);
				final List<ActivityDO> list2=(List<ActivityDO>) JSON.parseArray(result.getData().toString(), ActivityDO.class);
				Log.i("tag", list2.toString());

				Message msg = new Message();  
				msg.obj = list2;//result.getData().toString();  
				handler.sendMessage(msg); 
			}
			@Override
			public void onError(String info) {
				// TODO Auto-generated method stub
				Log.i("tag",info);
				Log.i("tag","获取失败");
			}
		});	//回调结束

		//增加活动
		add_activity=(ImageView)getView().findViewById(R.id.add_activity);
		add_activity.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setClass( getActivity(),  ActivityFabu.class);
				TabSquare.this.startActivity(intent);
			}
		});
		//筛选
		shaixuan=(ImageButton)getView().findViewById(R.id.shaixuan);
		shaixuan.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Log.i("tag", "free 2 memory:" + Runtime.getRuntime().freeMemory());
				Intent intent=new Intent();
				intent.setClass( getActivity(),ShaiXuan.class);
				TabSquare.this.startActivityForResult(intent, CODE_SHAIXUAN);
			}
		});

	}//end of onActivityCreated

	
	
    
	//把数据传进去
	private ArrayList<HashMap<String, Object>> getDate() {
		ArrayList<HashMap<String, Object>> listItem = new ArrayList<HashMap<String, Object>>();
		Log.i("tag", String.valueOf(list.size())+"条记录");

		//把数据丢进去
		for (int i = 0; i < list.size(); i++) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("activity_theme", list.get(i).getActivityContent());
			map.put("activity_nickname",list.get(i).getNickname());
			map.put("activity_place", list.get(i).getActivityPlace());
			map.put("activity_time",list.get(i).getActivityTime());
			map.put("activity_distance",new DecimalFormat("0.00").format(list.get(i).getDistance()));
			
			map.put("distance",list.get(i).getDistance());
			
			map.put("activity_setup",list.get(i).getSignUpNumber());
			map.put("activity_comment",list.get(i).getCommentNumber());
			map.put("activity_sex",list.get(i).getSex());
			map.put("activity_head",list.get(i).getHeadPortrait());
			listItem.add(map);
		}
		return listItem;

	}

	
	//创建MyAdapter内部类
	private class MyAdapter extends BaseAdapter{
		private LayoutInflater mInflater;// 得到一个LayoutInfalter对象用来导入布局 /*构造函数*/
		ViewHolder holder;
		private MyAdapter(Context context){
			this.mInflater=LayoutInflater.from(context);
			Log.i("tag", "MyAdapter构造成功");
		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			Log.i("tag", "getCount");
			Log.i("tag", String.valueOf(getDate().size()));

			return getDate().size();
		}
		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			Log.i("tag", "getItem");
			return null;

		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub.
			Log.i("tag", "getItemId");
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			Log.i("tag", "进入getView");
			// 观察convertView随ListView滚动情况
			Log.v("tag", "getView " + position + " " + convertView);
			if (convertView == null) {
				Log.i("tag", "得到各个控件的对象");
				convertView = mInflater.inflate(R.layout.dating01_item_man, null);
				holder = new ViewHolder();
				/* 得到各个控件的对象 */
				holder.activity_sex = (ImageView) convertView.findViewById(R.id.activity_sex);
				holder.activity_theme = (TextView) convertView.findViewById(R.id.activity_theme);
				holder.activity_nickname = (TextView) convertView.findViewById(R.id.activity_nickname);
				holder.activity_place = (TextView) convertView.findViewById(R.id.activity_place);
				holder.activity_time = (TextView) convertView.findViewById(R.id.activity_time);
				holder.activity_distance = (TextView) convertView.findViewById(R.id.activity_distance);
				holder.activity_setup = (TextView) convertView.findViewById(R.id.activity_setup);
				holder.activity_comment = (TextView) convertView.findViewById(R.id.activity_comment);
				holder.activity_head = (ImageView) convertView.findViewById(R.id.activity_head);
				convertView.setTag(holder);
			}// 绑定ViewHolder对象 当convertView不为空时 不需要再fingViewById()来找每个控件了 只需要convertView.hol
			else {
				holder = (ViewHolder) convertView.getTag();
				Log.i("tag", "取出ViewHolder对象");
				// 取出ViewHolder对象
			}
			/* 设置TextView显示的内容，即我们存放在动态数组中的数据 */
			if(list.get(position).getSex().equals("女")){
				holder.activity_sex.setBackgroundDrawable(getResources().getDrawable(R.drawable.activity_woman));
			}else{
				holder.activity_sex.setBackgroundDrawable(getResources().getDrawable(R.drawable.activity_man));
			}
			holder.activity_theme.setText(IistItem.get(position).get("activity_theme").toString());
			holder.activity_nickname.setText(IistItem.get(position).get("activity_nickname").toString());
			holder.activity_place.setText(IistItem.get(position).get("activity_place").toString());
			holder.activity_time.setText(IistItem.get(position).get("activity_time").toString());
			/*int distances = Integer.parseInt(IistItem.get(position).get("distance").toString());
			Log.i("tag",String.valueOf(distances);*/
			//if(distances >= 1){
			//	Log.i("tag","distances>=1");
				holder.activity_distance.setText(IistItem.get(position).get("activity_distance").toString()+"km");
			//}else{
		//		Log.i("tag","distances<1");
			///	holder.activity_distance.setText(Integer.parseInt(IistItem.get(position).get("activity_distance").toString())*1000+"m");
			//}
			holder.activity_place.setText(IistItem.get(position).get("activity_place").toString());
			holder.activity_setup.setText(IistItem.get(position).get("activity_setup").toString());
			holder.activity_comment.setText(IistItem.get(position).get("activity_comment").toString());
		
			ImageLoadUtils.loadImage(IistItem.get(position).get("activity_head").toString(), holder.activity_head, null);
			Log.i("tag", "设置TextView显示的内容，即我们存放在动态数组中的数据");
			return convertView;
		}


	}
	/* 存放控件 */
	public class ViewHolder {
		public ImageView activity_sex;
		public TextView activity_theme;
		public TextView activity_nickname;
		public TextView activity_place;
		public TextView activity_time;
		public TextView activity_distance;
		public TextView activity_comment;
		public TextView activity_setup;
		public ImageView activity_head;
	}
	
	
	//onCreate外面回调
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==CODE_SHAIXUAN && resultCode==CODE_SHAIXUAN ){
			Bundle bundle = data.getExtras();
			String sex = null;
			//获取筛选性别
			int value = bundle.getInt("value");
			if(value == 1){
				sex = "男";
			}else if(value == 2){
				sex = "女";
			}else if(value == 0){
				sex = null;
			}
			
			//获取选中了哪几个活动，1为选中，0为未选中
			flag[0] = bundle.getBoolean("flag1");
			flag[1] = bundle.getBoolean("flag2");
			flag[2] = bundle.getBoolean("flag3");
			flag[3] = bundle.getBoolean("flag4");
			flag[4] = bundle.getBoolean("flag5");
			flag[5] = bundle.getBoolean("flag6");
			flag[6] = bundle.getBoolean("flag7");
			flag[7] = bundle.getBoolean("flag8");
			flag[8] = bundle.getBoolean("flag9");
			
			
			String activityTheme[] = {"随便","运动","旅行","阅读","聚餐","K歌","桌游","电影","网游"};
			List<String> activityTyps = new ArrayList<String>();
			if(flag[0]==true){
				activityTyps=null;
			}else{
				for(int i=1;i<=8;i++){
					if(flag[i]==true)
						activityTyps.add(activityTheme[i]);
				}
			}
			Log.i("tag", String.valueOf(activityTyps));
			

			ActivityAPI activityAPI=ActivityAPI.getInstance();	//Long.valueOf(result.getData().toString())
			activityAPI.getActivityAround(UserID, 38, 38, 10000, sex, 0, 0, activityTyps, null, 0, 10, null, new OnResponseListener() {

				public void onResponse(String data) {
					// TODO Auto-generated method stub
					Log.i("tag","获取成功");
					Log.i("tag",data);
					final ResponseDO result=JSON.parseObject(data,ResponseDO.class);
					final List<ActivityDO> list3=(List<ActivityDO>) JSON.parseArray(result.getData().toString(), ActivityDO.class);
					Log.i("tag", list3.toString());
				    /*List<ActivityDO> list3=null;
					if(result.getData()==null){
						list3=null;
					}else{
						list3=(List<ActivityDO>) JSON.parseArray(result.getData().toString(), ActivityDO.class);
						if(list3.size()==0){
							list3=null;
						}
					}*/

					Message msg = new Message();  
					msg.obj = list3;//result.getData().toString(); 
					handler2.sendMessage(msg); 
				}
				@Override
				public void onError(String info) {
					// TODO Auto-generated method stub
					Log.i("tag",info);
					Log.i("tag","获取失败");
				}
			});	//回调结束
		}
	}

}
