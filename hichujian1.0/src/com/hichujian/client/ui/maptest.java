package com.hichujian.client.ui;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMapClickListener;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfigeration;
import com.baidu.mapapi.map.MyLocationConfigeration.LocationMode;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeOption;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeOption;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.example.hichujian.R;



public class maptest extends Activity implements OnGetGeoCoderResultListener{

	// 定位相关
	LocationClient mLocClient;
	public MyLocationListenner myListener = new MyLocationListenner();
	private LocationMode mCurrentMode;
	BitmapDescriptor mCurrentMarker;
	private boolean isFirstLoc = true;
	
	//地图主控件
	private MapView mMapView;
	private BaiduMap mBaiduMap;
	/**
	 * 搜索相关
	 */
	GeoCoder mSearch = null;
	 //用于显示地图状态的面板
	private TextView mStateBar;
	//点击点
	private LatLng currentPt;

	
	    @Override  
	    protected void onCreate(Bundle savedInstanceState) {  
	        super.onCreate(savedInstanceState);   
	        //在使用SDK各组件之前初始化context信息，传入ApplicationContext  
	        //注意该方法要再setContentView方法之前实现  
	        SDKInitializer.initialize(getApplicationContext());  
	        setContentView(R.layout.activity_mapcontrol);  
	        //获取地图控件引用  
	        mMapView = (MapView) findViewById(R.id.bmapView);  
	        mBaiduMap = mMapView.getMap();
			Log.i("tag", "进入到baidumapsdk.demo.MapControlDemo mMapView");
			//开始地图定位
			mCurrentMode = LocationMode.NORMAL;		
					mBaiduMap
					.setMyLocationConfigeration(new MyLocationConfigeration(
							mCurrentMode, true, null));			
			
			Log.i("tag", "进入到baidumapsdk.demo.MapControlDemo开启定位图层");
			// 开启定位图层
			mBaiduMap.setMyLocationEnabled(true);
			// 定位初始化
			mLocClient = new LocationClient(this);
			mLocClient.registerLocationListener(myListener);
			LocationClientOption option = new LocationClientOption();
			option.setOpenGps(true);// 打开gps
			option.setCoorType("bd09ll"); // 设置坐标类型
			option.setScanSpan(1000);
			mLocClient.setLocOption(option);
			mLocClient.start();
			
			// 初始化搜索模块，注册事件监听
			mSearch = GeoCoder.newInstance();
			mSearch.setOnGetGeoCodeResultListener(this);
			findViewById(R.id.geocode).setOnClickListener(
					new OnClickListener(){
						public void onClick(View v) {	
								EditText editCity = (EditText) findViewById(R.id.city);
								EditText editGeoCodeKey = (EditText) findViewById(R.id.geocodekey);
								// Geo搜索
								mSearch.geocode(new GeoCodeOption().city(
										editCity.getText().toString()).address(
										editGeoCodeKey.getText().toString()));						
						}					
					});
			mStateBar = (TextView) findViewById(R.id.state);
			initListener();
	    }
		public class MyLocationListenner implements BDLocationListener {

			public void onReceiveLocation(BDLocation location) {
				// map view 销毁后不在处理新接收的位置
				if (location == null || mMapView == null || !isFirstLoc)
					return;
				MyLocationData locData = new MyLocationData.Builder()
				.accuracy(location.getRadius())
				// 此处设置开发者获取到的方向信息，顺时针0-360
				.direction(100).latitude(location.getLatitude())
				.longitude(location.getLongitude()).build();
				//设置定位数据
				mBaiduMap.setMyLocationData(locData);
				// 修改为自定义marker
				mCurrentMarker = BitmapDescriptorFactory
						.fromResource(R.drawable.icon_geo);
				mBaiduMap
				.setMyLocationConfigeration(new MyLocationConfigeration(
						mCurrentMode, true, mCurrentMarker));
				LatLng ll = new LatLng(location.getLatitude(),
						location.getLongitude());
				MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
				mBaiduMap.animateMapStatus(u);
				String state = String.format("当前经度： %f 当前纬度：%f",
						locData.longitude, locData.latitude);
				mStateBar.setText(state);
				isFirstLoc = false;
			}
			public void onReceivePoi(BDLocation poiLocation) {

			}
		}
		//初始化点击监控器
		private void initListener() {
			mBaiduMap.setOnMapClickListener(new OnMapClickListener() {
				public void onMapClick(LatLng point) {
					//关闭定位图层
					mBaiduMap.setMyLocationEnabled(false);
					currentPt = point;
					LatLng ptCenter = new LatLng(((float) currentPt.latitude), ((float)currentPt.longitude));
					// 反Geo搜索
					mSearch.reverseGeoCode(new ReverseGeoCodeOption()
					.location(ptCenter));
					updateMapState();
				}

				public boolean onMapPoiClick(MapPoi poi) {
					return false;
				}
			});
		}
		//更新statebar
		private void updateMapState() {
			String state = "";
			if (mStateBar == null) {
				return;
			}
			if(currentPt != null){
				state = String.format("当前经度： %f 当前纬度：%f",
						currentPt.longitude, currentPt.latitude);
			}
			state += "\n";
			mStateBar.setText(state);
		}
	    @Override  
	    protected void onDestroy() {  
	        super.onDestroy();  
	        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理  
	        mMapView.onDestroy();  
	    }  
	    @Override  
	    protected void onResume() {  
	        super.onResume();  
	        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理  
	        mMapView.onResume();  
	        }  
	    @Override  
	    protected void onPause() {  
	        super.onPause();  
	        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理  
	        mMapView.onPause();  
	        }
		//为搜索街道的回调函数
		public void onGetGeoCodeResult(GeoCodeResult result) {
			if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
				Toast.makeText(maptest.this, "抱歉，未能找到结果", Toast.LENGTH_LONG)
						.show();
			}
			mBaiduMap.clear();
			mBaiduMap.addOverlay(new MarkerOptions().position(result.getLocation())
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.icon_marka)));
			mBaiduMap.setMapStatus(MapStatusUpdateFactory.newLatLng(result
					.getLocation()));
			String strInfo = String.format("纬度：%f 经度：%f",
					result.getLocation().latitude, result.getLocation().longitude);
			Toast.makeText(maptest.this, strInfo, Toast.LENGTH_LONG).show();
		}
		//为搜索经纬度的回调函数
		public void onGetReverseGeoCodeResult(ReverseGeoCodeResult result) {
			if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
				Toast.makeText(maptest.this, "抱歉，未能找到结果", Toast.LENGTH_LONG)
						.show();
			}
			mBaiduMap.clear();
			mBaiduMap.addOverlay(new MarkerOptions().position(result.getLocation())
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.icon_marka)));
			mBaiduMap.setMapStatus(MapStatusUpdateFactory.newLatLng(result
					.getLocation()));
			Toast.makeText(maptest.this, result.getAddress(),
					Toast.LENGTH_LONG).show();

		}
}
