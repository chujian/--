package com.hichujian.client.ui;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.Constants;
import com.hichujian.client.api.ActivityAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;
import com.hichujian.client.utils.Utils;
import com.widget.time.JudgeDate;
import com.widget.time.ScreenInfo;
import com.widget.time.WheelMain;


public class ActivityFabu extends Activity{
	static int value=0;	//用来标识哪个被选中了
	private ImageButton uploading;
	private static final int PHOTO_REQUEST_TAKEPHOTO = 1;// 拍照
	private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
	private static final int PHOTO_REQUEST_CUT = 3;// 结果
	private String[] items,itemstime;//各列表项要显示的内容
	private String sex;
	private Long  UserID;
	WheelMain wheelMain;
	private Long activityaheadTime ;
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	// 创建一个以当前时间为名称的文件
	File tempFile = new File(Environment.getExternalStorageDirectory(),getPhotoFileName());
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.activity_fabu);
        InputMethodManager  manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);  
      //性别选择
        final TextView buxian=(TextView)findViewById(R.id.buxian);
        final TextView man1=(TextView)findViewById(R.id.man1);
        final TextView woman1=(TextView)findViewById(R.id.woman1);
       
        
      //获取UserId
        SharedPreferences preferences = getSharedPreferences("User" , Context.MODE_PRIVATE);
        String UserId = preferences.getString( "UserId", "");
        UserID = Long. valueOf (UserId);
        Log. i("tag",Long. valueOf(UserId)+"Long型");
        Log. i("tag", UserId+ "");

      //主函数接受对象
        final Handler handler = new Handler() { 
                 @Override 
                 public void handleMessage(Message msg) { 
                     if(msg.what==States.ACTI_CREATE_SUCCESSFUL){
                    	 Toast. makeText(ActivityFabu.this, "活动创建成功了哈~" , Toast.LENGTH_SHORT).show();
                    	 finish();
                     }
                 } 
             }; 
        
      //不限
        buxian.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				value=3;
				//男的设置背景
				Drawable nav_upman1=getResources().getDrawable(R.drawable.fabu_man_unchecked);  
				nav_upman1.setBounds(0, 0, nav_upman1.getMinimumWidth(), nav_upman1.getMinimumHeight());  
				man1.setCompoundDrawables(null, null, nav_upman1, null);  
				//男的设置字体
				man1.setTextColor(Color.parseColor("#7b7b7b"));
				
				//女的设置背景
				Drawable nav_upwomen2=getResources().getDrawable(R.drawable.fabu_woman_unchecked);  
				nav_upwomen2.setBounds(0, 0, nav_upwomen2.getMinimumWidth(), nav_upwomen2.getMinimumHeight());  
				woman1.setCompoundDrawables(null, null, nav_upwomen2, null);
				//女的设置
				woman1.setTextColor(Color.parseColor("#7b7b7b"));
				
				//不限的设置背景
				Drawable nav_upnolimit1=getResources().getDrawable(R.drawable.no_limit_checked);  
				nav_upnolimit1.setBounds(0, 0, nav_upnolimit1.getMinimumWidth(), nav_upnolimit1.getMinimumHeight());  
				buxian.setCompoundDrawables(null, null, nav_upnolimit1, null);
				//不限的设置
				buxian.setTextColor(Color.parseColor("#0ec16f"));
			}
		});
        
        //男
        man1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				value=1;
				//男的设置背景
				Drawable nav_upman1=getResources().getDrawable(R.drawable.fabu_man_checked);  
				nav_upman1.setBounds(0, 0, nav_upman1.getMinimumWidth(), nav_upman1.getMinimumHeight());  
				man1.setCompoundDrawables(null, null, nav_upman1, null);  
				//男的设置字体
				man1.setTextColor(Color.parseColor("#36a8ff"));
				
				//女的设置背景
				Drawable nav_upwomen1=getResources().getDrawable(R.drawable.fabu_woman_unchecked);  
				nav_upwomen1.setBounds(0, 0, nav_upwomen1.getMinimumWidth(), nav_upwomen1.getMinimumHeight());  
				woman1.setCompoundDrawables(null, null, nav_upwomen1, null);
				//女的设置
				woman1.setTextColor(Color.parseColor("#7b7b7b"));
				
				//不限的设置背景
				Drawable nav_upnolimit2=getResources().getDrawable(R.drawable.no_limit_unchecked);  
				nav_upnolimit2.setBounds(0, 0, nav_upnolimit2.getMinimumWidth(), nav_upnolimit2.getMinimumHeight());  
				buxian.setCompoundDrawables(null, null, nav_upnolimit2, null);
				//不限的设置
				buxian.setTextColor(Color.parseColor("#7b7b7b"));
			}
		});
        //女
        woman1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				value=2;
				//男的设置背景
				Drawable nav_upman1=getResources().getDrawable(R.drawable.fabu_man_unchecked);  
				nav_upman1.setBounds(0, 0, nav_upman1.getMinimumWidth(), nav_upman1.getMinimumHeight());  
				man1.setCompoundDrawables(null, null, nav_upman1, null);  
				//男的设置字体
				man1.setTextColor(Color.parseColor("#7b7b7b"));
				
				//女的设置背景
				Drawable nav_upwomen1=getResources().getDrawable(R.drawable.fabu_woman_checked);  
				nav_upwomen1.setBounds(0, 0, nav_upwomen1.getMinimumWidth(), nav_upwomen1.getMinimumHeight());  
				woman1.setCompoundDrawables(null, null, nav_upwomen1, null);
				//女的设置
				woman1.setTextColor(Color.parseColor("#ff3469"));
				
				//不限的设置背景
				Drawable nav_upnolimit3=getResources().getDrawable(R.drawable.no_limit_unchecked);  
				nav_upnolimit3.setBounds(0, 0, nav_upnolimit3.getMinimumWidth(), nav_upnolimit3.getMinimumHeight());  
				buxian.setCompoundDrawables(null, null, nav_upnolimit3, null);
				//不限的设置
				buxian.setTextColor(Color.parseColor("#7b7b7b"));
			}
		});
        //时间选择器
        final TextView TimeTxt = (TextView)findViewById(R.id.TimeTxt);
        LinearLayout TimePicker = (LinearLayout)findViewById(R.id.TimePicker);
        TimePicker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				LayoutInflater inflater = LayoutInflater
						.from(ActivityFabu.this);
				final View timepickerview = inflater.inflate(
						R.layout.timepicker, null);
				ScreenInfo screenInfo = new ScreenInfo(ActivityFabu.this);
				wheelMain = new WheelMain(timepickerview, true);
				wheelMain.screenheight = screenInfo.getHeight();
				Calendar calendar = Calendar.getInstance();
				int year = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH);
				int day = calendar.get(Calendar.DAY_OF_MONTH);
				int h = calendar.getTime().getHours();
				int m = calendar.getTime().getMinutes();
				wheelMain.initDateTimePicker(year, month, day, h, m);
				new AlertDialog.Builder(ActivityFabu.this)
						.setTitle("选择时间")
						.setView(timepickerview)
						.setPositiveButton("确定",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										TimeTxt.setText(wheelMain.getTime());
									}
								})
						.setNegativeButton("取消",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
									}
								}).show();
			}
		});
        
      //返回按钮监听
        ImageButton back_to_login1=(ImageButton)findViewById(R.id.back_to_login1);
        back_to_login1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
        
        //活动地点
        LinearLayout activity_map=(LinearLayout)findViewById(R.id.activity_map);
        activity_map.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent();
				intent.setClass(ActivityFabu.this, maptest.class);
				Log.i("tag", "activity_map进入前");
				ActivityFabu.this.startActivity(intent);
			}
		});
        
     //上传图片按钮
        uploading=(ImageButton)findViewById(R.id.uploading);
        uploading.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showDialog();
			}
		});
     
     //活动类型
        LinearLayout activity_type=(LinearLayout)findViewById(R.id.activity_type);
        final TextView activity_type_item=(TextView)findViewById(R.id.activity_type_item);
        activity_type.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				items=new String[]{"随便","运动","旅游","阅读","聚餐","K歌","桌游","电影","网友"};
				Builder builder=new AlertDialog.Builder(ActivityFabu.this);
				builder.setIcon(R.drawable.cha);
				builder.setTitle("请选择要发布的活动类型:");
				//添加列表项
				builder.setItems(items,new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int which) {
						// TODO Auto-generated method stub
						activity_type_item.setText(items[which]);
					}
				});
				builder.create().show();
			}
		});
        
     /* //期望人数
        LinearLayout excepted_person=(LinearLayout)findViewById(R.id.excepted_person);
        final EditText excepted_person_item=(EditText)findViewById(R.id.excepted_person_item);
        excepted_person.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				final String[] items=new String[]{"1人","2人","3人","4人","5人","6人","7人","8人","9人","10人","10-15人","15-50人"};
				Builder builder=new AlertDialog.Builder(ActivityFabu.this);
				builder.setIcon(R.drawable.cha);
				builder.setTitle("请选择期望的人数:");
				//添加列表项
				builder.setItems(items,new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int which) {
						// TODO Auto-generated method stub
						excepted_person_item.setText(items[which]);
					}
				});
				builder.create().show();
			}
		});*/
      
      //报名截止时间
        LinearLayout registration_deadline=(LinearLayout)findViewById(R.id.registration_deadline);
        final TextView registration_deadline_item=(TextView)findViewById(R.id.registration_deadline_item);
        registration_deadline.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				itemstime=new String[]{"不提前","提前30分钟","提前1小时","提前2小时","提前3小时","提前1天","提前2天"};
				Builder builder=new AlertDialog.Builder(ActivityFabu.this);
				builder.setIcon(R.drawable.cha);
				builder.setTitle("请选择报名截止时间:");
				//添加列表项
				builder.setItems(items,new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int which) {
						// TODO Auto-generated method stub
						registration_deadline_item.setText(items[which]);
					}
				});
				builder.create().show();
			}
		});
        //发布活动主题
        final EditText activity_fabu_theme = (EditText)findViewById(R.id.activity_fabu_theme);
       //发布活动说明
        final EditText activity_fabu_explain=(EditText)findViewById(R.id.activity_fabu_explain);
        //期待活动
        final EditText excepted_person_item=(EditText)findViewById(R.id.excepted_person_item);
        TextView commit=(TextView)findViewById(R.id.commit);
        commit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				//获取活动主题
				final String activity_fabu_theme_string = activity_fabu_theme.getText().toString();
				//获取活动类型
				final String activity_type_item_string = activity_type_item.getText().toString();
				//获取活动时间
				String TimeTxtString = TimeTxt.getText().toString();
				Long activityTime = Timestamp.parse(TimeTxtString);
				
				String ahead = registration_deadline_item.getText().toString();
				if(ahead.equals("不提前")){
					activityaheadTime = 0*24*3600*1000L;
				}
				if(ahead.equals("提前30分钟")){
					activityaheadTime = 1800*1000L;
				}
				if(ahead.equals("提前1小时")){
					activityaheadTime = 1*3600*1000L;
				}
				if(ahead.equals("提前2小时")){
					activityaheadTime = 2*3600*1000L;
				}
				if(ahead.equals("提前3小时")){
					activityaheadTime = 3*3600*1000L;
				}
				if(ahead.equals("提前1天")){
					activityaheadTime = 1*24*3600*1000L;
				}
				if(ahead.equals("提前2天")){
					activityaheadTime = 2*24*3600*1000L;
				}
				
				
				//获取活动地点
				
				//获取活动性别
				if(value==1){	//表示男
					sex="男";
				}else if(value==2){	//表示女
					sex="女";
				} else{
					sex="不限";
				}
				
				//期待人数
				int excepted_person_item_int = 1;
				try {
					excepted_person_item_int = Integer.parseInt(excepted_person_item.getText().toString());
				} catch(NumberFormatException e) {
				}
				//报名截止时间
				
				
				//说明
				final String activity_fabu_explain_string = activity_fabu_explain.getText().toString();
				//上传图片
				final Uri uri=Uri.fromFile(tempFile);
				String URI = Constants.ROOT_ADDRESS+"/"+"hichujianpicture.jpg";
							
				ActivityAPI activityAPI = ActivityAPI.getInstance();
				activityAPI.createActivity(UserID, activity_type_item_string, activity_fabu_theme_string, sex, activity_fabu_explain_string, excepted_person_item_int, new Timestamp(activityTime-activityaheadTime), 38.002, 38.001, URI, null,String.valueOf(uri), new OnResponseListener() {
					
					@Override
					public void onResponse(String data) {
						// TODO Auto-generated method stub
						Log.i("tag", "请求成功");
						ResponseDO result=JSON.parseObject(data,ResponseDO.class);
						if(result.getResultCode()==States.ACTI_CREATE_SUCCESSFUL){//活动创建成功
							Log.i("tag", "创建成功成功");
							//发送消息
							Message msg = new Message();
							msg. what=States.ACTI_CREATE_SUCCESSFUL ;
							handler.sendMessage(msg); 
						}
						if(result.getResultCode()==States.ACTI_CREATE_JSON_ERROR){//活动json格式错误
							Log.i("tag", "活动json格式错误");
						}
						if(result.getResultCode()==States.ACTI_CREATE_OTHER_ERROR){//活动创建其他错误（数据库错误）
							Log.i("tag", "活动创建其他错误(数据库错误)");
						}
					}
					
					@Override
					public void onError(String info) {
						// TODO Auto-generated method stub
						Log.i("tag", "创建失败");
					}
				});//回调结束				
			}
		});
}//end of Oncreate
	
	@Override
    public boolean onTouchEvent(android.view.MotionEvent event) {
      InputMethodManager imm = (InputMethodManager)     getSystemService(INPUT_METHOD_SERVICE);
           return                       
              imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
 }
	//提示对话框方法
		private void showDialog() {
			new AlertDialog.Builder(this)
			.setTitle("头像设置")
			.setPositiveButton("拍照", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					// 调用系统的拍照功能
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					// 指定调用相机拍照后照片的储存路径
					intent.putExtra(MediaStore.EXTRA_OUTPUT,
							Uri.fromFile(tempFile));
					startActivityForResult(intent, PHOTO_REQUEST_TAKEPHOTO);
				}
			})
			.setNegativeButton("相册", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					Intent intent = new Intent(Intent.ACTION_PICK, null);
					intent.setDataAndType(
							MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
							"image/*");
					startActivityForResult(intent, PHOTO_REQUEST_GALLERY);
				}
			}).show();
		}
		//回调函数
		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			// TODO Auto-generated method stub

			switch (requestCode) {
			case PHOTO_REQUEST_TAKEPHOTO://当选择拍照时调用
				startPhotoZoom(Uri.fromFile(tempFile), 200);
				break;

			case PHOTO_REQUEST_GALLERY://当选择从本地获取图片时
				//做非空判断，当我们觉得不满意想重新剪裁的时候便不会报异常，下同
				if (data != null)
					startPhotoZoom(data.getData(), 200);
				break;

			case PHOTO_REQUEST_CUT://返回的结果
				if (data != null) 
					setPicToView(data);
				break;
			}
			super.onActivityResult(requestCode, resultCode, data);

		}
		//照片裁剪
		private void startPhotoZoom(Uri uri, int size) {
			Intent intent = new Intent("com.android.camera.action.CROP");
			intent.setDataAndType(uri, "image/*");
			// crop为true是设置在开启的intent中设置显示的view可以剪裁
			intent.putExtra("crop", "true");

			// aspectX aspectY 是宽高的比例
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);

			// outputX,outputY 是剪裁图片的宽高
			/*intent.putExtra("outputX", size);
			intent.putExtra("outputY", size);*/
			intent.putExtra("return-data", true);

			startActivityForResult(intent, PHOTO_REQUEST_CUT);
			
		}

		//将进行剪裁后的图片显示到UI界面上
		private void setPicToView(Intent picdata) {
			Bundle bundle = picdata.getExtras();
			if (bundle != null) {
				Bitmap	photo = bundle.getParcelable("data");
				Drawable drawable = new BitmapDrawable(photo);
				uploading.setBackgroundDrawable(drawable);
				savePhotoToSDCard(Constants.ROOT_ADDRESS+"/","hichujianpicture.jpg",photo);
			}
		}

		// 使用系统当前日期加以调整作为照片的名称
		private String getPhotoFileName() {
			Date date = new Date(System.currentTimeMillis());
			SimpleDateFormat dateFormat = new SimpleDateFormat("'IMG'_yyyyMMdd_HHmmss");
			return dateFormat.format(date) + ".jpg";
		}

		  //隐藏软键盘
	    @Override
	    public boolean dispatchTouchEvent(MotionEvent ev) {
	        if (ev.getAction() == MotionEvent.ACTION_DOWN) {

	            // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
	            View v = getCurrentFocus();

	            if (isShouldHideInput(v, ev)) {
	                hideSoftInput(v.getWindowToken());
	            }
	        }
	        return super .dispatchTouchEvent(ev);
	    }

	    /**
	     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
	     *
	     * @param v
	     * @param event
	     * @return
	     */
	    private boolean isShouldHideInput(View v, MotionEvent event) {
	        if (v != null && (v instanceof EditText)) {
	            int[] l = { 0, 0 };
	            v.getLocationInWindow(l);
	            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
	                    + v.getWidth();
	            if (event.getX() > left && event.getX() < right
	                    && event.getY() > top && event.getY() < bottom) {
	                // 点击EditText的事件，忽略它。
	                return false ;
	            } else {
	                return true ;
	            }
	        }
	        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
	        return false ;
	    }

	    /**
	     * 多种隐藏软件盘方法的其中一种
	     *
	     * @param token
	     */
	    private void hideSoftInput(IBinder token) {
	        if (token != null) {
	            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE );
	            im.hideSoftInputFromWindow(token,
	                    InputMethodManager. HIDE_NOT_ALWAYS);
	        }
	    }
	    
	  //保存照片到SD卡上面
		public static void savePhotoToSDCard(String path, String photoName,
	            Bitmap photoBitmap) {
	        if (android.os.Environment.getExternalStorageState().equals(
	                android.os.Environment.MEDIA_MOUNTED)) {
	            File dir = new File(path);
	            if (!dir.exists()) {
	                dir.mkdirs();
	            }
	            File photoFile = new File(path, photoName); //在指定路径下创建文件
	            FileOutputStream fileOutputStream = null;
	            try {
	                fileOutputStream = new FileOutputStream(photoFile);
	                if (photoBitmap != null) {
	                    if (photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100,
	                            fileOutputStream)) {
	                        fileOutputStream.flush();
	                    }
	                }
	            } catch (FileNotFoundException e) {
	                photoFile.delete();
	                e.printStackTrace();
	            } catch (IOException e) {
	                photoFile.delete();
	                e.printStackTrace();
	            } finally {
	                try {
	                    fileOutputStream.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	    }


}