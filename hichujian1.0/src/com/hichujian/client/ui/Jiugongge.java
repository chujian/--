package com.hichujian.client.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.example.hichujian.R;
import com.hichujian.client.api.ActivityAPI;
import com.hichujian.client.model.ResponseDO;
import com.hichujian.client.utils.AsyncRunner.OnResponseListener;
import com.hichujian.client.utils.States;

public class Jiugongge extends Activity{
	private Long UserID ;
	private String USERID;
	private LocationManager loctionManager;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		setContentView(R.layout.jiugongge);
		
		SharedPreferences preferences = getSharedPreferences("User", Context.MODE_PRIVATE);
		String UserId = preferences.getString("UserId", "");
		UserID = Long.valueOf(UserId);
		Log.i("tag",Long.valueOf(UserId)+"Long型");
		Log.i("tag", UserId);
		
		//图片1
		ImageView jiugongge_suibian1=(ImageView)findViewById(R.id.jiugongge_suibian1);
		jiugongge_suibian1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				select(0);
			}
		});

		//图片2
		ImageView jiugongge_running2=(ImageView)findViewById(R.id.jiugongge_running2);
		jiugongge_running2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				select(1);
			}
		});

		//图片3
		ImageView jiugongge_traveling3=(ImageView)findViewById(R.id.jiugongge_traveling3);
		jiugongge_traveling3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				select(2);
			}
		});
		//图片4
		ImageView jiugongge_reading4=(ImageView)findViewById(R.id.jiugongge_reading4);
		jiugongge_reading4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				select(3);
			}
		});
		//图片5
		ImageView jiugongge_eatting5=(ImageView)findViewById(R.id.jiugongge_eatting5);
		jiugongge_eatting5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				select(4);
			}
		});
		//图片6
		ImageView jiugongge_kge6=(ImageView)findViewById(R.id.jiugongge_kge6);
		jiugongge_kge6.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				select(5);
			}
		});
		//图片7
		ImageView jiugongge_tablegame7=(ImageView)findViewById(R.id.jiugongge_tablegame7);
		jiugongge_tablegame7.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				select(6);
			}
		});
		//图片8
		ImageView jiugongge_move8=(ImageView)findViewById(R.id.jiugongge_move8);
		jiugongge_move8.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				select(7);
			}
		});
		//图片9
		ImageView jiugongge_game9=(ImageView)findViewById(R.id.jiugongge_game9);
		jiugongge_game9.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				select(8);
			}
		});
	}
	public void select(int i){
		/*double longitude =0.0;  
		double latitude=0.0;  
		
		  
		LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);  
		        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){  
		            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);  
		            if(location != null){  
		                latitude = location.getLatitude();  
		                longitude = location.getLongitude();  
		                }  
		        }else{  
		            LocationListener locationListener = new LocationListener() {  
		                  
		                // Provider的状态在可用、暂时不可用和无服务三个状态直接切换时触发此函数  
		                @Override  
		                public void onStatusChanged(String provider, int status, Bundle extras) {  
		                      
		                }  
		                  
		                // Provider被enable时触发此函数，比如GPS被打开  
		                @Override  
		                public void onProviderEnabled(String provider) {  
		                      
		                }  
		                  
		                // Provider被disable时触发此函数，比如GPS被关闭   
		                @Override  
		                public void onProviderDisabled(String provider) {  
		                      
		                }  
		                  
		                //当坐标改变时触发此函数，如果Provider传进相同的坐标，它就不会被触发   
		                @Override  
		                public void onLocationChanged(Location location) {  
		                    if (location != null) {     
		                        Log.e("Map", "Location changed : Lat: "    
		                        + location.getLatitude() + " Lng: "    
		                        + location.getLongitude());     
		                    }  
		                }  
		            };  
		            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,1000, 0,locationListener);     
		            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);     
		            if(location != null){     
		                latitude = location.getLatitude(); //经度     
		                longitude = location.getLongitude(); //纬度  
		            }     
		        }  
		        Log.i("tag", String.valueOf(longitude)+"经度"+String.valueOf(latitude)+"纬度");   */ 
		/*String contextService=Context.LOCATION_SERVICE;
		//通过系统服务，取得LocationManager对象
		loctionManager=(LocationManager) getSystemService(contextService);
		//通过GPS位置提供器获得位置(指定具体的位置提供器)
		String provider=LocationManager.GPS_PROVIDER;
		Location location = loctionManager.getLastKnownLocation(provider);
		double longitude = location.getLongitude();
		double latitude = location.getLatitude();
		*/
		
		Intent intent=new Intent();
		intent.setClass( Jiugongge. this,  Tab.class);
		//把用户ID数据存入SharePreferences
		SharedPreferences preferences1 = getSharedPreferences("secletNumber", Context.MODE_PRIVATE);
		Editor editor = preferences1.edit();
		editor.putString("SecletNumber", String.valueOf(i));
		editor.commit();
		Log.i("tag", "SecletNumber存入SharePreferences后值为"+ String.valueOf(i));
		Jiugongge.this.startActivity(intent);
		//ActivityAPI activity=ActivityAPI.getInstance();
		//Log.i("tag",String.valueOf(activityTyps));	//activityTyps
		/*activity.getActivityAround(UserID,longitude,latitude, 10.00, null, 0, 100,new String[]{"K歌"} , null, 0, 10, null, new OnResponseListener() {
			@Override
			public void onResponse(String data) {
				// TODO Auto-generated method stub
				ResponseDO result=JSON.parseObject(data,ResponseDO.class);
				if(result.getResultCode()==States.UPLOAD_SUCCESSFUL){	//上传成功
					Log.i("tag", "筛选成功");
					Log.i("tag", data+"data \n"+result.getData().toString()+"String \n");
				}
			}
			
			@Override
			public void onError(String info) {
				// TODO Auto-generated method stub
				Log.i("tag", "筛选成功");
			}
		});//获取附近的活动
*/	}
}
