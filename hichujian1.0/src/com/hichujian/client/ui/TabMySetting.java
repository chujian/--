package com.hichujian.client.ui;
import com.example.hichujian.R;
import com.hichujian.client.ui.setting.MySetting;
import com.hichujian.client.ui.setting.NewMessage;
import com.hichujian.client.ui.setting.OrdPush;
import com.hichujian.client.ui.setting.ResetPsaaword;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class TabMySetting extends Fragment{
	private LinearLayout setting;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.dating04, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		//设置按钮
		setting=(LinearLayout)getView().findViewById(R.id.setting);
		setting.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setClass( getActivity(),  MySetting.class);
				TabMySetting.this.startActivity(intent);
			}
		});

		//订阅推送
		LinearLayout order_push=(LinearLayout)getView().findViewById(R.id.order_push);
		order_push.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.setClass( getActivity(),  OrdPush.class);
				TabMySetting.this.startActivity(intent);
			}
		});
		//修改密码
		LinearLayout ResetPsaaword=(LinearLayout)getView().findViewById(R.id.ResetPsaaword);
		ResetPsaaword.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent =new Intent();
				intent.setClass(getActivity(), ResetPsaaword.class);
				TabMySetting.this.startActivity(intent);
			}
		});
		//新消息推送
		LinearLayout NewMessage=(LinearLayout)getView().findViewById(R.id.NewMessage);
		NewMessage.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent =new Intent();
				intent.setClass(getActivity(), NewMessage.class);
				TabMySetting.this.startActivity(intent);
			}
		});
		
		//新消息推送
		LinearLayout AboutChujian=(LinearLayout)getView().findViewById(R.id.AboutChujian);
		AboutChujian.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				Intent intent =new Intent();
				intent.setClass(getActivity(), com.hichujian.client.ui.setting.AboutChujian.class);
				TabMySetting.this.startActivity(intent);
			}
		});
}//end of Oncreate
	

}
