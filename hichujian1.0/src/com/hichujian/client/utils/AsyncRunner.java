package com.hichujian.client.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;
/**
 * 异步多线程HTTP请求器
 * @author alen
 *
 */
public class AsyncRunner {
	public static final String TAG = "http request";//调试信息tag
	
	public final static String AUTHORIZATION = "Authorization";
	public final static String COOKIE = "Cookie";
	public final static String DATE = "Date";
	
	public static final String CHARSET = "UTF-8";
	public static final byte HTTP_GET = 0;//请求类型
	public static final byte HTTP_POST = 1;
	private static final int MAX_THREAD_COUNT = 5;//最大线程数量
	
	private static ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREAD_COUNT);
	//private static HttpClient client = new DefaultHttpClient();
	private static String JSESSIONID = null;
	private static OnResponseListener defaultOnResponseListener = new OnResponseListener() {
			@Override
			public void onResponse(String data) {}
			@Override
			public void onError(String info) {}
		};
	/**
	 * 发送HTTP请求
	 * @param url	请求的url
	 * @param params	请求的url参数
	 * @param methodType	请求类型（GET/POST）
	 * @param onResponseListener	请求返回后回调
	 */
	public static void sendRequest(final String url, final List<NameValuePair> params, final byte methodType, final OnResponseListener onResponseListener) {
		for(int i = 0; i < params.size(); i ++ ) {
			Log.i("tag", params.get(i).getName() + ":" + params.get(i).getValue());
		}
		Runnable task = new Runnable() {
			HttpClient client = new DefaultHttpClient();
			OnResponseListener listener = null;
			public void run() {
				if(onResponseListener != null) {
					listener = onResponseListener;
				} else {
					listener = defaultOnResponseListener;
				}
				HttpUriRequest request = null;
				long length = 0;
				if(methodType == HTTP_GET) {
					if(params != null) {
						request = new HttpGet(url + "?" + URLEncodedUtils.format(params, CHARSET));
						System.out.println(url + "?" + URLEncodedUtils.format(params, CHARSET));
					} else {
						request = new HttpGet(url);	
					}
				} else if(methodType == HTTP_POST){
					HttpPost post = new HttpPost(url);
					try {
						UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, CHARSET);
						post.setEntity(entity);
						try {
							length = new UrlEncodedFormEntity(params, CHARSET).getContent().available();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
						listener.onError("请求参数错误：" + e.getMessage());
						return;
					}
					request = post;
				} else {
					listener.onError("HTTP请求类型错误，只能为GET或POST");
					return;
				}
				//设置请求头的信息
				// 在cookie中带上JSESSIONID 
				Log.i(TAG, "http " + "JSESSIONID=" + JSESSIONID);
				if(null != JSESSIONID){
					request.setHeader(COOKIE, "JSESSIONID=" + JSESSIONID);
					String date = Utils.getGMTDate();
					request.setHeader(DATE, date);
					//参数的user_id都得放在第一个
					String userID = params.get(0).getValue();
					String sign = Utils.sign(userID, date, JSESSIONID, url.substring(url.substring(0, url.lastIndexOf("/")).lastIndexOf("/")), (int)length);			
					Log.i(TAG, "http sign:" + sign);
					request.setHeader(AUTHORIZATION, sign);
				}
				
				
				
				request.setHeader("user-agent", null);
				request.setHeader("Cookie2", null);
				request.setHeader("connection", null);
				request.setHeader("expect", null);
				
				try {
					Log.i(TAG, "http " + (methodType==HTTP_GET?"get":"post") + " 请求开始 : " + request.getURI().toString());
					HttpResponse response = client.execute(request);
					
					if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						CookieStore mCookieStore = ((AbstractHttpClient) client).getCookieStore();
						List<Cookie> cookies = mCookieStore.getCookies();
						Log.i(TAG, "http " + "cookies=" + cookies);
						for (int i = 0; i < cookies.size(); i++) {
							//这里是读取Cookie['PHPSESSID']的值存在静态变量中，保证每次都是同一个值
							if ("JSESSIONID".equals(cookies.get(i).getName())) {
								JSESSIONID = cookies.get(i).getValue();
								break;
				            }
						}
						Log.i(TAG, "http " + (methodType==HTTP_GET?"get":"post") + " 请求成功  : " + request.getURI().toString());
						String data = EntityUtils.toString(response.getEntity(), CHARSET);
						data = data.replaceAll("^\r\n", "");//去除第一个空行
						listener.onResponse(data);
						Header[] headers = response.getAllHeaders();
						for(Header head : headers) {
							System.out.println(head.getName() + ":" + head.getValue());
						}
					} else {
						Log.i(TAG, "http " + (methodType==HTTP_GET?"get":"post") + " 请求失败  : " + request.getURI().toString());
						listener.onError("请求失败：state  = " + response.getStatusLine().getStatusCode());
					}
					request.abort();
				} catch(Exception e) {
					request.abort();
					listener.onError("http请求错误：" + e.getMessage());
					e.printStackTrace();
				}
			}
		};
		executorService.execute(task);
	}
	/**
	 * 下载图片并且显示到imageview上
	 * @param imgUrl 图片的url
	 * @param imgv	显示图片的ImageView对象
	 */
	public static void loadImageAndShow(final String imgUrl, final ImageView imgv) {
		if(null == imgv) {
		//	Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
			Log.i(TAG, "imgv = null，取消了图片加载任务");
			return;
		}
		Runnable task = new Runnable() {
			public void run() {
				Log.i(TAG, "头像加载开始：");
				InputStream is = null;
				HttpURLConnection conn = null;
				try {
					 conn = (HttpURLConnection) (new URL(imgUrl)).openConnection();
					 conn.setDoInput(true);
					 conn.connect();
					 is = conn.getInputStream();
					 final Bitmap b = BitmapFactory.decodeStream(is);
					 imgv.setImageBitmap(b);
					 Log.i(TAG, "图片加载完毕!");
				} catch(Exception e) {
					e.printStackTrace();
				} finally {
					try {
						if(is != null)
							is.close();
						if(conn != null)
							conn.disconnect();
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		};
		executorService.execute(task);
	}
	
	//HTTP请求回调接口
	public interface OnResponseListener {
		public void onResponse(String data);
		public void onError(String info);
	}
}
