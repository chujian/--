package com.hichujian.client.utils;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @file_name ClassPathResource.java
 * @date 2013-4-2
 * @time 下午03:21:05
 * @type_name ClassPathResource
 * 判断是否符合手机号的规则
 */
public class ClassPathResource {
	public static boolean isMobileNO(String mobiles) {
		Pattern p = Pattern.compile("^((13[0-9])|(17\\d)|(14\\d)|(15[^4,\\D])|(18[0-9]))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}
	/*public static void main(String[] args) throws IOException {
		System.out.println(ClassPathResource.isMobileNO("18877778989"));
		System.out.println(ClassPathResource.isMobileNO("52244664644"));
	}*/
}

 