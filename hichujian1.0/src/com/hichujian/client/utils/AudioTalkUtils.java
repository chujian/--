package com.hichujian.client.utils;

import java.io.File;
import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;
/**
 * 语音工具类
 * 录音
 * 播放录音
 * @author 307427687
 *
 */
public class AudioTalkUtils {
	private static MediaRecorder recorder;//录音器
	private static String filePath = null;//声音文件路径
	
	private AudioTalkUtils() {}

	//开始录音
	public static void startRecord() {
		try {
			recorder = new MediaRecorder();
			recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
			recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			File tempFile = File.createTempFile("cjsf", ".amr", Environment.getExternalStorageDirectory());
			filePath = tempFile.getAbsolutePath();
			recorder.setOutputFile(tempFile.getAbsolutePath());
			recorder.prepare();
			
			recorder.start();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//停止录音，返回录音保存地址
	public static String stopRecord() {
		try {
			recorder.stop();
			recorder.release();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return filePath;
	}
	//播放录音
	public static void playAudio(String fileName) {
		try {
			final MediaPlayer player = new MediaPlayer();
			player.setDataSource(fileName);
			player.prepare();
			player.start();
			Log.i("", "开始播放");
			player.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					Log.i("", "播放完毕");
					player.stop();
					player.release();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
