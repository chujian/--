package com.hichujian.client.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESUtils {
	public static String srcKey = "a!g(201#co'e,e*f";//16个字符的密钥

	public static byte[] ivbyte = { 0x38, 0x37, 0x36, 0x35, 0x34, 0x33, 0x32,
			0x31, 0x38, 0x37, 0x36, 0x35, 0x34, 0x33, 0x32, 0x31 };

	// 加密
	public static String encrypt(String sSrc){
		if (srcKey == null || srcKey.length() != 16) {
			return null;
		}
		try {
			SecretKeySpec skeySpec = new SecretKeySpec(srcKey.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");// "算法/模式/补码方式"
			IvParameterSpec iv = new IvParameterSpec(ivbyte);// 使用CBC模式，需要一个向量iv，可增加加密算法的强�?
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
			byte[] encrypted = cipher.doFinal(sSrc.getBytes("UTF-8"));
			return Base64Encoder.encode(encrypted);// 此处使用BASE64转成String，不能new String
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// 解密
	public static String decrypt(String sSrc) {

		try {
			// 判断Key是否正确
			if (srcKey == null || srcKey.length() != 16) {
				return null;
			}
			SecretKeySpec skeySpec = new SecretKeySpec(srcKey.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec iv = new IvParameterSpec(ivbyte);
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			
			byte[] encrypted1 = Base64Decoder.decodeToBytes(sSrc);// 先用base64解密
			byte[] original = cipher.doFinal(encrypted1);
			String originalString = new String(original, "UTF-8");
			return originalString;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
