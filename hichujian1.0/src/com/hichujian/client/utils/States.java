package com.hichujian.client.utils;

public final class States {
	
	//**************对于所有的表字段状态 默认都为0
	//用户
	public static final int USER_STATE_NORMAL = 0;		//用户账户正常
	public static final int USER_STATE_FROZEN = 1;		//用户账户冻结
	
	public static final int USER_ONLINE = 0;			//用户在线
	public static final int USER_OFFLINE = 1;			//用户离线
	
	//活动模块
	public static final int ACTIVITY_NOT_DELETED= 0;	//活动正常
	public static final int ACTIVITY_DELETED = 1;		//活动被删除
	
	//评论模块
	public static final int COMMENT_NOT_DELETED = 0;	//活动未被删除
	public static final int COMMENT_DELETED = 1;		//活动已被删除
	
	//聊天记录
	public static final int MESSAGE_NOT_DELETED = 0;	//聊天未被删除
	public static final int MESSAGE_DELETED = 1;		//聊天已被删除
	
	public static final int MESSAGE_TYPE_TEXT = 0;		//聊天类型 文本
	public static final int MESSAGE_TYPE_IMAGE = 1;		//聊天类型 图像
	public static final int MESSAGE_TYPE_AUDIO = 2;		//聊天类型 语音
	
	//报名
	public static final int SIGN_UP_NOT_RECEIVED = 0;	//报名未被接受
	public static final int SIGN_UP_RECEIVED = 1;		//报名已被接受
	
	//举报
	public static final int REPORT_NOT_DISPOSED = 0;	//举报未被处理
	public static final int REPORT_DISPOSED = 1;		//举报已被处理
	
	//推送订单
	public static final int PUSH_DISTURBED = 0;					//非勿扰模式
	public static final int PUSH_NOT_DISTURBED = 1;				//勿扰模式

	public static final int PUSH_ON_ACTIVITY_FINISHED = 0;		//活动完成时推送
	public static final int NOT_PUSH_ON_ACTIVITY_FINISHED = 1;	//活动完成时不推送

	public static final int PUSH_ON_ACTIVITY_STARTED = 0;		//活动开始时推送
	public static final int NOT_PUSH_ON_ACTIVITY_STARTED = 1;	//活动开始时不推送
	
	public static final int PUSH_ON_ACTIVITY_SIGN_UP = 0;		//活动有人报名时推送
	public static final int NOT_PUSH_ON_ACTIVITY_SIGN_UP = 1;	//活动有人报名时不推送
	
	public static final int PUSH_PRIVATE_MESSAGE = 0;			//私信推送
	public static final int NOT_PUSH_PRIVATE_MESSAGE = 1;		//私信不推送
	//针对活动的推送频率
	public static final int PUSH_FREQ_CONSTANTLY = 0;			//经常推送
	public static final int PUSH_FREQ_ONCE_A_HOUR = 1;			//一小时推送一次
	public static final int PUSH_FREQ_ONCE_A_DAY = 2;			//一天推送一次
	public static final int PUSH_FREQ_NOT_PUSH = 3;				//不推送
	
	//推送类型                                    
	int PUSH_TYPE_ALL = 0;					//所有的类型    
	int PUSH_TYPE_ACTIVITY = 1;				//推送类型为活动    
	int PUSH_TYPE_COMMENT = 2;				//推送类型为评论 
	int PUSH_TYPE_ACTIVITY_SIGG_UP = 3;		//推送类型为活动有人报名
	int PUSH_TYPE_CANCEL_SIGN_UP = 4;		//推送类型为取消报名
	int PUSH_TYPE_ACTIVITY_ACCEPTED = 5;	//推送类型为活动被接受
	int PUSH_TYPE_ACTIVITY_DISMISS = 6;		//推送类型为活动解散
	int PUSH_TYPE_ACTIVITY_START = 7;		//推送类型为活动开始  
	int PUSH_TYPE_AD = 8;					//推送类型为广告    
	
	//******************请求返回状态码****************************
	//***用户模块状态码***
	//登入、登出状态码
	public static final int LOGIN_SUCCESSFUL = 100;				//登入成功
	public static final int LOGIN_PHONE_NOT_EXISTS = 101;		//登入手机号未注册
	public static final int LOGIN_PASSWORD_ERROR = 102;			//登入账户或密码错误
	public static final int LOGIN_USER_FROZEN = 103;			//登入账户被账户冻结
	public static final int LOGIN_OTHER_ERROR = 104;			//登入账户其他错误（数据库错误）
	//用户操作状态码
	public static final int LOGIN_NOT_LOGINING = 106;			//用户未登入
	public static final int SGIN_CHECK_ERROR = 107;				//签名错误
	public static final int USER_ILLEGAL_OPRATION = 108;		//用户非法操作、如删除他人的活动等等
	public static final int LOGIN_TOKEN_ERROR = 109;			//登入Token错误
	
	public static final int LOGINOUT_SUCCESSFUL = 110;			//登出成功
	public static final int LOGINOUT_ERROR = 111;				//登出错误
	
	//获取验证验
	public static final int AUTHCODE_GET_SUCCESSFUL = 120;			//获取验证码成功
	public static final int AUTHCODE_GET_EXISTS = 121;				//验证码已经成功获取两分钟内不能再次获取
	public static final int AUTHCODE_GET_ERROR = 122;				//获取验证码出错
	//验证验证码
	public static final int AUTHCODE_CHECK_SUCCESSFUL = 125;		//验证码验证成功
	public static final int AUTHCODE_CHECK_PARAMETER_ERROR = 126;	//参数错误
	public static final int AUTHCODE_CHECK_EXPIRED = 127;			//验证码过期
	public static final int AUTHCODE_CHECK_CODE_ERROR = 128;		//输入验证码错误
	
	
	//注册状态码
	public static final int REGISTER_SUCCESSFUL = 130;				//注册成功
	public static final int REGISTER_PHONE_EXISTS = 131;			//手机号已被注册
	public static final int REGISTER_PHONE_NOT_EXISTS = 132;		//手机号未被注册
	public static final int REGISTER_ERROR = 133;					//注册错误
	
	//个人信息设置
	public static final int SETUP_SUCCESSFUL = 140;					//设置成功
	public static final int SETUP_INFO_ERROR = 141;					//设置信息错误	
	public static final int SETUP_ERROR = 142;						//设置数据库操作错误
	
	//密码更改
	public static final int PWD_CHANGE_SUCCESSFUL = 150;			//密码更改成功
	public static final int PWD_CHANGE_OLD_PWD_ERROR= 151;			//验证旧密码错误
	public static final int PWD_CHANGE_OTHER_ERROR= 152;			//更改密码其他错误
	
	//重载密码（忘记密码）
	public static final int PWD_RESET_SUCCESSFUL = 160;				//密码重置成功
	public static final int PWD_RESET_PHONE_NOT_EXISTS = 161;		//手机都没注册，怎么重置密码..
	public static final int PWD_RESET_OTHER_ERROR = 162;			//密码重置其他错误
	
	//获取用户信息
	public static final int GET_USER_INFO_SUCCESSFUL = 170;			//获取用户信息成功
	public static final int GET_USER_INFO_NOT_EXISTS = 171;			//获取用户信息不存在
	public static final int GET_USER_INFO_OTHER_ERROR = 172;		//获取用户信息其他错误

	//更新用户位置
	public static final int UPDATE_LOCATION_SUCCESSFUL = 175;		//更新用户位置成功
	public static final int UPDATE_LOCATION_FAILED = 176;			//更新用户位置失败
	public static final int UPDATE_LOCATION_OTHER_ERROR = 177;	 	//更新用户位置其他错误
	
	//拉黑
	public static final int PULL_BLACK_SUCCESSFUL = 180;			//拉黑成功
	public static final int PULL_BLACK_OTHER_ERROR = 181;			//拉黑错误 
	//取消拉黑
	public static final int CANCEL_PULL_BLACK_SUCCESSFUL = 185;			//取消拉黑成功
	public static final int CANCEL_PULL_BLACK_OTHER_ERROR = 186;	 	//取消拉黑错误
	
	//用户上传文件
	public static final int UPLOAD_SUCCESSFUL = 190;					//文件上传成功
	public static final int UPLOAD_SIZE_EXCEEDED = 191;					//文件大小溢出
	public static final int UPLOAD_NO_FILES = 192;						//没有选择上传的文件
	public static final int UPLOAD_UPYUN_ERROR = 193;					//又拍云错误
	public static final int UPLOAD_FAILED = 194;						//上传文件失败
	
	//用户删除文件
	public static final int FILE_DELETE_SUCCESSFUL = 195;				//文件删除成功
	public static final int FILE_DELETE_UPYUN_ERROR = 196;				//又拍云错误
	public static final int FILE_DELETE_ILLEGAL = 197;					//非法删除文件、删除他人文件
	//***用户模块***
	
	//***活动模块状态码***
	//获活动信息（大厅活动、我发起的活动、我报名的活动）
	public static final int GET_ACTI_INFO_SUCCESSFUL = 200;			//获取活动信息成功
	public static final int GET_ACTI_NOT_EXISTS = 201;				//获取活动信息不存在
	public static final int GET_ACTI_INFO_OTHER_ERROR = 202;		//获取活动信息其他错误
	
	//获取活动的列表（大厅筛选列表、我报名的活动列表、我发起的活动列表）
	public static final int GET_ACTI_LIST_SUCCESSFUL = 205;			//获取活动信息列表
	public static final int GET_ACTI_LIST_ERROR = 206;				//获取活动信息列表错误
	
	//获取某个活动的报名人列表
	public static final int GET_ACTI_SIGN_UPER_SUCCESSFUL = 215;
	public static final int GET_ACTI_SIGN_UPER_ERROR = 216;
	
	//活动创建
	public static final int ACTI_CREATE_SUCCESSFUL = 220;			//活动创建成功
	public static final int ACTI_CREATE_JSON_ERROR = 221;			//活动json格式错误
	public static final int ACTI_CREATE_OTHER_ERROR = 222;			//活动创建其他错误（数据库错误）
	
	//活动删除
	public static final int DLETE_ACTI_SUCCESSFUL = 230;			//活动删除成功
	public static final int DLETE_ACTI_ERROR = 231;					//活动删除失败
	
	//报名
	public static final int SINGUP_SUCCESSFUL = 240;				//报名成功
	public static final int SINGUP_OTHER_ERROR = 241;				//报名错误 
	//取消报名
	public static final int CANCEL_SINGUP_SUCCESSFUL = 245;			//取消报名成功
	public static final int CANCEL_SINGUP_OTHER_ERROR = 246;		//取消报名错误
	//接受报名
	public static final int ACCEPT_SINGUP_SUCCESSFUL = 250;			//接受报名成功
	public static final int ACCEPT_SINGUP_OTHER_ERROR = 251;		//接受报名错误
	
	//举报
	public static final int REPORT_SUCCESSFUL = 255;				//举报成功
	public static final int REPORT_OTHER_ERROR = 256;				//举报错误 
	//取消举报
	public static final int CANCEL_REPORT_SUCCESSFUL = 260;			//取消举报成功
	public static final int CANCEL_REPORT_OTHER_ERROR = 261;	 	//取消举报错误
	
	//***活动模块状态码***
	
	//****评论模块状态码******
	//评论
	public static final int COMMENT_SUCCESSFUL = 300;				//评论成功
	public static final int COMMENT_QUERY_USER_FAILED = 301;		//评论失败(没找到评论者用户)
	public static final int COMMENT_OTHER_ERROR = 302;				//评论错误 
	//取消评论
	public static final int DELETE_COMMENT_SUCCESSFUL = 305;			//删除评论成功
	public static final int DELETE_COMMENT_NOT_EXISTS = 306;			//删除评论失败(没找到对应的记录)
	public static final int DELETE_COMMENT_HAS_DELETED = 307;			//删除评论失败(已被删除)
	public static final int DELETE_COMMENT_OTHER_ERROR = 308;	 		//删除评论错误
	
	//通过活动获取评论
	public static final int LIST_COMMENTS_SUCCESSFUL = 310;				//获取活动评论列表成功
	public static final int LIST_COMMENTS_IS_NULL = 311;				//获取活动评论列表为空
	public static final int LIST_COMMENTS_OTHER_ERROR = 312;			//获取活动评论列表错误
	//****评论模块状态码******
	
	//****私信模块状态码******
	//添加记录
	public static final int ADD_MESSAGE_SUCCESSFUL = 400;				//添加记录成功
	public static final int ADD_MESSAGE_OTHER_ERROR = 401;				//添加记录错误
	//删除记录
	public static final int DELETE_MESSAGE_SUCCESSFUL = 405;			//删除记录成功
	public static final int DELETE_MESSAGE_NOT_EXISTS = 406;			//删除记录不存在
	public static final int DELETE_MESSAGE_HAS_DELETED = 407;			//删除记录 是 已删除记录
	public static final int DELETE_MESSAGE_OTHER_ERROR = 408;			//删除记录错误
	//通过两个用户id获取消息记录
	public static final int LIST_MESSAGES_SUCCESSFUL = 410;				//获取消息记录列表成功
	public static final int LIST_MESSAGES_IS_NULL = 411;				//获取消息记录列表为空
	public static final int LIST_MESSAGES_OTHER_ERROR = 412;			//获取消息记录列表错误
	//****私信模块状态码******
	
	//*******推送模块状态码******
	//推送设置
	public static final int PUSH_SET_SUCCESSFUL = 500;					//推送设置成功
	public static final int PUSH_SET_JSON_ERROR = 501;					//推送设置信息json格式错误
	public static final int PUSH_SET_OTHER_ERROR = 502;					//推送设置其他错误
	
	//获取用户推送设置信息
	public static final int GET_SETTINGS_SUCCESSFUL = 505;				//获取推送设置信息成功
	public static final int GET_SETTINGS_IS_NULL = 506;					//获取推送设置信息为空
	public static final int GET_SETTINGS_ERROR = 507;					//获取推送设置信息错误
	
	//获取一条推送记录
	public static final int GET_PUSH_SUCCESSFUL = 510;
	public static final int GET_PUSH_ERROR = 511;
	//通过用户获取推送记录
	public static final int GET_PUSH_RECORDS_SUCCESSFUL = 515;
	public static final int GET_PUSH_RECORDS_ERROR = 516;
	
	//获取活动推送
	public static final int GET_ACTIVITY_PUSH_SUCCESSFUL = 520;			//获取活动推送成功
	public static final int GET_ACTIVITY_PUSH_FAILED = 521;				//获取活动推送失败
	//*******推送模块状态码******
	
	//*******更新模块状态码*****
	//检查是否更新
	public static final int CHECK_UPDATE_YES = 600;						//检查到更新
	public static final int CHECK_UPDATE_NO = 601;						//未检查到更新
		
}