package com.hichujian.client.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CodeCounter {
	
	static int normalLines = 0;
	static int commentLines = 0;
	static int whiteLines = 0;

	/**
	 * java文件的代码量统计
	 * 通过三个静态量来统计代码行数
	 * @param 无
	 */
	public static void main(String[] args) {

		File dirOrFile = new File("F:\\androidforchujian\\hichujian6.73");
		all_counter(dirOrFile);
		System.out.println("代码行数:" + normalLines);
		System.out.println("注释行数:" + commentLines);
		System.out.println("空白行数:" + whiteLines);
		
	}
	
	/*
	 * 通过递归的方法遍历整个文件夹中的文件 
	 * 当以文件名以.java结尾时调用counter(File)统计
	 * @param 一个目的文件或文件夹
	 */
	public static void all_counter(File dirOrFile) {
		if (dirOrFile.isFile()) {
			if (dirOrFile.getName().endsWith(".java")||dirOrFile.getName().endsWith(".xml")) {
				counter(dirOrFile);
			}
		} else {
			File[] files = dirOrFile.listFiles();
			for (int i = 0; i < files.length; i++) {
				all_counter(files[i]);
			}
		}
	}
	
	/**
	 * 统计单个文中的代码量
	 * 
	 * @param 统计的目的文件
	 */ 	
	public static void counter(File file) {
		BufferedReader read = null;
		try {
			boolean tag = false;
			read = new BufferedReader(new FileReader(file));
			String line = "";
			
			while ((line = read.readLine()) != null) {		//按行那字符串
				if (line.matches("^([^\\S\\n]*)$")){		//若干个非换行空白字符的行 也可能一个空白字符也没有
					whiteLines ++;
				} else if (line.matches("^([\\s&&[^\n]]*)//.*$")) {	//以空格+//开头的行
					commentLines ++;						
				} else if (line.matches("^([^\\S\\n]*)/\\*.*$") && line.matches("^.*\\*/([^\\S\\n]*)$")) {
					commentLines ++;
				} else if (line.matches("^([^\\S\\n]*)/\\*.*$") && !line.matches("^.*\\*/([^\\S\\n]*)$")) {
					commentLines ++;
					tag = true;
				} else if (!line.matches("^([^\\S\\n]*)/\\*.*$") && line.matches("^.*\\*/([^\\S\\n]*)$")) {
					commentLines ++;
					tag = false;
				} else if (tag) {
					commentLines ++;
				} else {
					normalLines ++;
				}
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (read != null) {
				try {
					read.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}

}
