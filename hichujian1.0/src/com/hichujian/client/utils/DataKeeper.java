package com.hichujian.client.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hichujian.client.model.PushSettingDO;
import com.hichujian.client.model.UserDO;

/**
 * 数据持久化工具类
 * 		必须在Application启动时调用init方法初始化！！！
 * 	 	必须在Application结束时调用destroy方法关闭数据库！！！
 * @author Alen
 *
 */
public class DataKeeper {
	private static SQLiteOpenHelper dbHelper;
	public static Context mContext;
	public static final String fileRootPath = "/storage/sdcard1/hichujian/";
	public static final String voicePath = "voice/";
	public static final String videoPath = "video/";
	public static final String imagePath = "image/";
	
	public static final String IMAGE = "imgae";
	public static final String VIDEO = "video";
	public static final String VOICE = "voice";
	
	//数据库
	public static final	String DATABASE_NAME = "client_chujian.sqlite";						//数据库名
	//关系表
	public static final String TABLE_MESSAGE = "client_message";						//聊天消息记录表
	public static final String TABLE_PUSH = "client_push";								//推送记录表
	public static final String TABLE_USER = "client_user";								//用户信息表
	public static final String TABLE_ACTIVITY = "client_activity";						//活动信息表
	//**********字段名********
	//聊天消息表字段
	public static final String KEY_MESSAGE_ID = "message_id";							//消息id			INTEGER
	public static final String KEY_MESSAGE_SENDER_ID = "sender_id";						//消息发送者id	INTEGER
	public static final String KEY_MESSAGE_RECEIVER_ID = "receiver_id";					//消息接收者id	INTEGER
	public static final String KEY_MESSAGE_TYPE = "type";								//消息类型		SMALLINT 0：文本、1：图片、2：语音
	public static final String KEY_MESSAGE_CONTENT = "content";							//消息内容		VARCHAR(255)
	public static final String KEY_MESSAGE_STATUS = "status";							//消息状态		SMALLINT 0：正常、1：已删除
	public static final String KEY_MESSAGE_SEND_TIME = "send_time";						//消息发送时间	TIMESTAMP
	//用户信息表字段
	public static final String KEY_USER_ID = "user_id";									//用户id			INTEGER
	public static final String KEY_USER_PHONE_NUMBER = "phone_number";					//手机号			VARCHAR(255)
	public static final String KEY_USER_QQ_NUMBER = "qq_number";						//QQ号			VARCHAR(255)
	public static final String KEY_USER_WEIBO_NUMBER = "weibo_number";					//微博号			VARCHAR(255)
	public static final String KEY_USER_PULLED_BLACK_NUMBER = "pulled_black_number";	//被拉黑次数		SMALLINT
	public static final String KEY_USER_BIRTHDAY = "birthday";							//生日			DATE
	public static final String KEY_USER_STATUS = "user_status";							//登入状态		SMALLINT 0:不在线、1:在线
	public static final String KEY_USER_SEX = "sex";									//性别			VARCHAR(255)
	public static final String KEY_USER_JOB = "job";									//职业			VARCHAR(255)
	public static final String KEY_USER_VOICE = "voice";								//声音			VARCHAR(255)
	public static final String KEY_USER_PHOTO = "photo";								//图片			VARCHAR(255)
	public static final String KEY_USER_UNIT = "unit";									//单位			VARCHAR(255)
	public static final String KEY_USER_LOCATION = "location";							//地区			VARCHAR(255)
	public static final String KEY_USER_NICKNAME= "nickname";							//昵称			VARCHAR(255)
	public static final String KEY_USER_HEAD_PORTRAIT = "head_portrait";				//头像			VARCHAR(255)
	public static final String KEY_USER_EMOTIONF_STATE = "emotion_state";				//情感状态		VARCHAR(255)
	public static final String KEY_USER_INTEREST_HOBBIES = "interest_hobbies";			//兴趣爱好		VARCHAR(255)
	public static final String KEY_USER_USUALLY_APPEAR_AREA = "usually_appear_area";	//常出没			VARCHAR(255)
	public static final String KEY_USER_STATE = "user_state";							//账户状态		SMALLINT 0：正常、1：冻结
	public static final String KEY_USER_CONSTELLATION = "constellation";				//星座			VARCHAR(255)
	
	//推送记录表字段
	public static final String KEY_PUSH_ID = "push_id";									//推送id			INTEGER
	public static final String KEY_PUSH_TYPE = "push_type";								//推送类	型		SMALLINT 见说明文档：初见推送内容说明文档	
	public static final String KEY_PUSH_CONTENT = "push_content";						//推送内容		TEXT
	public static final String KEY_PUSH_TIME = "push_time";								//推送时间		TIMESTAMP
	
	//活动信息表字段
	public static final String KEY_ACTIVITY_ID = "activityID";							//活动id			INTEGER
	public static final String KEY_ACTIVITY_TYPE = "activity_type";						//活动类型		VARCHAR(255)
	public static final String KEY_ACTIVITY_CONTENT = "activity_content";				//活动主题		VARCHAR(255)
	public static final String KEY_ACTIVITY_INVITED_SEX = "invited_object_sex";			//期望性别		VARCHAR(255)
	public static final String KEY_ACTIVITY_EXPLAIN = "activity_explain";				//活动说明		VARCHAR(255)
	public static final String KEY_ACTIVITY_EXPECTED_NUMBER = "expected_number";		//期望人数		SMALLINT
	public static final String KEY_ACTIVITY_TIME = "activity_time";						//活动时间		TIMESTAMP
	public static final String KEY_ACTIVITY_LONGITUDE = "activity_longitude";			//活动位置经度	DECIMAL(11,6)
	public static final String KEY_ACTIVITY_LATITUDE = "activity_latitude";				//活动位置纬度	DECIMAL(11,6)
	public static final String KEY_ACTIVITY_PLACE = "activity_place";					//活动地点		VARCHAR(255)
	public static final String KEY_ACTIVITY_DELETE_STATUS = "activity_delete_status";	//活动状态		SMALLINT 0：正常 1：解散
	public static final String KEY_ACTIVITY_USER_ID = "userID";							//用户id			INTEGER
	public static final String KEY_ACTIVITY_NICKNAME = "nickname";						//用户昵称		VARCHAR(255)
	public static final String KEY_ACTIVITY_HEAD_PROTRAIT = "head_portrait";			//用户头像		VARCHAR(255)
	public static final String KEY_ACTIVITY_SEX = "sex";								//用户性别		VARCHAR(255)
	public static final String KEY_ACTIVITY_BIRTHDAY = "birthday";						//用户生日		DATE
	public static final String KEY_ACTIVITY_CREATE_TIME = "create_time";				//活动创建时间	TIMESTAMP
	public static final String KEY_ACTIVITY_SIGN_UP_NUMBER = "sign_up_number";			//报名次数		INTEGER
	public static final String KEY_ACTIVITY_COMMENT_NUMBER = "comment_number";			//评名次数 		INTEGER
	public static final String KEY_ACTIVITY_REPORT_NUMBER = "report_number";			//举报次数		INTEGER
	public static final String KEY_ACTIVITY_PAST_TIME = "activity_past_time";			//活动过期时间	TIMESTAMP
	public static final String KEY_ACTIVITY_PICTURE = "activity_picture";				//活动图片		VARCHAR(255)
	
	public static final String KEY_PUSH_USER_ID = "push_user_id";//百度推送用户ID
	public static final String KEY_PUSH_CHANNEL_ID = "channel_id";//百度推送
	//SharePreference
	public static final String PREFERENCE_USER = "user";								//存档名
	public static final String KEY_PUSH_SETTINGS_DO = "push_settngs";					//推送设置
	public static final String KEY_USER_TOKEN = "token";								//用户登入Token	VARCHAR(255)
	public static final String KEY_USER_SESSION_ID = "session_id";						//用户sessionid	VARCHAR(255)
	
	public static DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S", Locale.US);
	
	
	//不能实例化
	private DataKeeper() {}
	
	//获取context，获取存档数据库引用
	public static void init(Context context) {
		Log.i("tag", "DatabaseKeeper init()");
		mContext = context;
		dbHelper = getSQLiteOpenHelper(context);
		File file = new File(fileRootPath);
		if(!file.exists()) {
			file = new File(fileRootPath + imagePath);
			file.mkdirs();
			file = new File(fileRootPath + videoPath);
			file.mkdir();
			file = new File(fileRootPath + voicePath);
			file.mkdir();
		}
	}
	//关闭所有数据库
	public static void destroy() {
		Log.i("tag", "DatabaseKeeper destroy()");
		dbHelper.close();
	}

	//获取数据库管理引用
	private static SQLiteOpenHelper getSQLiteOpenHelper(Context context) {
		return new SQLiteOpenHelper(context, DATABASE_NAME, null, 1) {
			@Override
			public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
				Log.i("tag", "SQLiteOpenHelper onUpgrade()");
			}
				
			@Override
			public void onOpen(SQLiteDatabase db) {
				// TODO Auto-generated method stub
				Log.i("tag", "SQLiteOpenHelper onOpen()");
				super.onOpen(db);
			}
			//首次创建数据库的时候创建所有的表
			@Override
			public void onCreate(SQLiteDatabase db) {
				Log.i("tag", "SQLiteOpenHelper onCreate()");
				//创建用户信息表
				StringBuffer userSql = new StringBuffer("");
				userSql.append("CREATE TABLE " + TABLE_USER + "(")
					.append(KEY_USER_ID + " INTEGER PRIMARY KEY,")
					.append(KEY_USER_PHONE_NUMBER + " VARCHAR(255),")
					.append(KEY_USER_QQ_NUMBER + " VARCHAR(255),")
					.append(KEY_USER_WEIBO_NUMBER + " VARCHAR(255),")
					.append(KEY_USER_PULLED_BLACK_NUMBER + " SMALLINT,")
					.append(KEY_USER_BIRTHDAY + " DATE,")
					.append(KEY_USER_STATUS + " SMALLINT,")
					.append(KEY_USER_SEX + " VARCHAR(255),")
					.append(KEY_USER_JOB + " VARCHAR(255),")
					.append(KEY_USER_VOICE + " VARCHAR(255),")
					.append(KEY_USER_PHOTO + " VARCHAR(255),")
					.append(KEY_USER_UNIT + " VARCHAR(255),")
					.append(KEY_USER_LOCATION + " VARCHAR(255),")
					.append(KEY_USER_NICKNAME + " VARCHAR(255),")
					.append(KEY_USER_HEAD_PORTRAIT + " VARCHAR(255),")
					.append(KEY_USER_EMOTIONF_STATE + " VARCHAR(255),")
					.append(KEY_USER_INTEREST_HOBBIES + " VARCHAR(255),")
					.append(KEY_USER_USUALLY_APPEAR_AREA + " VARCHAR(255),")
					.append(KEY_USER_STATE + " SMALLINT,")
					.append(KEY_USER_CONSTELLATION + " VARCHAR(255),")
					.append(");");
				Log.i("tag", "建用户表sql:" + userSql.toString());
				db.execSQL(userSql.toString());
				
				StringBuffer activitySql = new StringBuffer("");
				activitySql.append("CREATE TABLE " + TABLE_ACTIVITY + "(")
				.append(KEY_ACTIVITY_ID + " INTEGER PRIMARY KEY,")
				.append(KEY_ACTIVITY_TYPE + " VARCHAR(255),")
				.append(KEY_ACTIVITY_CONTENT + " VARCHAR(255),")
				.append(KEY_ACTIVITY_INVITED_SEX + " VARCHAR(255),")
				.append(KEY_ACTIVITY_EXPLAIN + " VARCHAR(255),")
				.append(KEY_ACTIVITY_EXPECTED_NUMBER + " SMALLINT,")
				.append(KEY_ACTIVITY_TIME + " TIMESTAMP,")
				.append(KEY_ACTIVITY_LONGITUDE + " DECIMAL(11,6),")
				.append(KEY_ACTIVITY_LATITUDE + " DECIMAL(11,6),")
				.append(KEY_ACTIVITY_PLACE + " VARCHAR(255),")
				.append(KEY_ACTIVITY_DELETE_STATUS + " SMALLINT,")
				.append(KEY_ACTIVITY_USER_ID + " INTEGER,")
				.append(KEY_ACTIVITY_NICKNAME + " VARCHAR(255),")
				.append(KEY_ACTIVITY_HEAD_PROTRAIT + " VARCHAR(255),")
				.append(KEY_ACTIVITY_SEX + " VARCHAR(255),")
				.append(KEY_ACTIVITY_BIRTHDAY + " DATE,")
				.append(KEY_ACTIVITY_CREATE_TIME + " TIMESTAMP,")
				.append(KEY_ACTIVITY_SIGN_UP_NUMBER + " INTEGER,")
				.append(KEY_ACTIVITY_COMMENT_NUMBER + " INTEGER,")
				.append(KEY_ACTIVITY_REPORT_NUMBER + " INTEGER,")
				.append(KEY_ACTIVITY_PAST_TIME + " TIMESTAMP,")
				.append(KEY_ACTIVITY_PICTURE + " VARCHAR(255)")
				.append(");");
				Log.i("tag", "建活动表sql:" + activitySql.toString());
				db.execSQL(activitySql.toString());
				
				//创建消息记录表 无过期时间
				StringBuffer messageSql = new StringBuffer(""); 
				messageSql.append("CREATE TABLE " + TABLE_MESSAGE + "(")
					.append(KEY_MESSAGE_ID + " INTEGER PRIMARY KEY,")
					.append(KEY_MESSAGE_SENDER_ID + " INTEGER,")
					.append(KEY_MESSAGE_RECEIVER_ID + " INTEGER,")
					.append(KEY_MESSAGE_TYPE + " SMALLINT,")
					.append(KEY_MESSAGE_CONTENT + " VARCHAR(255),")
					.append(KEY_MESSAGE_STATUS + " SMALLINT,")
					.append(KEY_MESSAGE_SEND_TIME + " TIMESTAMP")
					.append(");");
				Log.i("tag", "建消息表sql:" + messageSql.toString());
				db.execSQL(messageSql.toString());							//建消息表
				db.execSQL("insert table_message values(1,1,30,0,'fafa',0,'2014-07-16 00:00:00.000')");
				
				//创建推送记录表 不用更新的无过期时间
				StringBuffer pushSql = new StringBuffer("");
				pushSql.append("CREATE TABLE " + TABLE_PUSH + "(")
					.append(KEY_PUSH_ID + " INTEGER PRIMARY KEY,")
					.append(KEY_PUSH_TYPE + " SMALLINT,")
					.append(KEY_PUSH_CONTENT + " TEXT,")
					.append(KEY_PUSH_TIME + " TIMESTAMP")
					.append(");");
				Log.i("tag", "建推送记录表sql:" + messageSql.toString());
				db.execSQL(pushSql.toString());								//建推送记录表
			}
		};
	}
	
	//存储百度推送用户标识
	public static void storeBaiduUserInfo(String userID, String channelID) {
		mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
		.edit()
		.putString(KEY_PUSH_USER_ID, userID)
		.putString(KEY_PUSH_CHANNEL_ID, channelID)
		.commit();
	}
	//获取百度推送用户标识
	public static String[] getBaiduUserInfo() {
		String[] info = null;
		SharedPreferences s = mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE);
		String userID = s.getString(KEY_PUSH_USER_ID, "");
		String channelID = s.getString(KEY_PUSH_CHANNEL_ID, "");
		
		if(!TextUtils.isEmpty(userID) && !TextUtils.isEmpty(userID)) {
			info = new String[2];
			info[0] = userID;
			info[1]	= channelID;
		}
		return info;
	}
	//***********当前用户的相关账户信息***************
	//存储账号信息
	public static void storeUserAccount(String userID, String phoneNumber) {
		mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
		.edit()
		.putString(KEY_USER_ID, userID)
		.putString(KEY_USER_PHONE_NUMBER, phoneNumber)
		.commit();
	}
	
	//获取用户ID
	public static String getUserID() {
		return mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
		.getString(KEY_USER_ID, null);
	}
	
	//存储jsessionid
	public static void storeJSessionId(String jsessionid) {
		mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
		.edit()
		.putString(KEY_USER_SESSION_ID, jsessionid)
		.commit();
	}
	
	//获取jsessionid
	public static String getJSessionId() {
		return mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
				.getString(KEY_USER_SESSION_ID, null);
	}
	
	//存储Token
	public static void storeToken(String token) {
		mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
		.edit()
		.putString(KEY_USER_TOKEN, token)
		.commit();
	}
	
	//获取Token
	public static String getToken() {
		return mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
				.getString(KEY_USER_TOKEN, null);
		
	}
	/*public static void storeUserInfo(UserDO userDO) {
		mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
		.edit()
		.putString(KEY_USER_DO, JSONObject.toJSONString(userDO))
		.commit();
	}
	//获取用户信息
	public static UserDO getUserInfo() {
		String jsonStr = mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
							.getString(KEY_USER_DO, "");
		if(jsonStr.equals("")) {
			return null;
		}
		return JSONObject.parseObject(jsonStr, UserDO.class);
	}*/
	
	//****************存储推送设置*************
	public static void storePushSettings(PushSettingDO pushSettigns) {
		mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
		.edit()
		.putString(KEY_PUSH_SETTINGS_DO, JSONObject.toJSONString(pushSettigns))
		.commit();
	}
	
	//获取推送设置
	public static PushSettingDO getPushSettings() {
		String jsonStr = mContext.getSharedPreferences(PREFERENCE_USER, Context.MODE_PRIVATE)
							.getString(KEY_PUSH_SETTINGS_DO, JSON.toJSONString(new PushSettingDO(0, null, 5, null, 0)));
		return JSONObject.parseObject(jsonStr, PushSettingDO.class);
	}
	
	//插入用户信息
	public void storeUser(UserDO user) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_USER_BIRTHDAY, user.getBirthday().toString());
		values.put(KEY_USER_CONSTELLATION, user.getConstellation());
		values.put(KEY_USER_EMOTIONF_STATE, user.getEmotionState());
		values.put(KEY_USER_HEAD_PORTRAIT, user.getHeadPortrait());
		values.put(KEY_USER_ID, user.getUserID());
		values.put(KEY_USER_INTEREST_HOBBIES, user.getInterestHobbies());
		values.put(KEY_USER_JOB, user.getJob());
		values.put(KEY_USER_LOCATION, user.getLocation());
		values.put(KEY_USER_NICKNAME, user.getNickname());
		values.put(KEY_USER_PHONE_NUMBER, user.getPhoneNumber());
		values.put(KEY_USER_PHOTO, user.getPhoto());
		values.put(KEY_USER_PULLED_BLACK_NUMBER, user.getPulledBlackNumber());
		values.put(KEY_USER_QQ_NUMBER, user.getQqNumber());
		values.put(KEY_USER_SEX, user.getSex());
		values.put(KEY_USER_STATE, user.getUserState());
		values.put(KEY_USER_STATUS, user.getUserStatus());
		values.put(KEY_USER_UNIT, user.getUnits());
		values.put(KEY_USER_USUALLY_APPEAR_AREA, user.getUsuallyAppearArea());
		values.put(KEY_USER_VOICE, user.getVoice());
		values.put(KEY_USER_WEIBO_NUMBER, user.getWeiboNumber());
		db.insert(TABLE_USER, null, values);
		db.close();
	}
	
	//获取用户信息
	public static UserDO getUser(long userID) {
		UserDO user = new UserDO();
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Log.i("tag", "获取用户信息SQL语句： SELECT * FROM " + TABLE_USER + " WHERE " + KEY_USER_ID + "=?;");
		Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USER + " WHERE " + KEY_USER_ID + "=?;", new String[]{String.valueOf(userID)});
		cursor.moveToFirst();
		while(!cursor.isLast()) {
			Log.i("tag", "设置用户信息");
			user.setBirthday(java.sql.Date.valueOf(cursor.getString(cursor.getColumnIndex(KEY_USER_BIRTHDAY))));
			user.setConstellation(cursor.getString(cursor.getColumnIndex(KEY_USER_CONSTELLATION)));
			user.setEmotionState(cursor.getString(cursor.getColumnIndex(KEY_USER_EMOTIONF_STATE)));
			user.setHeadPortrait(cursor.getString(cursor.getColumnIndex(KEY_USER_HEAD_PORTRAIT)));
			user.setInterestHobbies(cursor.getString(cursor.getColumnIndex(KEY_USER_INTEREST_HOBBIES)));
			user.setJob(cursor.getString(cursor.getColumnIndex(KEY_USER_JOB)));
			user.setLocation(cursor.getString(cursor.getColumnIndex(KEY_USER_LOCATION)));
			user.setNickname(cursor.getString(cursor.getColumnIndex(KEY_USER_NICKNAME)));
			user.setPhoneNumber(cursor.getString(cursor.getColumnIndex(KEY_USER_PHONE_NUMBER)));
			user.setPhoto(cursor.getString(cursor.getColumnIndex(KEY_USER_PHOTO)));
			user.setPulledBlackNumber(cursor.getInt(cursor.getColumnIndex(KEY_USER_PULLED_BLACK_NUMBER)));
			user.setQqNumber(cursor.getString(cursor.getColumnIndex(KEY_USER_QQ_NUMBER)));
			user.setSex(cursor.getString(cursor.getColumnIndex(KEY_USER_SEX)));
			user.setUnits(cursor.getString(cursor.getColumnIndex(KEY_USER_UNIT)));
			user.setUserID(cursor.getLong(cursor.getColumnIndex(KEY_USER_ID)));
			user.setUserState(cursor.getInt(cursor.getColumnIndex(KEY_USER_STATE)));
			user.setUserStatus(cursor.getInt(cursor.getColumnIndex(KEY_USER_STATUS)));
			user.setUsuallyAppearArea(cursor.getString(cursor.getColumnIndex(KEY_USER_USUALLY_APPEAR_AREA)));
			user.setVoice(cursor.getString(cursor.getColumnIndex(KEY_USER_VOICE)));
			user.setWeiboNumber(cursor.getString(cursor.getColumnIndex(KEY_USER_WEIBO_NUMBER)));
			cursor.moveToNext();
		}
		return user;
	}
	/*//保存一条聊天记录
	public static void storeMessage(MessageDO msg) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_MESSAGE_ID, msg.getChattingID());
		values.put(KEY_MESSAGE_SENDER_ID, msg.getSenderID());
		values.put(KEY_MESSAGE_RECEIVER_ID, msg.getReceiverID());
		values.put(KEY_MESSAGE_TYPE, msg.getChattingContent());
		values.put(MessageAPI.KEY_TYPE, msg.getChattingContentType());
		values.put(MessageAPI.KEY_CREATE_TIME, sdf.format(msg.getChattingSendTime()));
		
		db.insert(TABLE_MESSAGE, null, values);
		db.close();
	}
	//获取一批聊天记录
	public static List<MessageDO> getMessageList(long userID, long anotherUserID, long beforTime, int count) {
		List<MessageDO> msgList = new ArrayList<MessageDO>();
		//构建获取聊天记录SQL语句
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		//构建返回列表
		Cursor cursor = db.query(TABLE_TALK, null, MessageAPI.KEY_CREATE_TIME + " < ?", new String[] {String.valueOf(beforTime)}, null, null, MessageAPI.KEY_CREATE_TIME, String.valueOf(count));
		while(cursor.moveToNext()) {
			MessageDO msg = new MessageDO();
			msg.setChattingID(cursor.getLong(cursor.getColumnIndex(MessageAPI.KEY_MESSAGE_ID)));
			msg.setSenderID(cursor.getLong(cursor.getColumnIndex(MessageAPI.KEY_FROM_USER_ID)));
			msg.setReceiverID(cursor.getLong(cursor.getColumnIndex(MessageAPI.KEY_TO_USER_ID)));
			msg.setChattingContent(cursor.getString(cursor.getColumnIndex(MessageAPI.KEY_CONTENT)));
			msg.setChattingContentType(cursor.getInt(cursor.getColumnIndex(MessageAPI.KEY_TYPE)));
			msg.setChattingSendTime(new Timestamp(cursor.getLong(cursor.getColumnIndex(MessageAPI.KEY_CREATE_TIME))));
			
			msgList.add(msg);
		}
		cursor.close();
		db.close();
		return msgList;
	}
	//清空聊天记录
	public static void clearTalkRecords(long fromUserID, long toUserID) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.delete(TABLE_TALK, null, null);
		db.close();
	}
	//保存一条推送记录
	public static void storePush(PushDO push) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(PushAPI.KEY_PUSH_ID, push.getPushID());
		values.put(PushAPI.KEY_PUSH_TYPE, push.getPushType());
		values.put(PushAPI.KEY_PUSH_TIME, sdf.format(push.getPushTime()));
		values.put(PushAPI.KEY_PUSH_CONTENT, push.getPushContent());
		
		db.insert(TABLE_PUSH, null, values);
		db.close();
	}
	//获取一批推送记录
	public static List<PushDO> getPushList(long beforTime, int count) {
		List<PushDO> pushList = new ArrayList<PushDO>();

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		
		Cursor cursor = db.query(TABLE_PUSH, null, PushAPI.KEY_PUSH_TIME + " < ?", new String[] {String.valueOf(beforTime)}, null, null, PushAPI.KEY_PUSH_TIME, String.valueOf(count));
		while(cursor.moveToNext()) {
			PushDO push = new PushDO();
			push.setPushID(cursor.getLong(cursor.getColumnIndex(PushAPI.KEY_PUSH_ID)));
			push.setPushType(cursor.getInt(cursor.getColumnIndex(PushAPI.KEY_PUSH_TYPE)));
			push.setPushTime(new Timestamp(cursor.getLong(cursor.getColumnIndex(PushAPI.KEY_PUSH_TIME))));
			push.setPushContent(cursor.getString(cursor.getColumnIndex(PushAPI.KEY_PUSH_CONTENT)));
			
			pushList.add(push);
		}
		cursor.close();
		db.close();
		
		return pushList;
	}
	//清空推送记录
	public static void clearPushRecords() {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		db.delete(TABLE_PUSH, null, null);
		db.close();
	}*/
	//存储临时文件 返回文件路径
	public static String storeFile(File file, String type) {
		String suffix = file.getName().substring(file.getName().lastIndexOf(".") + 1);
		byte[] data = null;
		try {
			FileInputStream in = new FileInputStream(file);
			data = new byte[in.available()];
			in.read(data, 0, data.length);
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return storeFile(data, suffix, type);
	}
	public static String storeFile(byte[] data, String suffix, String type) {
		String path = null;
		if(type == IMAGE) {
			path = fileRootPath + imagePath + "IMG_" + System.currentTimeMillis() + "." + suffix;
		}
		try {
			FileOutputStream out = new FileOutputStream(path);
			out.write(data, 0, data.length);
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			path = null;
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			path = null;
		}
		return path;
	}
}