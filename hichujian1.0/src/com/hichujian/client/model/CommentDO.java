package com.hichujian.client.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 评论DO
 */
public class CommentDO implements Serializable {	
	private static final long serialVersionUID = 1L;

	private long commentID;							//评论ID
	private long activityID;						//活动ID
	private long reviewerID;						//评论者ID
	private String reviewerNickname;				//评论者昵称
	private String reviewerHeadPortrait;			//评论者头像url
	private int commentStatus; 						//评论状态
	private String commentContent;					//评论内容
	private int commentFloor;						//楼层号
	private Timestamp commentTime;					//评论时间
	
	public CommentDO() {}
	
	public CommentDO(long activityID, long reviewerID,  String commentContent) {
		super();
		this.activityID = activityID;
		this.reviewerID = reviewerID;
		this.commentContent = commentContent;
	}

	@Override
	public String toString() {
		return "评论id：" + this.commentID + "\t楼层：" + this.commentFloor + "\t评论内容：" + this.commentContent ;
	}

	public long getCommentID() {
		return commentID;
	}
	public void setCommentID(long commentID) {
		this.commentID = commentID;
	}
	public long getActivityID() {
		return activityID;
	}
	public void setActivityID(long activityID) {
		this.activityID = activityID;
	}
	public long getReviewerID() {
		return reviewerID;
	}
	public void setReviewerID(long reviewerID) {
		this.reviewerID = reviewerID;
	}
	public String getReviewerNickname() {
		return reviewerNickname;
	}
	public void setReviewerNickname(String reviewerNickname) {
		this.reviewerNickname = reviewerNickname;
	}
	public String getReviewerHeadPortrait() {
		return reviewerHeadPortrait;
	}
	public void setReviewerHeadPortrait(String reviewerHeadPortrait) {
		this.reviewerHeadPortrait = reviewerHeadPortrait;
	}
	public String getContent() {
		return commentContent;
	}
	public void setContent(String content) {
		this.commentContent = content;
	}
	public int getFloor() {
		return commentFloor;
	}
	public void setFloor(int floor) {
		this.commentFloor = floor;
	}

	public Timestamp getCreateTime() {
		return commentTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.commentTime = createTime;
	}

	public void setCommentStatus(int commentStatus) {
		this.commentStatus = commentStatus;
	}

	public int getCommentStatus() {
		return commentStatus;
	}
}