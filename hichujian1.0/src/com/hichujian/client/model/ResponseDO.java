package com.hichujian.client.model;
/**
 * HTTP请求响应DO
 * @author 307427687
 *
 */
public class ResponseDO {
	private int resultCode;			//请求结果状态码
	private Object data;			//返回的数据（根据不同请求data指向不同类型的数据）
	
	public ResponseDO() {}
	
	public ResponseDO(int resultCode, Object data) {
		this.resultCode = resultCode;
		this.data = data;
	}
	
	@Override
	public String toString() {
		return "resultCode:" + resultCode + "\tdata:" + data;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}