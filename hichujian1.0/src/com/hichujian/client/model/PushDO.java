package com.hichujian.client.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 推送消息DO
 */
public class PushDO implements Serializable {
	private static final long serialVersionUID = 1L;

	private long pushID;				//推送消息ID
	private long receiverID;			//接收者ID
	private Timestamp pushTime;			//推送时间
	private int pushType;				//推送类型
	private String pushContent;			//推送内容
	
	public PushDO() {}
	
	public PushDO(long receiverID, Timestamp pushTime,
			int pushType, String pushContent) {
		super();
		this.receiverID = receiverID;
		this.pushTime = pushTime;
		this.pushType = pushType;
		this.pushContent = pushContent;
	}
	
	public long getPushID() {
		return pushID;
	}
	public void setPushID(long pushID) {
		this.pushID = pushID;
	}
	public long getReceiverID() {
		return receiverID;
	}
	public void setReceiverID(long receiverID) {
		this.receiverID = receiverID;
	}
	public Timestamp getPushTime() {
		return pushTime;
	}
	public void setPushTime(Timestamp pushTime) {
		this.pushTime = pushTime;
	}
	public int getPushType() {
		return pushType;
	}
	public void setPushType(int pushType) {
		this.pushType = pushType;
	}
	public String getPushContent() {
		return pushContent;
	}
	public void setPushContent(String pushContent) {
		this.pushContent = pushContent;
	}
}
