package com.hichujian.client.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 举报DO
 * @author Aci
 */
public class ReportDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public long reporterID;			//举报者id
	public long activityID;			//活动id
	public Timestamp reportTime;	//举报时间
	public String reportInfo;		//举报说明信息
	public int reportStatus;		//举报状态
	
	public ReportDO() {}

	public ReportDO(long reporterID, long activityID, Timestamp reportTime,
			String reportInfo, int reportStatus) {
		super();
		this.reporterID = reporterID;
		this.activityID = activityID;
		this.reportTime = reportTime;
		this.reportInfo = reportInfo;
		this.reportStatus = reportStatus;
	}

	public long getReporterID() {
		return reporterID;
	}

	public void setReporterID(long reporterID) {
		this.reporterID = reporterID;
	}

	public long getActivityID() {
		return activityID;
	}

	public void setActivityID(long activityID) {
		this.activityID = activityID;
	}

	public Timestamp getReportTime() {
		return reportTime;
	}

	public void setReportTime(Timestamp reportTime) {
		this.reportTime = reportTime;
	}

	public String getReportInfo() {
		return reportInfo;
	}

	public void setReportInfo(String reportInfo) {
		this.reportInfo = reportInfo;
	}

	public int getReportStatus() {
		return reportStatus;
	}

	public void setReportStatus(int reportStatus) {
		this.reportStatus = reportStatus;
	}
	
}
