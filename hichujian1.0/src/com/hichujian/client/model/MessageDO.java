package com.hichujian.client.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 聊天消息DO
 */
public class MessageDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long chattingID;				//该条聊天记录的ID
	private long senderID;					//发送者ID
	private long receiverID;				//接受者ID
	private Timestamp chattingSendTime;		//创建时间
	private int chattingStatus;				//聊天状态
	private String chattingContent;			//消息内容
	private int chattingContentType;		//消息类型：文本（包括表情）、语音
	
	public MessageDO() {}
	
	public MessageDO(long senderID, long receiverID,
			Timestamp chattingSendTime, int chattingStatus, String chattingContent, int chattingContentType) {
		super();
		this.senderID = senderID;
		this.receiverID = receiverID;
		this.chattingSendTime = chattingSendTime;
		this.chattingStatus = chattingStatus;
		this.chattingContent = chattingContent;
		this.chattingContentType = chattingContentType;
	}
	
	@Override
	public String toString() {
		return "发送者：" + senderID + "\t接受者：" + receiverID + "\t时间：" + chattingSendTime + "\t内容："  + chattingContent + "\t聊天类型：" + chattingContentType;
	}
	
	public long getChattingID() {
		return chattingID;
	}

	public void setChattingID(long chattingID) {
		this.chattingID = chattingID;
	}

	public long getSenderID() {
		return senderID;
	}

	public void setSenderID(long senderID) {
		this.senderID = senderID;
	}

	public long getReceiverID() {
		return receiverID;
	}

	public void setReceiverID(long receiverID) {
		this.receiverID = receiverID;
	}

	public Timestamp getChattingSendTime() {
		return chattingSendTime;
	}

	public void setChattingSendTime(Timestamp chattingSendTime) {
		this.chattingSendTime = chattingSendTime;
	}

	public String getChattingContent() {
		return chattingContent;
	}

	public void setChattingContent(String chattingContent) {
		this.chattingContent = chattingContent;
	}

	public int getChattingContentType() {
		return chattingContentType;
	}

	public void setChattingContentType(int chattingContentType) {
		this.chattingContentType = chattingContentType;
	}

	public void setChattingStatus(int chattingStatus) {
		this.chattingStatus = chattingStatus;
	}

	public int getChattingStatus() {
		return chattingStatus;
	}

}
