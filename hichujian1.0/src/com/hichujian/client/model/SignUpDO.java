package com.hichujian.client.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class SignUpDO implements Serializable {
	private static final long serialVersionUID = 1L;

	private long signUperID;
	private long activityID;
	private UserDO signUper;		//报名者
	private ActivityDO activity;	//报名的活动
	private int receiveStatus;		//接受状态
	private Timestamp signUpTime;	//报名时间
	private String signUpInfo;		//报名说明信息
	
	public SignUpDO() {}
	
	public SignUpDO(long signUperID, long activityID, int receiveStatus,
			Timestamp signUpTime, String signUpInfo) {
		super();
		this.signUperID = signUperID;
		this.activityID = activityID;
		this.receiveStatus = receiveStatus;
		this.signUpTime = signUpTime;
		this.signUpInfo = signUpInfo;
	}



	public long getSignUperID() {
		return signUperID;
	}

	public void setSignUperID(long signUperID) {
		this.signUperID = signUperID;
	}

	public long getActivityID() {
		return activityID;
	}

	public void setActivityID(long activityID) {
		this.activityID = activityID;
	}

	public UserDO getSignUper() {
		return signUper;
	}

	public void setSignUper(UserDO signUper) {
		this.signUper = signUper;
	}

	public ActivityDO getActivity() {
		return activity;
	}

	public void setActivity(ActivityDO activity) {
		this.activity = activity;
	}

	public int getReceiveStatus() {
		return receiveStatus;
	}

	public void setReceiveStatus(int receiveStatus) {
		this.receiveStatus = receiveStatus;
	}

	public Timestamp getSignUpTime() {
		return signUpTime;
	}

	public void setSignUpTime(Timestamp signUpTime) {
		this.signUpTime = signUpTime;
	}

	public String getSignUpInfo() {
		return signUpInfo;
	}

	public void setSignUpInfo(String signUpInfo) {
		this.signUpInfo = signUpInfo;
	}
}
