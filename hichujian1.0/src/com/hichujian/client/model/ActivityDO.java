package com.hichujian.client.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
/**
 * 活动DO
 * @author 
 */
public class ActivityDO implements Serializable, Comparable<ActivityDO>{

	private static final long serialVersionUID = 1L;
	//private static final long EXPIRE_DAYS = 7L;				//活动过期时间
	
	private long activityID;								//活动ID
	private String activityType;							//活动类型
	private String activityContent;							//活动内容
	private String invitedObjectSex;						//邀请对象的性别
	private String activityExplain;							//活动详情
	private int expectedNumber;								//期望人数
	private Timestamp activityTime;							//活动时间
	private double activityLongitude;						//活动所在经度
	private double activityLatitude;						//活动所在纬度
	private String activityPlace;							//活动地点
	private int activityDeleteStatus;						//活动状态（正常、删除）
	private int userID;										//发起活动的用户ID
	private Timestamp createTime;							//创建时间
	private int signUpNumber;								//参与人数
	private int commentNumber;								//评论数
	private int reportNumber;								//被举报次数
	private Timestamp activityPastTime;						//活动过期时间
	private String nickname;								//发布者昵称
	private String headPortrait;							//发布者头像url
	private String sex;										//发布者性别
	private Date birthday;									//发布者生日
	private String activityPicture;							//活动图片
	private double distance;								//距离
	private boolean isSignUped;								//用户是否报名该活动
	private boolean isReported;								//用户是否举报该活动
	
	public ActivityDO() {}	
	
	public ActivityDO(String activityType, String activityContent, Timestamp activityTime, String activityPlace,
					int expectedNumber, String invitedObjectSex, String activityExplain, double activityLongitude, double activityLatitude, 
					int userID, String nickname, String headPortrait, String sex, Date birthday, Timestamp activityPastTime, String activityPicture) {
		Calendar c = Calendar.getInstance();
		this.createTime = new Timestamp(c.getTimeInMillis());
		this.activityPastTime = activityPastTime;
		this.activityContent = activityContent;
		this.activityTime = activityTime;
		this.activityPlace = activityPlace;
		this.expectedNumber = expectedNumber;
		this.invitedObjectSex = invitedObjectSex;
		this.activityExplain = activityExplain;
		this.userID = userID;
		this.activityLongitude = activityLongitude;
		this.activityLatitude = activityLatitude;
		this.nickname = nickname;
		this.headPortrait = headPortrait;
		this.sex = sex;
		this.birthday = birthday;
		this.activityPicture = activityPicture;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return ("距离：" + this.getDistance()) +"活动ID" + this.activityID + "昵称：" + this.nickname + "活动类型：" + this.activityType + "\t活动内容：" + this.activityContent ;
	}
	
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getHeadPortrait() {
		return headPortrait;
	}

	public void setHeadPortrait(String headPortrait) {
		this.headPortrait = headPortrait;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activitType) {
		this.activityType = activitType;
	}
	public String getActivityContent() {
		return activityContent;
	}
	public void setActivityContent( String activityContent) {
		this.activityContent = activityContent;
	}
	public int getActivityDeleteStatus() {
		return activityDeleteStatus;
	}
	public void setActivityDeleteStatus(int activityDeleteStatus) {
		this.activityDeleteStatus = activityDeleteStatus;
	}
	public String getActivityExplain() {
		return activityExplain;
	}
	public void setActivityExplain(String activityExplain) {
		this.activityExplain = activityExplain;
	}
	public long getActivityID() {
		return activityID;
	}
	public void setActivityID(long activityID) {
		this.activityID = activityID;
	}
	public double getActivityLatitude() {
		return activityLatitude;
	}
	public void setActivityLatitude(float activityLatitude) {
		this.activityLatitude = activityLatitude;
	}
	public double getActivityLongitude() {
		return activityLongitude;
	}
	public void setActivityLongitude(float activityLongitude) {
		this.activityLongitude = activityLongitude;
	}
	public Timestamp getActivityPastTime() {
		return activityPastTime;
	}
	public void setActivityPastTime(Timestamp activityPastTime) {
		this.activityPastTime = activityPastTime;
	}
	public String getActivityPlace() {
		return activityPlace;
	}
	public void setActivityPlace(String activityPlace) {
		this.activityPlace = activityPlace;
	}
	public Timestamp getActivityTime() {
		return activityTime;
	}
	public void setActivityTime(Timestamp activityTime) {
		this.activityTime = activityTime;
	}
	public int getCommentNumber() {
		return commentNumber;
	}
	public void setCommentNumber(int commentNumber) {
		this.commentNumber = commentNumber;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public int getExpectedNumber() {
		return expectedNumber;
	}
	public void setExpectedNumber(int expectedNumber) {
		this.expectedNumber = expectedNumber;
	}
	public String getInvitedObjectSex() {
		return invitedObjectSex;
	}
	public void setInvitedObjectSex(String invitedObjectSex) {
		this.invitedObjectSex = invitedObjectSex;
	}
	public int getReportNumber() {
		return reportNumber;
	}
	public void setReportNumber(int reportNumber) {
		this.reportNumber = reportNumber;
	}
	public int getSignUpNumber() {
		return signUpNumber;
	}
	public void setSignUpNumber(int signUpNumber) {
		this.signUpNumber = signUpNumber;
	}
	public int getUserID() {
		return userID;
	}
		
	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public void setActivityLongitude(double activityLongitude) {
		this.activityLongitude = activityLongitude;
	}

	public void setActivityLatitude(double activityLatitude) {
		this.activityLatitude = activityLatitude;
	}

	@Override
	public int compareTo(ActivityDO o) {
		return new Double(this.distance).compareTo(o.getDistance());
	}

	public void setSignUped(boolean isSignUped) {
		this.isSignUped = isSignUped;
	}

	public boolean isSignUped() {
		return isSignUped;
	}

	public void setReported(boolean isReported) {
		this.isReported = isReported;
	}

	public boolean isReported() {
		return isReported;
	}

	public void setActivityPicture(String activityPicture) {
		this.activityPicture = activityPicture;
	}

	public String getActivityPicture() {
		return activityPicture;
	}

}