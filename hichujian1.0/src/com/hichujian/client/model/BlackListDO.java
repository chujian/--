package com.hichujian.client.model;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 黑名单DO
 * @author Aci
 *
 */
public class BlackListDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public long userID;						//用户ID
	public long pulledBlackUserID;			//被拉黑者ID
	public Timestamp pullBlackTime;			//拉黑时间
	
	public BlackListDO() {}

	public BlackListDO(long userID, long pulledBlackUserID,
			Timestamp pullBlackTime) {
		super();
		this.userID = userID;
		this.pulledBlackUserID = pulledBlackUserID;
		this.pullBlackTime = pullBlackTime;
	}

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public long getPulledBlackUserID() {
		return pulledBlackUserID;
	}

	public void setPulledBlackUserID(long pulledBlackUserID) {
		this.pulledBlackUserID = pulledBlackUserID;
	}

	public Timestamp getPullBlackTime() {
		return pullBlackTime;
	}

	public void setPullBlackTime(Timestamp pullBlackTime) {
		this.pullBlackTime = pullBlackTime;
	}
	
}
