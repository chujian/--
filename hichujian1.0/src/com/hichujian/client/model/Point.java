package com.hichujian.client.model;

public class Point {
	private double longitude;
	private double latitude;
	
	public Point() {}
	
	public Point(double longitude, double latitude) {
		super();
		this.setLongitude(longitude);
		this.latitude = latitude;
	}



	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLatitude() {
		return latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLongitude() {
		return longitude;
	}
}
