package com.hichujian.client.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
/**
 * 用户信息DO
 * @author 307427687
 *
 */
public class UserDO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private long userID;						//用户ID
	private int pulledBlackNumber;				//被拉黑次数
	private float height;						//身高
	private float weight;						//体重
	private Timestamp userRegisterTime;			//注册时间
	private Date birthday;						//生日
	private String qqNumber;					//QQ号
	private String weiboNumber;					//微博号
	private String phoneNumber;					//手机号
	private int userStatus;						//用户状态（在线、离线）
	private String sex;							//性别
	private String password;					//密码
	private String job;							//工作
	private String personalSignature;			//个性签名
	private String voice;						//语音介绍
	private String photo;						//相册url
	private String units;						//单位
	private String location;					//位置
	private String nickname;					//昵称
	private String headPortrait;				//头像url
	private String emotionState;				//心情状态
	private String interestHobbies;				//兴趣爱好
	private String usuallyAppearArea;			//常出没的地点
	private String constellation;				//星座
	private int userState;						//账号状态
	private boolean isPullBlack;				//是否拉黑
	
	public UserDO() {
	}
	
	public UserDO(String phoneNumber, String password, String nickname, String headPortrait, Date birthday, String sex) {
		this.phoneNumber = phoneNumber;
		this.password = password;
		this.nickname = nickname;
		this.headPortrait = headPortrait;
		this.birthday = birthday;
		this.sex = sex;
		this.userRegisterTime = new Timestamp(new java.util.Date().getTime());
		this.constellation = "";//Util.getStar(birthday);
	}
	
	@Override
	public String toString() {
		return "用户昵称：" + this.nickname;
	}

	public String getNickname(){
		return nickname;
	}
	
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getConstellation() {
		return constellation;
	}

	public void setConstellation(String constellation) {
		this.constellation = constellation;
	}

	public String getEmotionState() {
		return emotionState;
	}

	public void setEmotionState(String emotionState) {
		this.emotionState = emotionState;
	}

	public String getHeadPortrait() {
		return headPortrait;
	}

	public void setHeadPortrait(String headPortrait) {
		this.headPortrait = headPortrait;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public String getInterestHobbies() {
		return interestHobbies;
	}

	public void setInterestHobbies(String interestHobbies) {
		this.interestHobbies = interestHobbies;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String Birthday() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPersonalSignature() {
		return personalSignature;
	}

	public void setPersonalSignature(String personalSignature) {
		this.personalSignature = personalSignature;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getPulledBlackNumber() {
		return pulledBlackNumber;
	}

	public void setPulledBlackNumber(int pulledBlackNumber) {
		this.pulledBlackNumber = pulledBlackNumber;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public Timestamp getUserRegisterTime() {
		return userRegisterTime;
	}

	public void setUserRegisterTime(Timestamp userRegisterTime) {
		this.userRegisterTime = userRegisterTime;
	}

	public int getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}

	public String getUsuallyAppearArea() {
		return usuallyAppearArea;
	}

	public void setUsuallyAppearArea(String usuallyAppearArea) {
		this.usuallyAppearArea = usuallyAppearArea;
	}

	public String getVoice() {
		return voice;
	}

	public void setVoice(String voice) {
		this.voice = voice;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public long getUserID() {
		return userID;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public String getQqNumber() {
		return qqNumber;
	}

	public void setQqNumber(String qqNumber) {
		this.qqNumber = qqNumber;
	}

	public String getWeiboNumber() {
		return weiboNumber;
	}

	public void setWeiboNumber(String weiboNumber) {
		this.weiboNumber = weiboNumber;
	}
	public int getUserState() {
		return userState;
	}
	
	public void setUserState(int userState) {
		this.userState = userState;
	}

	public void setPullBlack(boolean isPullBlack) {
		this.isPullBlack = isPullBlack;
	}

	public boolean isPullBlack() {
		return isPullBlack;
	}

}
