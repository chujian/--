package com.hichujian.client.model;

import java.io.Serializable;

/**
 * 推送设置DO
 */
public class PushSettingDO implements Serializable {
	private static final long serialVersionUID = 1L;

	private long userID;						//使用该推动设置的用户ID
	private String sex;							//推送活动的发起者性别
	private double distance;					//推送的活动距离范围
	private String activityType;				//活动类型 多个类型用 ',' 分隔
	private int pushFrequency;					//针对活动的推送频率（1、2、3、4）

	public PushSettingDO() {}

	public PushSettingDO(long userID, String sex, double distance,
			String activityType, int pushFrequency) {
		super();
		this.userID = userID;
		this.sex = sex;
		this.distance = distance;
		this.activityType = activityType;
		this.pushFrequency = pushFrequency;
	}

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public int getPushFrequency() {
		return pushFrequency;
	}

	public void setPushFrequency(int pushFrequency) {
		this.pushFrequency = pushFrequency;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
